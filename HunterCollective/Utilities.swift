//
//  Utilities.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 23/06/21.
//

import Foundation

class Utilities: NSObject {
    static let shared = Utilities()
    private override init() {}
    
    func hourMinutesSecondsToSeconds (minSec: String) -> String {
        let arr = minSec.split(separator: ":")
        let hour: Int = Int(arr[0]) ?? 0
        let min: Int = Int(arr[1]) ?? 0
        let sec: Int = Int(arr[2]) ?? 0
        var str = ""
        if hour > 0{
            str = str + "\(hour)hr "
        }
        if min > 0 {
            str = str + "\(min)min "
        }
        if sec > 0 {
            str = str + "\(sec)sec"
        }
        return str
    }
    
    func SecondsToHourMinString (sec: Int) -> String {
       let hour = sec/3600
        let min = (sec % 3600)/60
        let seconds = (sec % 3600) % 60
        var str = ""
        if hour > 0{
            str = str + "\(hour)hr "
        }
        if min > 0 {
            str = str + "\(min)min "
        }
        if seconds > 0 {
            str = str + "\(seconds)sec"
        }
        if str == ""{
            str = "00sec"
        }
        return str
    }
    
    func secondsToHourMinutesSeconds (seconds : Int) -> String {
        let sec = (seconds % 3600) % 60
        let min = (seconds % 3600) / 60
        let hour = seconds/3600
      return "\(String(format: "%02d",hour)):\(String(format: "%02d",min)):\(String(format: "%02d",sec))"
    }
    
    func hourMinutesSecondsToSecondsInt (minSec: String) -> Int {
        let arr = minSec.split(separator: ":")
        let seconds = (Int(arr[0])! * 3600) + (Int(arr[1])! * 60) + Int(arr[2])!
        return seconds
    }
    
    func getLocalTimeFromUTC(strDate:String)-> String  {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.init(identifier: "UTC")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let convertedDate = formatter.date(from: strDate)
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "hh:mm a"
        print(formatter.string(from: convertedDate!))
        return formatter.string(from: convertedDate!)
    }
    func dateOrTimeTodayStr(strDate:String)-> String  {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.init(identifier: "UTC")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        if Calendar.current.isDateInToday(Date()) {
            let convertedDate = formatter.date(from: strDate)
            formatter.timeZone = TimeZone.current
            formatter.dateFormat = "hh:mm"
            return formatter.string(from: convertedDate!)
        } else {
            let convertedDate = formatter.date(from: strDate)
            formatter.timeZone = TimeZone.current
            formatter.dateFormat = "dd/MM/yyyy"
            return formatter.string(from: convertedDate!)
        }
        
    }
    
}
