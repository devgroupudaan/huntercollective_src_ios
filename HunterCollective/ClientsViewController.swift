//
//  ClientsViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 09/06/21.
//

import UIKit
import SwiftyJSON

class clientsCell: UITableViewCell{
    @IBOutlet weak var cImg: UIImageView!
    @IBOutlet weak var cName: UILabel!
}

class ClientsViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var clientsTblview: UITableView!
    
    var searchActive : Bool = false
    var filteredData = [Client]()
    
    var clients = [Client]()
    var selectedClient: Client!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        searchBar.backgroundColor = .clear
        searchBar.tintColor = .clear
        searchBar.backgroundImage = UIImage()
        
        clientsTblview.delegate = self
        clientsTblview.dataSource = self
        searchBar.backgroundImage = UIImage()

//        clients = [Client(image: "Carla", name: "Carla Hayes", email: "carla_hayes@yahoo.co.uk", phone: "07853220891"),Client(image: "Chris", name: "Chris Koors", email: "chris_koors@yahoo.co.uk", phone: "07853220892"),Client(image: "Harriet", name: "Harriet Polinski", email: "harriet_polinski@yahoo.co.uk", phone: "07853220893"),Client(image: "Jesse", name: "Jesse Michaels", email: "jesse_michaels@yahoo.co.uk", phone: "07853220894"),Client(image: "Layla", name: "Layla Hadid", email: "layla_hadid@yahoo.co.uk", phone: "07853220895"),Client(image: "Matuesz", name: "Matuesz Wilk", email: "matuesz_wilk@yahoo.co.uk", phone: "07853220896"),Client(image: "Nancy", name: "Nancy Holland", email: "nancy_holland@yahoo.co.uk", phone: "07853220897"),Client(image: "Sara", name: "Sara Larsson", email: "sara_larsson@yahoo.co.uk", phone: "07853220898"),Client(image: "Tia", name: "Tia Smith", email: "tia_smith@yahoo.co.uk", phone: "07853220899")]
        // Do any additional setup after loading the view.
        self.serverCallforClients()
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - tableview Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchActive {
            return filteredData.count
        } else {
            return clients.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "clientsCell") as! clientsCell
        var loc: Client!
        if searchActive {
            loc = filteredData[indexPath.row]
        } else {
            loc = clients[indexPath.row]
        }
        cell.cImg.kf.setImage(with: URL(string: loc.image), placeholder:#imageLiteral(resourceName: "person-24px 1"), options: nil, progressBlock: nil, completionHandler: nil)
        cell.cName.text = loc.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchActive {
            selectedClient = filteredData[indexPath.row]
        } else {
            selectedClient = clients[indexPath.row]
        }
        self.performSegue(withIdentifier: "toClientDetails", sender: nil)
    }
    
    
    //MARK: --- Search code ---
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.clientsTblview.reloadData()
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
//            let resultPredicate = NSPredicate(format: "name contains[cd] %@", searchText)
//            let data = aryTalkBloxContacts.filteredArrayUsingPredicate(resultPredicate)
//
//        searchResults = contacts.filter{($0.givenName.prefix(searchText.count) == searchText)}
        filteredData = clients.filter{($0.name.lowercased().contains(searchText.lowercased()))}
//            if(data.count > 0)
//            {
//                filteredData.removeAllObjects()
//                filteredData.addObjectsFromArray(data)
//            }
            
            

        if(searchText.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.clientsTblview.reloadData()
    }
    
    
    //MARK:- Server Calls
    func serverCallforClients(){
        showProgressOnView(self.view)
        
        NetworkManager.sharedInstance.executeGetServiceWithToken(WEB_URL.getClients,  completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    self.clients.removeAll()
                    let allClients = response!["data"].arrayValue
                    for item in allClients {
                        let client:Client = Client(image: item["image"].stringValue, name: item["name"].stringValue, email: item["email"].stringValue, mobile: item["mobile"].stringValue)
                        self.clients.append(client)
                    }
                    DispatchQueue.main.async{
                        self.clientsTblview.reloadData()
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toClientDetails" {
            let ClientDetailedVC : ClientDetailedViewController = segue.destination as! ClientDetailedViewController
            ClientDetailedVC.CLIENT = self.selectedClient
        }
    }
    

}
