//
//  EditServiceViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 23/06/21.
//

import UIKit
import SwiftyJSON

class EditServiceViewController: UIViewController, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var serviceImage: DesignableImageView!
    @IBOutlet weak var serviceTitle: DesignableTextfield!
    @IBOutlet weak var category: DesignableTextfield!
    @IBOutlet weak var descriptionFld: DesignableTextView!
    @IBOutlet weak var countLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var addOnYesBtn: DesignableButton!
    @IBOutlet weak var addOnNoBtn: DesignableButton!
    
    var isAddOn:Bool = false
    let imagePicker = UIImagePickerController()
    var service_price = 0
    var service_time = 0
    
    var service_id: Int!
    var category_name = ""
    var category_id: Int!
    var service_image: UIImage? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        descriptionFld.delegate = self
        // Do any additional setup after loading the view.
        
        priceLbl.text = "£\(service_price)"
        timeLbl.text = Utilities.shared.SecondsToHourMinString(sec: service_time)
        self.serverCallforServiceDetails(service_id: service_id)
    }
    
    func showActionSheet(){
        let actionSheet = UIAlertController.init(title: "", message: "Please select one of the following", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction.init(title: "Camera", style: .default, handler: { (action) in
            self.showCamera()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Gallery", style: .default, handler: { (action) in
            self.showPhotoLibrary()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func showCamera() {
        self.imagePicker.sourceType = .camera
        self.imagePicker.allowsEditing = true
        self.imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    func showPhotoLibrary() {
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.allowsEditing = true
        self.imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func editImage(_ sender: UIButton) {
        self.showActionSheet()
    }
    @IBAction func AddOnYes(_ sender: UIButton) {
        isAddOn = true
        addOnYesBtn.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        addOnYesBtn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        addOnYesBtn.layer.borderWidth = 0.0
        
        addOnNoBtn.backgroundColor = .clear
        addOnNoBtn.titleLabel?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        addOnNoBtn.layer.cornerRadius = 15
        addOnNoBtn.layer.borderWidth = 1.0
        addOnNoBtn.layer.borderColor = UIColor.darkGray.cgColor
    }
    @IBAction func AddOnNo(_ sender: UIButton) {
        isAddOn = false
        addOnYesBtn.backgroundColor = .clear
        addOnYesBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        addOnYesBtn.layer.cornerRadius = 15
        addOnYesBtn.layer.borderWidth = 1.0
        addOnYesBtn.layer.borderColor = UIColor.darkGray.cgColor
        
        addOnNoBtn.backgroundColor = #colorLiteral(red: 0.1353600621, green: 0.1353642344, blue: 0.1353619695, alpha: 1)
        addOnNoBtn.titleLabel?.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        addOnNoBtn.layer.borderWidth = 0.0
    }
    @IBAction func plusPrice(_ sender: UIButton) {
        service_price += 1
        priceLbl.text = "£\(service_price)"
    }
    @IBAction func minusPrice(_ sender: UIButton) {
        if service_price > 0 {
            service_price -= 1
            priceLbl.text = "£\(service_price)"
        }
    }
    @IBAction func plusTime(_ sender: UIButton) {
        service_time += 60
        timeLbl.text = Utilities.shared.SecondsToHourMinString(sec: service_time)
    }
    @IBAction func minusTime(_ sender: UIButton) {
        if service_time > 0 {
            service_time -= 60
            timeLbl.text = Utilities.shared.SecondsToHourMinString(sec: service_time)
        }
    }
    @IBAction func saveService(_ sender: UIButton) {
//        if service_image == nil {
//            self.view.makeToast("Please select an image", duration: 2.0, position: .bottom)
//        } else {
            self.serverCalltoUpdateServices()
//        }
    }
    
    //MARK:- UIImagePickerControlloller Delegates
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.serviceImage.image = image
            self.service_image = image
//            let imageData = image.pngData()
            self.serverCalltoUpdateServiceImage()
        }
        self.dismiss(animated: true, completion: nil)
    }
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//// Local variable inserted by Swift 4.2 migrator.
////let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
////
////        if let image = infoconvertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage) as? UIImage {
////            self.imguser.image = image
////            let imageData = image.pngData()
////            self.serverCallForUpdateProfilePhoto(imageData!)
////
////        }
//
//        self.dismiss(animated: true, completion: nil)
//    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - TextView Delegates
    func textViewDidChange(_ textView: UITextView) {
        let length = textView.text.count
        countLbl.text = "\(length)" + "/150"
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text.count == 0 {
            if textView.text.count != 0 {
                return true
            }
        } else if textView.text.count > 149 {
            return false
        }
        return true
    }
    
    //MARK:- Server Calls
    func serverCallforServiceDetails(service_id: Int){
        showProgressOnView(self.view)
        let urlString = WEB_URL.MainUrl + String(format: "api/edit/%d/service", service_id)

        NetworkManager.sharedInstance.executeGetServiceWithToken(urlString,  completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)

            if success == true {
                if response!["status"].stringValue  == "success"{
                    let allDetails = response!["data"].dictionaryValue
                    DispatchQueue.main.async{
                        self.serviceTitle.text = allDetails["title"]?.stringValue
                        self.descriptionFld.text = allDetails["description"]?.stringValue
                        self.isAddOn = ((allDetails["require_add_on"]?.boolValue) != nil)
                        let timeStr = allDetails["time"]?.stringValue
                        self.service_time = Utilities.shared.hourMinutesSecondsToSecondsInt(minSec: timeStr!)
                        self.timeLbl.text = Utilities.shared.SecondsToHourMinString(sec: self.service_time)
                        let category = allDetails["category"]?.dictionaryValue
                        self.category_id = category!["id"]?.intValue
                        self.category_name = category!["name"]!.stringValue
                        self.category.text = category!["name"]!.stringValue
                        self.serviceImage.kf.setImage(with: URL(string: allDetails["image"]!.stringValue), placeholder:#imageLiteral(resourceName: "icons8-full_image"), options: nil, progressBlock: nil, completionHandler: nil)
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })
    }
    
    func serverCalltoUpdateServices(){
        showProgressOnView(self.view)
        let urlString = WEB_URL.MainUrl + String(format: "api/update/%d/service", service_id)
        let params:[String: Any] = ["title": serviceTitle.text!,
                                    "category_id":category_id!,
                                    "description": descriptionFld.text!,
                                    "require_add_on":isAddOn,
                                    "price":service_price,
                                    "time":Utilities.shared.secondsToHourMinutesSeconds(seconds: service_time)]
//        NetworkManager.sharedInstance.postImageAPI(image: self.service_image!, imageName: "profile_img", parameter: params, url: WEB_URL.add_service, completionHandler: { (success:Bool, response:JSON?) in
        
        NetworkManager.sharedInstance.executeServiceWithToken(urlString, postParameters: params, completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    DispatchQueue.main.async {
                        self.view.makeToast(response!["message"].stringValue, duration: 2.0, position: .bottom)
                    }
                } else {
//                    let error =
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })
    }
    func serverCalltoUpdateServiceImage(){
        showProgressOnView(self.view)
        let urlString = WEB_URL.MainUrl + String(format: "api/update/%d/service/profile", service_id)
        NetworkManager.sharedInstance.postImageAPI(image: self.service_image!, imageName: "service_img", parameter: [:], url: urlString, completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    DispatchQueue.main.async {
                        self.view.makeToast(response!["message"].stringValue, duration: 2.0, position: .bottom)
                    }
                } else {
                    //                    let error =
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })
    }
//    func serverCalltoUpdateServices(){
//
//        showProgressOnView(self.view)
//        let params:[String: Any] = ["title": serviceTitle.text!,
//                                    "category_id":category_id!,
//                                    "description": descriptionFld.text!,
//                                    "require_add_on":isAddOn,
//                                    "price":service_price,
//                                    "time":Utilities.shared.secondsToHourMinutesSeconds(seconds: service_time)]
//
//        NetworkManager.sharedInstance.executeServiceWithToken(WEB_URL.add_service, postParameters: params, completionHandler: { (success:Bool, response:JSON?) in
//            hideProgressOnView(self.view)
//
//            if success == true {
//                if response!["status"].stringValue  == "success"{
//                    DispatchQueue.main.async{
//                        self.view.makeToast(response!["message"].stringValue, duration: 2.0, position: .bottom)
//                    }
//                } else {
//                    self.view.makeToast("error", duration: 2.0, position: .bottom)
//                }
//            } else {
//                self.view.makeToast("Some error occured.", duration: 2.0, position: .bottom)
//            }
//        })
//    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
