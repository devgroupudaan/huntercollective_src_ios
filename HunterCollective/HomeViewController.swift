//
//  HomeViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 03/06/21.
//

import UIKit
import Toast_Swift

class HomeViewController: UIViewController {// sideMenu_View  tabContainerView
    @IBOutlet weak var sideMenu_View: UIView!
    @IBOutlet weak var tabContainerView: UIView!
    
        //MARK: - Variables
        
        var initialPos: CGPoint?
        var touchPos: CGPoint?
        let blackTransparentViewTag = 02271994
        var openFlag: Bool = false
        
        //MARK: - ViewController Variables
        lazy var frontVC: UITabBarController? = {
            let front = self.storyboard?.instantiateViewController(withIdentifier: "FrontTabbar")
            return front as? UITabBarController
        }()
        
        lazy var rearVC: UIViewController? = {
            let rear = self.storyboard?.instantiateViewController(withIdentifier: "rearVC")
            return rear
        }()
        
        override func viewDidLoad() {
            super.viewDidLoad()
           
            displayTabbar()
            addShadowToView()
            setUpNotifications()
            setUpGestures()
            
            NotificationCenter.default.addObserver(self, selector: #selector(loggedIn), name: Notification.Name("loggedIn"), object: nil)
            // Do any additional setup after loading the view, typically from a nib.
        }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        if let fullName  = UserDefaults.standard.value(forKey: "fullName"){
//            self.view.makeToast(String(format: "Welcome %@", fullName as! CVarArg), duration: 2.0, position: .bottom)
//        }
    }
    @objc func loggedIn(){
                if let fullName  = UserDefaults.standard.value(forKey: "fullName"){
                    self.view.makeToast(String(format: "Welcome %@", fullName as! CVarArg), duration: 2.0, position: .bottom)
                }
    }
        func setUpNotifications(){
            let notificationOpenOrCloseSideMenu = Notification.Name("notificationOpenOrCloseSideMenu")
            NotificationCenter.default.addObserver(self, selector: #selector(openOrCloseSideMenu), name: notificationOpenOrCloseSideMenu, object: nil)
            
            let notificationCloseSideMenu = Notification.Name("notificationCloseSideMenu")
            NotificationCenter.default.addObserver(self, selector: #selector(closeSideMenu), name: notificationCloseSideMenu, object: nil)
            
            let notificationCloseSideMenuWithoutAnimation = Notification.Name("notificationCloseSideMenuWithoutAnimation")
            NotificationCenter.default.addObserver(self, selector: #selector(closeWithoutAnimation), name: notificationCloseSideMenuWithoutAnimation, object: nil)
            
            let notificationChangeFlag = Notification.Name("notificationChangeFlag")
            NotificationCenter.default.addObserver(self, selector: #selector(changeFlag), name: notificationChangeFlag, object: nil)
        }
        
        func setUpGestures(){
            let panGestureContainerView = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(panGesture:)))
            self.view.addGestureRecognizer(panGestureContainerView)
        }

        
        //MARK: - UISetup
        func displayTabbar(){
            // To display Tabbar in tabContainerView
            if let vc = frontVC {
                self.addChild(vc)
                vc.didMove(toParent: self)
                
                vc.view.frame = self.tabContainerView.bounds
                self.tabContainerView.addSubview(vc.view)
              
            }
        }
        
        func displaySideMenu(){
            // To display RearViewController in Side Menu View
            if !self.children.contains(rearVC!){
                if let vc = rearVC {
                    self.addChild(vc)
                    vc.didMove(toParent: self)
                    
                    vc.view.frame = self.sideMenu_View.bounds
                    self.sideMenu_View.addSubview(vc.view)
                  
                }

            }
        }
        
        //MARK: - Shadow View
        func addBlackTransparentView() -> UIView{
            //Black Shadow on MainView(i.e on TabBarController) when side menu is opened.
            let blackView = self.tabContainerView.viewWithTag(blackTransparentViewTag)
            if blackView != nil{
                return blackView!
            }else{
                let sView = UIView(frame: self.tabContainerView.bounds)
                sView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                sView.tag = blackTransparentViewTag
                sView.alpha = 0
                sView.backgroundColor = UIColor.black
                let recognizer = UITapGestureRecognizer(target: self, action: #selector(closeSideMenu))
                sView.addGestureRecognizer(recognizer)
                return sView
            }
            
            
        }
        
        func addShadowToView(){
            //Gives Illusion that main view is above the side menu
            self.tabContainerView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
            self.tabContainerView.layer.shadowOffset = CGSize(width: -1, height: 1)
            self.tabContainerView.layer.shadowRadius = 1
            self.tabContainerView.layer.shadowOpacity = 1
            self.tabContainerView.layer.borderColor = UIColor.lightGray.cgColor
            self.tabContainerView.layer.borderWidth = 0.2
        }
        
       
        
        
        //MARK: - Selector Methods
    @objc func changeFlag() {
        self.openFlag = false
    }
    @objc func openOrCloseSideMenu(){
            //Opens or Closes Side Menu On Click of Button
            if openFlag{
                //This closes Rear View
                let blackTransparentView = self.view.viewWithTag(self.blackTransparentViewTag)
                UIView.animate(withDuration: 0.3, animations: {
                    self.tabContainerView.frame = CGRect(x: 0, y: 0, width: self.tabContainerView.frame.size.width, height: self.tabContainerView.frame.size.height)
                    blackTransparentView?.alpha = 0
                    
                }) { (_) in
                    blackTransparentView?.removeFromSuperview()
                    self.openFlag = false
                }
            }else{
                //This opens Rear View
                UIView.animate(withDuration: 0.0, animations: {
                    self.displaySideMenu()
                    let blackTransparentView = self.addBlackTransparentView()

                    self.tabContainerView.addSubview(blackTransparentView)
                    
                }) { (_) in
                    UIView.animate(withDuration: 0.3, animations: {
                    
                    self.addBlackTransparentView().alpha = self.view.bounds.width * 00.5/(self.view.bounds.width * 1.8)
                    self.tabContainerView.frame = CGRect(x: self.tabContainerView.bounds.size.width * 00.5, y: 0, width: self.tabContainerView.frame.size.width, height: self.tabContainerView.frame.size.height)
                        }) { (_) in
                        self.openFlag = true
                        }
                }
                
            }
       
        }
        
        @objc func closeSideMenu(){
            //To close Side Menu
            let blackTransparentView = self.view.viewWithTag(self.blackTransparentViewTag)
                UIView.animate(withDuration: 0.3, animations: {
                    self.tabContainerView.frame = CGRect(x: 0, y: 0, width: self.tabContainerView.frame.size.width, height: self.tabContainerView.frame.size.height)
                    blackTransparentView?.alpha = 0.0
                    
                }) { (_) in
                    blackTransparentView?.removeFromSuperview()
                    self.openFlag = false
                }
        
        }
        
        @objc func closeWithoutAnimation(){
            //To close Side Menu without animation
            let blackTransparentView = self.view.viewWithTag(self.blackTransparentViewTag)
            blackTransparentView?.alpha = 0
            blackTransparentView?.removeFromSuperview()
           self.tabContainerView.frame = CGRect(x: 0, y: 0, width: self.tabContainerView.frame.size.width, height: self.tabContainerView.frame.size.height)
           self.openFlag = false
        }
        
        
        
        //MARK: - Pan Gesture
        @objc func handlePanGesture(panGesture: UIPanGestureRecognizer){
            //For Pan Gesture
            
            touchPos = panGesture.location(in: self.view)
            let translation = panGesture.translation(in: self.view)
            
            //Add BlackShadowView
            let blackTransparentView = self.addBlackTransparentView()
            self.tabContainerView.addSubview(blackTransparentView)
            
            
            if panGesture.state == .began{
                initialPos = touchPos
            }else if panGesture.state == .changed{
                let touchPosition = self.view.bounds.width * 00.5
                if (initialPos?.x)! > touchPosition && openFlag{
                    //To Close Rear View
                    if self.tabContainerView.frame.minX > 0{
                        self.tabContainerView.center = CGPoint(x: self.tabContainerView.center.x + translation.x, y: self.tabContainerView.bounds.midY)
                        panGesture.setTranslation(CGPoint.zero, in: self.view)
                        
                        blackTransparentView.alpha = self.tabContainerView.frame.minX/(self.view.bounds.width * 1.8)
                    }
                }else if !openFlag{
                    //To Open Rear View
                    if translation.x > 0.0{
                        displaySideMenu()
                        
                        self.tabContainerView.center = CGPoint(x: translation.x + self.tabContainerView.center.x, y: self.tabContainerView.bounds.midY)
                        panGesture.setTranslation(CGPoint.zero, in: self.view)
                        
                        blackTransparentView.alpha = self.tabContainerView.frame.minX/(self.view.bounds.width * 1.8)
                    }
                    
                }
                
            }else if panGesture.state == .ended{
                if self.tabContainerView.frame.minX > self.view.frame.midX{
                    //Opens Rear View
                    UIView.animate(withDuration: 0.2, animations: {
                        
                        self.tabContainerView.frame = CGRect(x: self.view.frame.width * 00.5, y: 0, width: self.tabContainerView.bounds.width, height: self.tabContainerView.bounds.height)
                        blackTransparentView.alpha = self.tabContainerView.frame.minX/(self.view.bounds.width * 1.8)
                    }) { (_) in
                        self.openFlag = true
                    }
                }else{
                    //Closes Rear View
                    UIView.animate(withDuration: 0.2, animations: {
                        self.tabContainerView.center = CGPoint(x: self.view.center.x, y: self.tabContainerView.bounds.midY)
                        blackTransparentView.alpha = 0
                    }) { (_) in
                        blackTransparentView.removeFromSuperview()
                        self.openFlag = false
                       
                    }
                }
            }
        }
        
    }

class HamburgerMenu{
    //Class To Implement Easy Functions To Open Or Close RearView
    //Make object of this class and call functions
    func triggerSideMenu(){
        let notificationOpenOrCloseSideMenu = Notification.Name("notificationOpenOrCloseSideMenu")
        NotificationCenter.default.post(name: notificationOpenOrCloseSideMenu, object: nil)
    }
    
    func closeSideMenu(){
        let notificationCloseSideMenu = Notification.Name("notificationCloseSideMenu")
        NotificationCenter.default.post(name: notificationCloseSideMenu, object: nil)
    }
    
    func closeSideMenuWithoutAnimation(){
        let notificationCloseSideMenuWithoutAnimation = Notification.Name("notificationCloseSideMenuWithoutAnimation")
        NotificationCenter.default.post(name: notificationCloseSideMenuWithoutAnimation, object: nil)
    }
    
}
 
