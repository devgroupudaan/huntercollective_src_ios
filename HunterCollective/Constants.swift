//
//  Constants.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 10/06/21.
//

import Foundation
import MBProgressHUD

struct WEB_URL {
    
static let MainUrl = "https://huntercollective-staging.udaantechnologies.com/"

    static let login:String            = "\(MainUrl)api/login" as String
    static let google_auth:String            = "\(MainUrl)api/google/auth" as String
    static let register:String            = "\(MainUrl)api/register" as String
    static let deleteAccount:String            = "\(MainUrl)api/delete/account" as String
    static let resetPassword:String            = "\(MainUrl)api/password/email" as String
    static let getLocations:String            = "\(MainUrl)api/locations" as String
    static let addLocations:String            = "\(MainUrl)api/add/locations" as String
    static let getMyLocations:String            = "\(MainUrl)api/my/locations" as String
    static let getClients:String            = "\(MainUrl)api/users" as String
    static let account_details:String            = "\(MainUrl)api/account/details" as String
    static let account_update:String            = "\(MainUrl)api/account/update" as String
    static let update_profile:String            = "\(MainUrl)api/account/update/profile" as String
    static let tags:String            = "\(MainUrl)api/tags" as String
    static let services:String            = "\(MainUrl)api/services" as String
    static let categories:String            = "\(MainUrl)api/categories" as String
    static let my_services:String            = "\(MainUrl)api/my/services" as String
    static let add_service:String            = "\(MainUrl)api/add/service" as String
    static let resend_verification:String            = "\(MainUrl)api/resend/verification" as String
    static let clients_chat_lists:String            = "\(MainUrl)api/clients/chat/lists" as String
    static let broadcast_all_message:String            = "\(MainUrl)api/broadcast/all/message" as String
    static let community_messages:String            = "\(MainUrl)api/community/messages" as String
    static let chair_booking:String            = "\(MainUrl)api/chair/booking" as String
    static let delete_booking:String            = "\(MainUrl)api/delete/booking" as String
    static let available_chair:String            = "\(MainUrl)api/available/chair" as String
    static let get_booking:String            = "\(MainUrl)api/get/booking" as String
    static let get_booking_with_date:String            = "\(MainUrl)api/get/booking/date" as String//get/appointment/date
    static let get_chair_booking_month:String            = "\(MainUrl)api/get/chair/booking/month" as String
    static let get_appointment_date:String            = "\(MainUrl)api/get/appointment/date" as String
    static let get_appointment_month:String            = "\(MainUrl)api/get/appointment/month" as String
    static let new_appointment:String            = "\(MainUrl)api/new/appointment" as String
    static let get_notes:String            = "\(MainUrl)api/get/notes" as String
    static let create_notes:String            = "\(MainUrl)api/create/notes" as String
}

let screenSize = UIScreen.main.bounds
let screenWidth = screenSize.width
let screenHeight = screenSize.height
//let window = UIApplication.shared.keyWindow!

let IS_IPHONE_5_SE = UIScreen.main.bounds.height == 568
let IS_IPHONE_6_7_8_SE2 = UIScreen.main.bounds.height == 667
let IS_IPHONE_6P_7P_8P = UIScreen.main.bounds.height == 736
let IS_IPHONE_X_Xs_11_11Pro = UIScreen.main.bounds.height == 812
let IS_IPHONE_XsMax_Xr_11ProMax = UIScreen.main.bounds.height == 896



//MARK:- MBProgressHUD Methods

func showProgressOnView(_ view:UIView) {
    let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
    loadingNotification.mode = MBProgressHUDMode.indeterminate
    loadingNotification.contentColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
    loadingNotification.bezelView.color = .clear
    loadingNotification.bezelView.style = .blur
    loadingNotification.bezelView.alpha = 0.3
//    loadingNotification.minSize = CGSize(width: UIScreen.main.bounds.width * 1.1, height: UIScreen.main.bounds.height  * 1.1)
//    loadingNotification.bounds = UIScreen.main.bounds
//    loadingNotification.bezelView.isHidden = true
//    loadingNotification.backgroundColor = .red
//    loadingNotification.backgroundView.color = .yellow
//    loadingNotification.label.text = "Loading..."
    
}

func hideProgressOnView(_ view:UIView) {
    MBProgressHUD.hide(for: view, animated: true)
}

struct HTTP_STATUS_CODE {
    static let TOKEN_EXPIRE:NSInteger  = 401
    static let UPDATE_REQUIRED:NSInteger  = 406
    static let INVALID:NSInteger  = 400
    static let SUCCESS:NSInteger  = 200
    static let NO_DATA:NSInteger  = 204
    static let SERVER_ERROR:NSInteger  = 500
    static let SHOW_LOADER:NSInteger  = 226
}
// MARK: - CheckError
struct ErroCode  {
    static let Succes:Int = 200
    static let Fail:Int   = 501
    static let Pending:Int = 406
    static let NoData:Int = 400
}
