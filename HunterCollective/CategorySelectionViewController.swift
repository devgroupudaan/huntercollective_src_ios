//
//  CategorySelectionViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 11/06/21.
//

import UIKit
import SwiftyJSON



class categoryCell: UITableViewCell{
    @IBOutlet weak var bgView: DesignableView!
    @IBOutlet weak var category_Img: DesignableImageView!
    @IBOutlet weak var cTitle: UILabel!
    @IBOutlet weak var cDetails: UILabel!
    
}

class CategorySelectionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var categoryTbl: UITableView!
    
    var categories = [Category]()
    var selectedCategoryID: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryTbl.delegate = self
        categoryTbl.dataSource = self
        
//        categories = [Category(image: "Category1", name: "Hair", description: "Blow drys and styling for her you'll want to whip.", status: false),
//                      Category(image: "Category2", name: "Barber", description: "Professionals eager to give you that fresh skin fade feel.", status: false),
//                      Category(image: "Category3", name: "Facials", description: "Refresh your skin and relax with a personalised facial treatment.", status: false),
//                      Category(image: "Category4", name: "Make up", description: "A pro make-up artist for any occassion eager to make you look your best.", status: false),
//                      Category(image: "Category5", name: "Pedicure", description: "Quality varnishes to make you look good from top to bottom.", status: false),
//                      Category(image: "Category6", name: "Waxing", description: "Give your skin that silky smooth feet with a personalised wax treatment.", status: false),
//                      Category(image: "Category7", name: "Eyelashes", description: "Treat yourself with a personalised lash experience.", status: false),
//                      Category(image: "Category8", name: "Eyebrows", description: "Shoose between a vriety of services for your brows.", status: false)]
//        self.serverCallforCategories()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.serverCallforCategories()
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - tableview Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell") as! categoryCell
        let category = categories[indexPath.row]
//        cell.category_Img.image = UIImage(named: category.image)
        cell.category_Img.kf.setImage(with: URL(string: category.image), placeholder:#imageLiteral(resourceName: "person-24px 1"), options: nil, progressBlock: nil, completionHandler: nil)
        cell.cTitle.text = category.name
        cell.cDetails.text = category.description
        if category.id == selectedCategoryID {
            cell.bgView.alpha = 0.8
        } else {
            cell.bgView.alpha = 0.4
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let category = categories[indexPath.row]
        selectedCategoryID = category.id
        self.performSegue(withIdentifier: "toServiceDetails", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        let cell = tableView.cellForRow(at: indexPath) as! categoryCell
        cell.bgView.alpha = 0.4
        return indexPath
    }
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! categoryCell
        cell.bgView.alpha = 0.8
    }
    
    //MARK:- Server Calls
    func serverCallforCategories(){
        showProgressOnView(self.view)
        
        NetworkManager.sharedInstance.executeGetServiceWithToken(WEB_URL.categories,  completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    self.categories.removeAll()
                    let allServices = response!["data"].arrayValue
                    for item in allServices {
                        let category:Category = Category(id: item["id"].intValue, image: item["image"].stringValue, name: item["name"].stringValue, description: item["description"].stringValue, status: item["status"].boolValue)
                        self.categories.append(category)
                    }
                    DispatchQueue.main.async{
                        self.categoryTbl.reloadData()
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
   
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toServiceDetails" {
            let ServiceSelectionVC : ServiceSelectionViewController = segue.destination as! ServiceSelectionViewController
            ServiceSelectionVC.CategoryID = self.selectedCategoryID
        }
    }
    

}
