//
//  ClientDetailedViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 09/06/21.
//

import UIKit
class sCell: UITableViewCell {
    @IBOutlet weak var sName: UILabel!
    @IBOutlet weak var sPrice: UILabel!
}

class pastAppointmentCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var monthlbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var client_name: UILabel!
    @IBOutlet weak var serviceTblView: UITableView!
    @IBOutlet weak var rescheduleBtn: DesignableButton!
    @IBOutlet weak var cancelBtn: DesignableButton!
    
    var numberofCells = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        serviceTblView.delegate = self
        serviceTblView.dataSource = self
        // Initialization code
        serviceTblView.reloadData()
    }
    func configure(at Index: Int){
        numberofCells = Index
        serviceTblView.reloadData()
    }
    
    // MARK: - tableview Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberofCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sCell") as! sCell
            
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

class ClientDetailedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var clientName: UILabel!
    @IBOutlet weak var client_image: DesignableImageView!
    @IBOutlet weak var pastAppointmentsTable: UITableView!
    
    var CLIENT: Client!
    var appointmentType = "past"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pastAppointmentsTable.delegate = self
        pastAppointmentsTable.dataSource = self
        
        clientName.text = CLIENT.name
        client_image.kf.setImage(with: URL(string: CLIENT.image), placeholder:#imageLiteral(resourceName: "person-24px 1"), options: nil, progressBlock: nil, completionHandler: nil)

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func typeChanged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0{
            appointmentType = "past"
        } else {
            appointmentType = "upcoming"
        }
        pastAppointmentsTable.reloadData()
    }
    
    // MARK: - tableview Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if appointmentType == "past"{
            return 2
        } else {
            return 4
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pastAppointmentCell") as! pastAppointmentCell
        cell.configure(at: indexPath.row)
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(180 + ((indexPath.row + 1) * 24))
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
