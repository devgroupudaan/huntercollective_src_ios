//
//  ServiceSelectionViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 11/06/21.
//

import UIKit
import SwiftyJSON

//struct subService {
//    var title: String
//    var duration: String
//}

class serviceCell: UITableViewCell{
    @IBOutlet weak var service_title: UILabel!
    @IBOutlet weak var service_duration: UILabel!
    @IBOutlet weak var addBtn: DesignableButton!
}

class ServiceSelectionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var serviceTable: UITableView!
    
    var subServices = [Service]()
    var CategoryID: Int!
    var selectedServiceIDs = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        serviceTable.delegate = self
        serviceTable.dataSource = self
        
//        subServices = [subService(title: "Consultation", duration: "Estimated Time - 15 mins"),
//                       subService(title: "Permanent/Semi", duration: "Estimated Time - 1hr"),
//                       subService(title: "T-section", duration: "Estimated Time - 1hr"),
//                       subService(title: "Toner", duration: "Estimated Time - 15 mins"),
//                       subService(title: "Bleach Roots and Toner", duration: "Estimated Time - 1hr 30 mins"),
//                       subService(title: "Half Head Highlisghts", duration: "Estimated Time - 1hr 30 mins"),
//                       subService(title: "Full Head Highlights", duration: "Estimated Time - 1hr 30 mins"),
//                       subService(title: "Ombre, Dip Dye and Balayage", duration: "Estimated Time - 1hr 30 mins"),
//                       subService(title: "Hair Up - Specail Events", duration: "Price dependent on consultation")]
        // Do any additional setup after loading the view.
        self.serverCallforServices()
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func confirmAddService(_ sender: UIButton) {
        self.serverCalltoAddServices()
    }
    @IBAction func addService(_ sender: UIButton) {
        let subservice = subServices[sender.tag]
        let subserviceID = subservice.id
        if selectedServiceIDs.contains(subserviceID) {
            let index = selectedServiceIDs.firstIndex(of: subserviceID)
            selectedServiceIDs.remove(at: index!)
        } else {
            selectedServiceIDs.append(subserviceID)
        }
        serviceTable.reloadData()
    }
    
    // MARK: - tableview Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subServices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "serviceCell") as! serviceCell
        let subservice = subServices[indexPath.row]
        cell.service_title.text = subservice.title
        cell.service_duration.text = Utilities.shared.hourMinutesSecondsToSeconds(minSec: subservice.time)
        cell.addBtn.tag = indexPath.row
        if selectedServiceIDs.contains(subservice.id) {
            cell.addBtn.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            cell.addBtn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        } else {
            cell.addBtn.backgroundColor = .clear
            cell.addBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    //MARK:- Server Calls
    func serverCallforServices(){
        showProgressOnView(self.view)
        
        NetworkManager.sharedInstance.executeGetServiceWithToken(WEB_URL.services,  completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    self.subServices.removeAll()
                    let allServices = response!["data"].arrayValue
                    for item in allServices {
                        let service:Service = Service(id: item["id"].intValue, title: item["title"].stringValue, description: item["description"].stringValue, price: item["price"].doubleValue, require_add_on: item["require_add_on"].boolValue, time: item["time"].stringValue)
                        self.subServices.append(service)
                    }
                    DispatchQueue.main.async{
                        self.serviceTable.reloadData()
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })
    }
    
    func serverCalltoAddServices(){
        
        showProgressOnView(self.view)
        let params:[String: Any] = ["category_id": self.CategoryID!,
                                    "service_id":self.selectedServiceIDs]
        
        NetworkManager.sharedInstance.executeServiceWithToken(WEB_URL.add_service, postParameters: params, completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    self.selectedServiceIDs.removeAll()
                    DispatchQueue.main.async{
                        self.view.makeToast(response!["message"].stringValue, duration: 2.0, position: .bottom)
                        self.serviceTable.reloadData()
                        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            } else {
                self.view.makeToast("Some error occured.", duration: 2.0, position: .bottom)
            }
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
