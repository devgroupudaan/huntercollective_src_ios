//
//  AppDelegate.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 03/06/21.
//

import UIKit
import GoogleSignIn
import Firebase

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        FirebaseApp.configure()
        GIDSignIn.sharedInstance().clientID = "403667082409-njm6s5rt1f7221m59pai43t3v2q0d18k.apps.googleusercontent.com"
        
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        if ( UserDefaults.standard.object(forKey: "isLoggedIn") == nil || UserDefaults.standard.object(forKey: "isLoggedIn") as! Bool != true){

            let vc  = storyboard.instantiateViewController(withIdentifier: "loginVC") as! SignInViewController
            self.window?.rootViewController = vc
        }
        else {
            let tokenStr = UserDefaults.standard.value(forKey: "token")
            debugPrint("TOKEN:",tokenStr!)
            let vc  = storyboard.instantiateViewController(withIdentifier: "homeVC") as! HomeViewController
            self.window?.rootViewController = vc
        }
        
        return true
    }

//    // MARK: UISceneSession Lifecycle
//
//    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
//        // Called when a new scene session is being created.
//        // Use this method to select a configuration to create the new scene with.
//        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
//    }
//
//    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
//        // Called when the user discards a scene session.
//        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
//        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
//    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {

    let isGoogle = GIDSignIn.sharedInstance()?.handle(url)//handle(url, sourceApplication: sourceApplication, annotation: annotation)

        return isGoogle!

    }

}

