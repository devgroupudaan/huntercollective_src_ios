//
//  AppointmentViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 03/06/21.
//

import UIKit
import SwiftyJSON
import Toast_Swift
import FSCalendar
import CalendarKit
import EventKit
import EventKitUI

class AppointmentViewController: UIViewController,UIGestureRecognizerDelegate {

    @IBOutlet weak var locationTitle: UILabel!
    @IBOutlet weak var calender: FSCalendar!
    @IBOutlet weak var dayView: DayView!
    @IBOutlet weak var headerLbl: UILabel!
    
    @IBOutlet weak var summaryView: UIView!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var appointmentDateLbl: UILabel!
    @IBOutlet weak var serviceslbl: UILabel!
    @IBOutlet weak var calenderSuperviewHeight: NSLayoutConstraint!
    //    var events = [Event]()
    
    var selectedDate = Date()
    var selectedDateStr = ""
    var myLocations = [MyLocation]()
    
    var appointments = [Appointment]()
    var monthAppointments = [Appointment]()
    var selectedAppointment: Appointment!
    
    var monthBookings = [BookData]()
    var Bookings = [BookData]()
    
    private var currentPage: Date?
    private lazy var today: Date = {
        return Date()
    }()
    private lazy var dateFormatter: DateFormatter = {
        let df = DateFormatter()
//        df.locale = Locale(identifier: "ko_KR")
        df.dateFormat = "MMMM yyyy"
        return df
    }()
    private lazy var dateFormatter2: DateFormatter = {
        let df = DateFormatter()
//        df.locale = Locale(identifier: "ko_KR")
        df.dateFormat = "yyyy-MM-dd"
        return df
    }()
    
    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calender, action: #selector(self.calender.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
    }()

   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addGestureRecognizer(self.scopeGesture)
//        self.tableView.panGestureRecognizer.require(toFail: self.scopeGesture)
        let scope: FSCalendarScope = .month
        self.calender.setScope(scope, animated: false)
        
        self.view.setNeedsLayout()
        dayView.isHeaderViewVisible = false
        dayView.delegate = self
        dayView.dataSource = self
        dayView.autoScrollToFirstEvent = true
        
        var style = CalendarStyle()
        style.timeline.eventGap = 10
        dayView.updateStyle(style)
        
        dayView.reloadData()
        
        self.selectedDateStr = self.dateFormatter2.string(from: Date())
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(LocationChanged), name: Notification.Name("locationChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LocationAdded), name: Notification.Name("locationAdded"), object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        if let selectedLocation  = UserDefaults.standard.value(forKey: "selectedLocation") {
//            locationTitle.text = selectedLocation as? String
//        } else {
//            locationTitle.text = ""
////            self.view.makeToast("No Location added yet.", duration: 1.5, position: .bottom)
//        }
        setCalendar()
        serverCallUserDetails()
        serverCallforMylocations()
//        serverCallforMonthAppointments()
        self.calender.select(Date())
//        serverCallforBooking()
    }
    @IBAction func closeSummary(_ sender: UIButton) {
        self.summaryView.isHidden = true
    }
    @IBAction func editAppointment(_ sender: UIButton) {
        self.performSegue(withIdentifier: "toEditAppointment", sender: nil)
    }
    @IBAction func contactClient(_ sender: UIButton) {
    }
    @IBAction func clientNoShow(_ sender: UIButton) {
    }
    @IBAction func completePayment(_ sender: UIButton) {
    }
    
    @objc func LocationChanged(){
        self.calender.select(Date())
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM"
//        let currentMonth = formatter.string(from: Date())
//        self.serverCallforMonthAppointments(month: currentMonth)
//        self.serverCallforMonthChairBookings(month: currentMonth)
        serverCallforMylocations()
        if let selectedLocation  = UserDefaults.standard.value(forKey: "selectedLocation") {
            locationTitle.text = selectedLocation as? String
        } else {
            locationTitle.text = ""
//            self.view.makeToast("No Location added yet.", duration: 1.5, position: .bottom)
        }
    }
    
    @objc func LocationAdded(){
        serverCallforMylocations()
    }
    
    private func scrollCurrentPage(isPrev: Bool) {
        let cal = Calendar.current
        var dateComponents = DateComponents()
        dateComponents.month = isPrev ? -1 : 1
        self.currentPage = cal.date(byAdding: dateComponents, to: self.currentPage ?? self.today)
        self.calender.setCurrentPage(self.currentPage!, animated: true)
        
    }

    @IBAction func menuBtnAction(_ sender: UIButton) {
        HamburgerMenu().triggerSideMenu()
    }
    @IBAction func prevBtnTapped(_ sender: UIButton) {
        scrollCurrentPage(isPrev: true)
    }
    @IBAction func nextbtnTapped(_ sender: UIButton) {
        scrollCurrentPage(isPrev: false)
    }
    
    //MARK:- Server Calls
    func serverCallUserDetails(){
        showProgressOnView(self.view)
        
        NetworkManager.sharedInstance.executeGetServiceWithToken(WEB_URL.account_details,  completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    
                    let user_detail = response!["data"][0].dictionaryValue
                    let tags = user_detail["tags"]!.arrayValue
                    var addedTgs: [Tag] = []
                    for tag in tags{
                        let tg = Tag(id: tag["id"].intValue, name: tag["name"].stringValue)
                        addedTgs.append(tg)
                    }
                    let UserDetails = UserDetail(name: user_detail["name"]!.stringValue, email: user_detail["email"]!.stringValue, image: user_detail["image"]!.stringValue, mobile: user_detail["mobile"]!.stringValue, date_o_birth: user_detail["date_o_birth"]!.stringValue, address: user_detail["address"]!.stringValue, introduction: user_detail["introduction"]!.stringValue, accept_client: user_detail["accept_client"]!.boolValue, tags: addedTgs )
                    
                    if let encodedCurrenUser = try? JSONEncoder().encode(UserDetails) {
                        let defaults = UserDefaults.standard
                        defaults.set(encodedCurrenUser, forKey: "currentUser")
                    }
                } else {
                    self.view.makeToast(response!["message"].stringValue, duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
    
    func serverCallforMylocations(){
        showProgressOnView(self.view)
        
        NetworkManager.sharedInstance.executeGetServiceWithToken(WEB_URL.getMyLocations,  completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    self.myLocations.removeAll()
                    var myLocationIDs = [Int]()
                    var myLocationNames = [String]()
                    let allLocations = response!["data"].arrayValue
                    for item in allLocations {
                        let location:MyLocation = MyLocation(id: item["id"].intValue, name: item["name"].stringValue, address1: item["address1"].stringValue, city: item["city"].stringValue, status: item["status"].stringValue, country: item["country"].stringValue)
                        myLocationIDs.append(item["id"].intValue)
                        myLocationNames.append(item["name"].stringValue)
                        self.myLocations.append(location)
                    }
                    UserDefaults.standard.setValue(myLocationIDs, forKey: "allLocations")
                    UserDefaults.standard.setValue(myLocationNames, forKey: "allLocationNames")
                    
                    if let selectedLocation  = UserDefaults.standard.value(forKey: "selectedLocation") {
                        if !myLocationNames.contains(selectedLocation as! String){
                            if myLocationNames.count > 0 {
                                self.locationTitle.text = myLocationNames.first
                                UserDefaults.standard.setValue(myLocationNames.first, forKey: "selectedLocation")
                            } else {
                                self.locationTitle.text = ""
                                UserDefaults.standard.setValue("", forKey: "selectedLocation")
                            }
                        } else {
                            self.locationTitle.text = selectedLocation as? String
                        }
                    } else {
                        self.locationTitle.text = myLocationNames.first
                        UserDefaults.standard.setValue(myLocationNames.first, forKey: "selectedLocation")
                    }
                    DispatchQueue.main.async{
                        self.serverCallforAppointments()
                        self.serverCallforChairBookings()
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM"
                        let currentMonth = formatter.string(from: Date())
                        self.serverCallforMonthAppointments(month: currentMonth)
                        self.serverCallforMonthChairBookings(month: currentMonth)
                    }
                    let notificationLocationAdded = Notification.Name("locationsUpdated")
                    NotificationCenter.default.post(name: notificationLocationAdded, object: nil)
                } else {
                    self.view.makeToast(response!["message"].stringValue, duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
    
    func serverCallforAppointments(){
        showProgressOnView(self.view)
        var locationID = 0
        for item in myLocations {
            if item.name == locationTitle.text {
                locationID = item.id
            }
        }
        let params:[String: Any] = ["appointment_date":selectedDateStr, "location_id":locationID]

        NetworkManager.sharedInstance.executeServiceWithToken(WEB_URL.get_appointment_date, postParameters: params,  completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)

            if success == true {
                if response!["status"].stringValue  == "success"{
                    self.appointments.removeAll()
                    let allBookings = response!["data"].arrayValue
                    for item in allBookings {

                        let userDict = item["users"].dictionaryValue
                        let user = User(id: userDict["id"]!.intValue, name: userDict["name"]!.stringValue, email: userDict["email"]!.stringValue, status: userDict["status"]!.boolValue, image: userDict["image"]!.stringValue, role: userDict["role"]!.intValue, mobile: userDict["mobile"]!.stringValue, date_o_birth: userDict["date_o_birth"]!.stringValue, address: userDict["address"]!.stringValue, introduction: userDict["introduction"]!.stringValue, accept_client: userDict["accept_client"]!.boolValue, created_at: userDict["created_at"]!.stringValue, updated_at: userDict["updated_at"]!.stringValue)
                        
                        let locDict = item["location"].dictionaryValue
                        let loc = MyLocation(id: locDict["id"]!.intValue, name: locDict["name"]!.stringValue, address1: locDict["address1"]!.stringValue, city: locDict["city"]!.stringValue, status: locDict["status"]!.stringValue, country: locDict["country"]!.stringValue)
                        
                        let appointment = Appointment(id: item["id"].intValue, user_id: item["user_id"].intValue, chair_count: item["chair_count"].intValue, location_id: item["location_id"].intValue, status: item["status"].intValue, boooking_date: item["boooking_date"].stringValue, boooking_time: item["boooking_time"].stringValue, users: user, start_time: item["start_time"].stringValue, end_time: item["end_time"].stringValue, frequency: item["frequency"].stringValue, booked_service: item["booked_service"].stringValue, chair_name: item["chair_name"].stringValue, total_cost: item["total_cost"].doubleValue, created_at: item["created_at"].stringValue, updated_at: item["updated_at"].stringValue, location: loc)
                            
                            self.appointments.append(appointment)
                    }
                    DispatchQueue.main.async{
                        self.dayView.reloadData()
                        self.dayView.reloadData()
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })

    }
    func serverCallforMonthAppointments(month:String){
        showProgressOnView(self.view)
        var locationID = 0
        for item in myLocations {
            if item.name == locationTitle.text {
                locationID = item.id
            }
        }
        let params:[String: Any] = ["location_id":locationID, "month": month]

        NetworkManager.sharedInstance.executeServiceWithToken(WEB_URL.get_appointment_month, postParameters: params,  completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)

            if success == true {
                if response!["status"].stringValue  == "success"{
                    self.monthAppointments.removeAll()
                    let allBookings = response!["data"].arrayValue
                    for item in allBookings {

                        let user = User(id: 0, name: "", email: "", status: false, image: "", role: 0, mobile: "", date_o_birth: "", address: "", introduction: "", accept_client: false, created_at: "", updated_at: "")
                            let locDict = item["location"].dictionaryValue
                            let loc = MyLocation(id: locDict["id"]!.intValue, name: locDict["name"]!.stringValue, address1: locDict["address1"]!.stringValue, city: locDict["city"]!.stringValue, status: locDict["status"]!.stringValue, country: locDict["country"]!.stringValue)
                        
                        let appointment = Appointment(id: item["id"].intValue, user_id: item["user_id"].intValue, chair_count: item["chair_count"].intValue, location_id: item["location_id"].intValue, status: item["status"].intValue, boooking_date: item["boooking_date"].stringValue, boooking_time: item["boooking_time"].stringValue, users: user, start_time: item["start_time"].stringValue, end_time: item["end_time"].stringValue, frequency: item["frequency"].stringValue, booked_service: item["booked_service"].stringValue, chair_name: item["chair_name"].stringValue, total_cost: item["total_cost"].doubleValue, created_at: item["created_at"].stringValue, updated_at: item["updated_at"].stringValue, location: loc)
                            
                            self.monthAppointments.append(appointment)
                    }
                    DispatchQueue.main.async{
                        self.dayView.reloadData()
                        self.calender.reloadData()
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })

    }
    func serverCallforChairBookings(){
        showProgressOnView(self.view)
        var locationID = 0
        for item in myLocations {
            if item.name == locationTitle.text {
                locationID = item.id
            }
        }
        let params:[String: Any] = ["location_id":locationID, "date": selectedDateStr]

        NetworkManager.sharedInstance.executeServiceWithToken(WEB_URL.get_booking_with_date, postParameters: params,  completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    self.Bookings.removeAll()
                    let allBookings = response!["data"].arrayValue
                    
                    for item in allBookings {
                        let bArr = item["booked"].arrayValue
                        var entities = [BookingEntity]()
                        for object in bArr {
                            let dict = object["chair"].dictionaryValue
                            var chair: Chair!
                            if dict.count > 0 {
                                chair = Chair(id: dict["id"]!.intValue, status: dict["status"]!.intValue, name: dict["name"]!.stringValue, image: dict["image"]!.stringValue, cost: dict["cost"]!.doubleValue, location_id: dict["location_id"]!.intValue, created_at: dict["created_at"]!.stringValue, updated_at: dict["updated_at"]!.stringValue, deleted_at: dict["deleted_at"]!.stringValue)
                            } else {
                                chair = Chair(id: 0, status: 0, name: "", image: "", cost: 0, location_id: 0, created_at: "", updated_at: "", deleted_at: "")
                            }
                            let currentEntity: BookingEntity = BookingEntity(id: object["id"].intValue, user_id: object["user_id"].intValue, booking_id: object["booking_id"].intValue, chair_id: object["chair_id"].intValue, chair_cost: object["chair_cost"].doubleValue, created_at: object["created_at"].stringValue, updated_at: object["updated_at"].stringValue, chair: chair)
                            entities.append(currentEntity)
                        }
                        let locDict = item["location"].dictionaryValue
                        var loc: MyLocation!
                        if locDict.count > 0 {
                         loc = MyLocation(id: locDict["id"]!.intValue, name: locDict["name"]!.stringValue, address1: locDict["address1"]!.stringValue, city: locDict["city"]!.stringValue, status: locDict["status"]!.stringValue, country: locDict["country"]!.stringValue)
                        } else {
                            loc = MyLocation(id: 0, name: "", address1: "", city: "", status: "", country: "")
                        }
                        let book_data:BookData = BookData(id: item["id"].intValue, user_id: item["user_id"].intValue, chair_id: item["chair_id"].intValue, location_id: item["location_id"].intValue, booking_date: item["boooking_date"].stringValue, booking_time: item["boooking_time"].stringValue, start_time: item["start_time"].stringValue, end_time: item["end_time"].stringValue, frequency: item["frequency"].stringValue, status: item["status"].boolValue, chair_name: item["chair_name"].stringValue, total_cost: item["total_cost"].doubleValue, created_at: item["created_at"].stringValue, updated_at: item["updated_at"].stringValue, booked: entities, location: loc)
                            self.Bookings.append(book_data)
                    }
                    DispatchQueue.main.async{
                        self.dayView.reloadData()
                        self.calender.reloadData()
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })

    }
    func serverCallforMonthChairBookings(month:String){
        showProgressOnView(self.view)
        var locationID = 0
        for item in myLocations {
            if item.name == locationTitle.text {
                locationID = item.id
            }
        }
        let params:[String: Any] = ["location_id":locationID, "month": month]

        NetworkManager.sharedInstance.executeServiceWithToken(WEB_URL.get_chair_booking_month, postParameters: params,  completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    self.monthBookings.removeAll()
                    let allBookings = response!["data"].arrayValue
                    
                    for item in allBookings {
                        let bArr = item["booked"].arrayValue
                        var entities = [BookingEntity]()
                        for object in bArr {
                            let dict = object["chair"].dictionaryValue
                            var chair: Chair!
                            if dict.count > 0 {
                                chair = Chair(id: dict["id"]!.intValue, status: dict["status"]!.intValue, name: dict["name"]!.stringValue, image: dict["image"]!.stringValue, cost: dict["cost"]!.doubleValue, location_id: dict["location_id"]!.intValue, created_at: dict["created_at"]!.stringValue, updated_at: dict["updated_at"]!.stringValue, deleted_at: dict["deleted_at"]!.stringValue)
                            } else {
                                chair = Chair(id: 0, status: 0, name: "", image: "", cost: 0, location_id: 0, created_at: "", updated_at: "", deleted_at: "")
                            }
                            let currentEntity: BookingEntity = BookingEntity(id: object["id"].intValue, user_id: object["user_id"].intValue, booking_id: object["booking_id"].intValue, chair_id: object["chair_id"].intValue, chair_cost: object["chair_cost"].doubleValue, created_at: object["created_at"].stringValue, updated_at: object["updated_at"].stringValue, chair: chair)
                            entities.append(currentEntity)
                        }
                        let locDict = item["location"].dictionaryValue
                        var loc: MyLocation!
                        if locDict.count > 0 {
                         loc = MyLocation(id: locDict["id"]!.intValue, name: locDict["name"]!.stringValue, address1: locDict["address1"]!.stringValue, city: locDict["city"]!.stringValue, status: locDict["status"]!.stringValue, country: locDict["country"]!.stringValue)
                        } else {
                            loc = MyLocation(id: 0, name: "", address1: "", city: "", status: "", country: "")
                        }
                        let book_data:BookData = BookData(id: item["id"].intValue, user_id: item["user_id"].intValue, chair_id: item["chair_id"].intValue, location_id: item["location_id"].intValue, booking_date: item["boooking_date"].stringValue, booking_time: item["boooking_time"].stringValue, start_time: item["start_time"].stringValue, end_time: item["end_time"].stringValue, frequency: item["frequency"].stringValue, status: item["status"].boolValue, chair_name: item["chair_name"].stringValue, total_cost: item["total_cost"].doubleValue, created_at: item["created_at"].stringValue, updated_at: item["updated_at"].stringValue, booked: entities, location: loc)
                            self.monthBookings.append(book_data)
                    }
                    DispatchQueue.main.async{
                        self.dayView.reloadData()
                        self.calender.reloadData()
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })

    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toEditAppointment" {
            let editAppointmentVC = segue.destination as! EditAppointmentViewController
            editAppointmentVC.appointment = selectedAppointment
        }
    }
    

    // MARK:- UIGestureRecognizerDelegate
    
//    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
////        let shouldBegin = self.tableView.contentOffset.y <= -self.tableView.contentInset.top
////        let shouldBegin = self.dayView.contentOffset.y <= -self.dayView.contentInset.top
////        if shouldBegin {
//            let velocity = self.scopeGesture.velocity(in: self.view)
//            switch self.calender.scope {
//            case .month:
//                return velocity.y < 0
//            case .week:
//                return velocity.y > 0
//            }
////        }
//        return true//shouldBegin
//    }
}

// MARK: EventDataSource
extension AppointmentViewController: EventDataSource {
    func eventsForDate(_ date: Date) -> [EventDescriptor] {
        var events = [Event]()
        
//        event.startDate = Date(timeIntervalSince1970: date.timeIntervalSince1970 + Double(6000 * 5))
//        event.endDate = Date(timeIntervalSince1970: event.startDate.timeIntervalSince1970 + Double(6000 * 60))
//        events.append(event)
        
        for item in self.appointments {
            let event = Event()
            let bookingDateStr = item.boooking_date//item["boooking_date"].stringValue
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let bookingDate = formatter.date(from: bookingDateStr)
            
//            let bookingTimeStr = item.boooking_time//item["boooking_time"].stringValue
//            let timeArr = bookingTimeStr.split(separator: "-")
//            var start_dateStr: String = String(timeArr.first!)
//            start_dateStr = start_dateStr.trimmingCharacters(in: .whitespaces)
//            let startDateStr = "\(bookingDateStr) \(start_dateStr)"
            
            let start_dateStr = item.start_time
            let startDateStr = "\(bookingDateStr) \(start_dateStr)"
            
            var end_dateStr = item.end_time
            end_dateStr = end_dateStr.trimmingCharacters(in: .whitespaces)
            let endDateStr = "\(bookingDateStr) \(end_dateStr)"
            
            let dF = DateFormatter()
            dF.dateFormat = "yyyy-MM-dd hh:mm a"
            dF.locale =  .current//Locale(identifier: "en_US_POSIX")
            let startDate = dF.date(from: startDateStr )
            let endDate = dF.date(from: endDateStr )
            
//            let index = self.appointments.firstIndex(where: {$0.id == item.id})
//            let AppointmentNo = "Appointment \((index ?? 0) + 1)"
            let AppointmentID = "Appointment ID:\(item.id)"
            
//            var info = [AppointmentNo, item.location.name, item.location.address1]
            var info = [item.users.name, AppointmentID, item.location.name, item.location.address1]
            info.append("\(item.start_time) - \(item.end_time)")
            // Set "text" value of event by formatting all the information needed for display
            event.text = info.reduce("", {$0 + $1 + "\n"})
//            event.text = String(item.location.name)
            event.startDate = startDate!
            event.endDate = endDate!
//            event.startDate = Date(timeIntervalSince1970: date.timeIntervalSince1970 + Double(6000 * 5))
//            event.endDate = Date(timeIntervalSince1970: event.startDate.timeIntervalSince1970 + Double(6000 * 60))
            events.append(event)
        }
        for item in self.Bookings {
            let event = Event()
            let bookingDateStr = item.booking_date//item["boooking_date"].stringValue
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let bookingDate = formatter.date(from: bookingDateStr)
                        
            let start_dateStr = item.start_time
            let startDateStr = "\(bookingDateStr) \(start_dateStr)"
            
            var end_dateStr = item.end_time
            end_dateStr = end_dateStr.trimmingCharacters(in: .whitespaces)
            let endDateStr = "\(bookingDateStr) \(end_dateStr)"
            
            let dF = DateFormatter()
            dF.dateFormat = "yyyy-MM-dd hh:mm a"
            dF.locale =  .current//Locale(identifier: "en_US_POSIX")
            let startDate = dF.date(from: startDateStr )
            let endDate = dF.date(from: endDateStr )
            
//            let index = self.Bookings.firstIndex(where: {$0.id == item.id})
//            let BookingNo = "Booking \((index ?? 0) + 1)"
            
            let chair = item.booked[0].chair.name
            
//            var info = [BookingNo, item.location.name, item.location.address1]
            var info = [chair, item.location.name, item.location.address1]
            info.append("\(item.start_time) - \(item.end_time)")
            // Set "text" value of event by formatting all the information needed for display
            event.text = info.reduce("", {$0 + $1 + "\n"})
            event.startDate = startDate!
            event.endDate = endDate!
            event.backgroundColor = .lightGray
            events.append(event)
        }
        return events
    }
    
    
}
extension AppointmentViewController: DayViewDelegate {
    func dayView(dayView: DayView, didTapTimelineAt date: Date) {
        print("didTapTimelineAt")
    }
    
    func dayView(dayView: DayView, didLongPressTimelineAt date: Date) {
        print("didLongPressTimelineAt")
    }
    
    func dayViewDidBeginDragging(dayView: DayView) {
        print("dayViewDidBeginDragging")
    }
    
    func dayViewDidTransitionCancel(dayView: DayView) {
        print("dayViewDidTransitionCancel")
    }
    
    func dayView(dayView: DayView, willMoveTo date: Date) {
        print("willMoveTo")
    }
    
    func dayView(dayView: DayView, didMoveTo date: Date) {
        print("didMoveTo")
    }
    
    func dayView(dayView: DayView, didUpdate event: EventDescriptor) {
        print("didUpdate")
    }
    
    func dayViewDidSelectEventView(_ eventView: EventView) {
//     print("Event has been selected: \(eventView.data)")
        print("Event has been selected")
        guard let kEvent = eventView.descriptor as? Event else {
            return
        }
       
        
//        for item in self.appointments {
//            let eventName = String(item.location.name)
//            if eventName == kEvent.text {
//                self.locationLbl.text = item.location.name
//                let bookingDate = item.boooking_date
//                let df = DateFormatter()
//                df.dateFormat = "yyyy-MM-dd"
//                let bDate = df.date(from: bookingDate)
//                df.dateFormat = "MMM d, yyyy"
//                let bDateStr = df.string(from: bDate!)
//                self.appointmentDateLbl.text = bDateStr
//                self.selectedAppointment = item
//            }
//        }
        if kEvent.backgroundColor != .lightGray{
            let appointmentNO = kEvent.text.split(separator: "\n")[1]
            let idStr = appointmentNO.split(separator: ":").last
            let ID = Int(idStr!)
            for item in self.appointments {
                if item.id == ID{
                    self.locationLbl.text = item.location.name
                    let bookingDate = item.boooking_date
                    let df = DateFormatter()
                    df.dateFormat = "yyyy-MM-dd"
                    let bDate = df.date(from: bookingDate)
                    df.dateFormat = "MMM d, yyyy"
                    let bDateStr = df.string(from: bDate!)
                    self.appointmentDateLbl.text = bDateStr
                    self.serviceslbl.text = ""
                    self.selectedAppointment = item
                }
            }
//            self.selectedAppointment = self.appointments[index! - 1]
            self.summaryView.isHidden =  false
        }
//        presentDetailViewForEvent(ckEvent.ekEvent)
   }
//    private func presentDetailViewForEvent(_ ekEvent: EKEvent) {
//        let eventController = EKEventViewController()
//        eventController.event = ekEvent
//        eventController.allowsCalendarPreview = true
//        eventController.allowsEditing = true
//        navigationController?.pushViewController(eventController,
//                                                 animated: true)
//    }

    func dayViewDidLongPressEventView(_ eventView: EventView) {
        print("Event has been longPressed: \(eventView.description)")
   }
}

extension AppointmentViewController: FSCalendarDelegate, FSCalendarDataSource,FSCalendarDelegateAppearance {
    func setCalendar() {
        calender.delegate = self
        calender.dataSource = self
//        calender.headerHeight = 0
        self.calender.appearance.eventDefaultColor = UIColor.black
        self.selectedDate = Date()
        self.selectedDateStr = self.dateFormatter2.string(from: Date())
        self.dayView.move(to: self.selectedDate)
        calender.scope = .month
        calender.reloadData()
        headerLbl.text = self.dateFormatter.string(from: calender.currentPage)
        
    }
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calenderSuperviewHeight.constant = bounds.height + 10
        self.view.layoutIfNeeded()
    }
//    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
////        let day: Int! = self.gregorian.component(.day, from: date)
////        return day % 5 == 0 ? day/5 : 0;
//
//        let df = DateFormatter()
//                    df.dateFormat = "yyyy-MM-dd"
//        let dateStr = df.string(from: date)
//        var bArr: [Appointment] = self.monthAppointments
//        bArr = bArr.filter({$0.boooking_date == dateStr})
//        return bArr.count
//
////        for item in self.monthAppointments{
////            let bookingDate = item.boooking_date
////            let df = DateFormatter()
////            df.dateFormat = "yyyy-MM-dd"
////            let bDate = df.date(from: bookingDate)
////        }
//    }
    
    func calendar(_ calendar: FSCalendar, imageFor date: Date) -> UIImage? {
//        let day: Int! = self.gregorian.component(.day, from: date)
//        return [13,24].contains(day) ? UIImage(named: "icon_cat") : nil
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let dateStr = df.string(from: date)
        var bArr: [BookData] = self.monthBookings
        bArr = bArr.filter({$0.booking_date == dateStr})
        if bArr.count > 0{
            return #imageLiteral(resourceName: "event_seat_small")
        }else {
            return nil
        }
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
//    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderDefaultColorFor date: Date) -> UIColor? {
//    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderSelectionColorFor date: Date) -> UIColor? {
//        let key = self.dateFormatter1.string(from: date)
//        if let color = self.borderSelectionColors[key] {
//            return color
//        }
//        return appearance.borderSelectionColor
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let dateStr = df.string(from: date)
        var bArr: [Appointment] = self.monthAppointments
        bArr = bArr.filter({$0.boooking_date == dateStr})
        if bArr.count > 0{
            return #colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1)
        }else {
            return appearance.borderSelectionColor
        }
    }
//    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderRadiusFor date: Date) -> CGFloat {
////        if [8, 17, 21, 25].contains((self.gregorian.component(.day, from: date))) {
//            return 0.0
////        }
////        return 1.0
//    }
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        self.headerLbl.text = self.dateFormatter.string(from: calendar.currentPage)
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM"
        let currentMonth = formatter.string(from: calendar.currentPage)
        self.serverCallforMonthAppointments(month: currentMonth)
        self.serverCallforMonthChairBookings(month: currentMonth)
    }
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print("calendar did select date \(self.dateFormatter.string(from: date))")
        self.selectedDate = date
        self.selectedDateStr = self.dateFormatter2.string(from: date)
        self.dayView.move(to: self.selectedDate)
        self.serverCallforAppointments()
        self.serverCallforChairBookings()
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
        }
    }
    
}

