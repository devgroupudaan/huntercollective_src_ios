//
//  EditAppointmentViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 27/08/21.
//

import UIKit
import FSCalendar
import DropDown

class timeCell: UITableViewCell {
    
    @IBOutlet weak var timeLbl: UILabel!
}

class EditAppointmentViewController: UIViewController,UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var timeTblview: UITableView!
    @IBOutlet weak var freqTxtFld: UITextField!
    @IBOutlet weak var client_name: UILabel!
    @IBOutlet weak var location_name: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var frequencyLbl: UILabel!
    
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    
    var appointment: Appointment!
    
    var timeArray = [String]()
    let frequencyDropdown = DropDown()
    
    var frequencyArr = [String]()
    var selectedFrequency = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timeArray = ["11:00AM","12:00AM","01:00PM","02:00PM","03:00PM","04:00PM","05:00PM","06:00PM","07:00PM"]
        viewHeight.constant = 1360
        self.view.setNeedsLayout()
        freqTxtFld.delegate = self
        timeTblview.delegate = self
        timeTblview.dataSource = self
        frequencyArr = ["One-off","Week","Month","3 Months","6 Months", "Year"]
        self.location_name.text = appointment.location.name
        self.dateLbl.text = appointment.boooking_date
        self.timeLbl.text = "\(appointment.start_time) - \(appointment.end_time)"
        self.frequencyLbl.text = appointment.frequency
        setupFrequencyDropdown()
        // Do any additional setup after loading the view.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        freqTxtFld.isUserInteractionEnabled = true
    }
    
    func setupFrequencyDropdown() {
        frequencyDropdown.anchorView = freqTxtFld
        
        frequencyDropdown.bottomOffset = CGPoint(x: 0, y: freqTxtFld.bounds.height)
        
        frequencyDropdown.dataSource = frequencyArr
        frequencyDropdown.direction = .bottom
        
        // Action triggered on selection
        frequencyDropdown.selectionAction = { [weak self] (index, item) in
            self?.freqTxtFld.text = item
            self?.freqTxtFld.isUserInteractionEnabled = true
            self?.frequencyDropdown.hide()
            self?.selectedFrequency = item
        }
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func saveAppointment(_ sender: UIButton) {
    }
    
    //MARK:- Textfield Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == freqTxtFld {
            frequencyDropdown.show()
            freqTxtFld.isUserInteractionEnabled = false
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("end")
        freqTxtFld.isUserInteractionEnabled = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    // MARK: - tableview Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.timeArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "timeCell") as! timeCell
        let time = timeArray[indexPath.row]
        cell.timeLbl.text = time
        return cell
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
