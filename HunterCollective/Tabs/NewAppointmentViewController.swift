//
//  NewAppointmentViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 08/06/21.
//

import UIKit
import FSCalendar
import DropDown
import SwiftyJSON

class svcCell:UICollectionViewCell {
    @IBOutlet weak var sImage: DesignableImageView!
    @IBOutlet weak var sOverlay: DesignableView!
    @IBOutlet weak var sName: UILabel!
    
}
class addOnCell:UICollectionViewCell {
    @IBOutlet weak var addOnImg: DesignableImageView!
    @IBOutlet weak var selOverlay: DesignableView!
    @IBOutlet weak var addOnName: UILabel!
    
}
class NewAppointmentViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate,UIGestureRecognizerDelegate {

    @IBOutlet weak var calender: FSCalendar!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var locationTxtFld: UITextField!
    @IBOutlet weak var clientTxtFld: UITextField!
    @IBOutlet weak var startTimeTxtFld: UITextField!
    @IBOutlet weak var endTimeTxtFld: UITextField!
    @IBOutlet weak var frequencyTxtFld: UITextField!
    @IBOutlet weak var timesTxtFld: UITextField!
    @IBOutlet weak var serviceCollectionView: UICollectionView!
    @IBOutlet weak var addOnCollectionView: UICollectionView!
    @IBOutlet weak var calenderHeight: NSLayoutConstraint!
    @IBOutlet weak var timesViewHeight: NSLayoutConstraint!
    
    let locationDropdown = DropDown()
    let clientDropdown = DropDown()
    let frequencyDropdown = DropDown()
    let timesDropdown = DropDown()
    
    var locationArr = [String]()
    var clients = [Client]()
    var selectedClient: Client!
    var clientArr = [String]()
    var frequencyArr = [String]()
    var timesArr = [String]()
    var services = [MyService]()
    var selectedServices = [Int]()
    
    var selectedLocationID = 0
    var selectedLocationStr = ""
    var selectedFrequency = ""
    var selectedNoTimes = ""
    var selectedDate = Date()
    var selectedDateStr = ""
    var requiredDeposit = false
    
    private var currentPage: Date?
    private lazy var today: Date = {
        return Date()
    }()
    private lazy var dateFormatter: DateFormatter = {
        let df = DateFormatter()
//        df.locale = Locale(identifier: "ko_KR")
        df.dateFormat = "MMMM yyyy"
        return df
    }()
    private lazy var dateFormatter2: DateFormatter = {
        let df = DateFormatter()
//        df.locale = Locale(identifier: "ko_KR")
        df.dateFormat = "yyyy-MM-dd"
        return df
    }()
    
    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calender, action: #selector(self.calender.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addGestureRecognizer(self.scopeGesture)
//        self.tableView.panGestureRecognizer.require(toFail: self.scopeGesture)
        let scope: FSCalendarScope = .month
        self.calender.setScope(scope, animated: false)
        
        serviceCollectionView.delegate = self
        serviceCollectionView.dataSource = self
        addOnCollectionView.delegate = self
        addOnCollectionView.dataSource = self
        
        locationTxtFld.delegate = self
        clientTxtFld.delegate = self
        frequencyTxtFld.delegate = self
        
//        if let locations: [String]  = UserDefaults.standard.value(forKey: "allLocationNames") as? [String] {
//            locationArr = locations
//            if let selectedLocation  = UserDefaults.standard.value(forKey: "selectedLocation") {
//                if !locations.contains(selectedLocation as! String){
//                    if locations.count > 0 {
//                        if let locationIDs: [Int]  = UserDefaults.standard.value(forKey: "allLocations") as? [Int] {
//                            self.selectedLocationID = locationIDs[0]
//                            self.selectedLocationStr = locations[0]
//                            self.locationTxtFld.text = locations.first
//                            UserDefaults.standard.setValue(locations.first, forKey: "selectedLocation")
//                        } else {
//                        }
//
//                    } else {
//                        self.selectedLocationID = 0
//                        self.locationTxtFld.text = ""
//                        UserDefaults.standard.setValue("", forKey: "selectedLocation")
//                    }
//                } else {
//                    if let locationIDs: [Int]  = UserDefaults.standard.value(forKey: "allLocations") as? [Int] {
//                        let index = locations.firstIndex(of: selectedLocation as! String)
//                        self.selectedLocationID = locationIDs[index!]
//                        self.locationTxtFld.text = selectedLocation as? String
//                    } else {
//                    }
//
//                }
//            } else {
//                if let locationIDs: [Int]  = UserDefaults.standard.value(forKey: "allLocations") as? [Int] {
//                    self.selectedLocationID = locationIDs[0]
//                } else {
//                }
//                self.locationTxtFld.text = locations.first
//                UserDefaults.standard.setValue(locations.first, forKey: "selectedLocation")
//            }
//
//        } else {
//            self.view.makeToast("You haven't added any location yet.", duration: 3.0, position: .bottom)
//        }
//        frequencyArr = ["One-off","Week","Month","3 Months","6 Months", "Year"]
////        serverCallforClients()
////        serverCallforServices()
//
//        setupLocationDropdown()
//        setupFrequencyDropdown()
        
        let datePicker1 = UIDatePicker()
        datePicker1.datePickerMode = .time
        datePicker1.locale = .current
        if #available(iOS 14, *) {
            datePicker1.preferredDatePickerStyle = .wheels
            datePicker1.sizeToFit()
        }
        let DF = DateFormatter()
        DF.dateFormat = "hh:mm a"
        let date = DF.date(from: "09:00 AM")
        datePicker1.date = date!
        startTimeTxtFld.text = "09:00 AM"
        startTimeTxtFld.inputView = datePicker1
        datePicker1.addTarget(self, action: #selector(handleDatePicker(sender:)), for: UIControl.Event.valueChanged)
        
        let datePicker2 = UIDatePicker()
        datePicker2.datePickerMode = .time
        datePicker2.locale = .current
        if #available(iOS 14, *) {
            datePicker2.preferredDatePickerStyle = .wheels
            datePicker2.sizeToFit()
        }
        let date2 = DF.date(from: "11:00 AM")
        datePicker2.date = date2!
        endTimeTxtFld.text = "11:00 AM"
        endTimeTxtFld.inputView = datePicker2
        datePicker2.addTarget(self, action: #selector(handleDatePicker2(sender:)), for: UIControl.Event.valueChanged)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setCalendar()
        serverCallforClients()
        
        self.timesViewHeight.constant = 0
        self.view.layoutIfNeeded()
        
        if let locations: [String]  = UserDefaults.standard.value(forKey: "allLocationNames") as? [String] {
            locationArr = locations
            if let selectedLocation  = UserDefaults.standard.value(forKey: "selectedLocation") {
                if !locations.contains(selectedLocation as! String){
                    if locations.count > 0 {
                        if let locationIDs: [Int]  = UserDefaults.standard.value(forKey: "allLocations") as? [Int] {
                            self.selectedLocationID = locationIDs[0]
                            self.selectedLocationStr = locations[0]
                            self.locationTxtFld.text = locations.first
                            UserDefaults.standard.setValue(locations.first, forKey: "selectedLocation")
                        } else {
                        }
                        
                    } else {
                        self.selectedLocationID = 0
                        self.locationTxtFld.text = ""
                        UserDefaults.standard.setValue("", forKey: "selectedLocation")
                    }
                } else {
                    if let locationIDs: [Int]  = UserDefaults.standard.value(forKey: "allLocations") as? [Int] {
                        let index = locations.firstIndex(of: selectedLocation as! String)
                        self.selectedLocationID = locationIDs[index!]
                        self.locationTxtFld.text = selectedLocation as? String
                    } else {
                    }
                    
                }
            } else {
                if let locationIDs: [Int]  = UserDefaults.standard.value(forKey: "allLocations") as? [Int] {
                    self.selectedLocationID = locationIDs[0]
                } else {
                }
                self.locationTxtFld.text = locations.first
                UserDefaults.standard.setValue(locations.first, forKey: "selectedLocation")
            }
           
        } else {
            self.view.makeToast("You haven't added any location yet.", duration: 3.0, position: .bottom)
        }
        frequencyArr = ["One-off","Week","Month","3 Months","6 Months", "Year"]
        timesArr = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
        setupLocationDropdown()
        setupFrequencyDropdown()
        
        serverCallforServices()
    }
//    @objc func onClickDoneButton() {
//        self.view.endEditing(true)
//    }
    @objc func handleDatePicker(sender: UIDatePicker) {
        let DF = DateFormatter()
        DF.dateFormat = "hh:mm a"
        startTimeTxtFld.text = DF.string(from: sender.date)
    }
    @objc func handleDatePicker2(sender: UIDatePicker) {
        let DF = DateFormatter()
        DF.dateFormat = "hh:mm a"
        endTimeTxtFld.text = DF.string(from: sender.date)
    }
    
    private func scrollCurrentPage(isPrev: Bool) {
        let cal = Calendar.current
        var dateComponents = DateComponents()
        dateComponents.month = isPrev ? -1 : 1
        self.currentPage = cal.date(byAdding: dateComponents, to: self.currentPage ?? self.today)
        self.calender.setCurrentPage(self.currentPage!, animated: true)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        locationTxtFld.isUserInteractionEnabled = true
        clientTxtFld.isUserInteractionEnabled = true
        frequencyTxtFld.isUserInteractionEnabled = true
        timesTxtFld.isUserInteractionEnabled = true
    }
    
    @IBAction func menuBtnAction(_ sender: UIButton) {
        HamburgerMenu().triggerSideMenu()
    }
    @IBAction func prevBtnTapped(_ sender: UIButton) {
        scrollCurrentPage(isPrev: true)
    }
    @IBAction func nextBtnTapped(_ sender: UIButton) {
        scrollCurrentPage(isPrev: false)
    }
    @IBAction func requireDeposit(_ sender: UISwitch) {
        if sender.isOn {
            requiredDeposit = true
        } else {
            requiredDeposit = false
        }
    }
    
    @IBAction func confirmAction(_ sender: UIButton) {
        serverCallforNewAppointment()
    }
    
    //MARK: - Set Up stateDropdown
    func setupLocationDropdown() {
        locationDropdown.anchorView = locationTxtFld
        
        locationDropdown.bottomOffset = CGPoint(x: 0, y: locationTxtFld.bounds.height)
        
        locationDropdown.dataSource = locationArr
        locationDropdown.direction = .bottom
        
        // Action triggered on selection
        locationDropdown.selectionAction = { [weak self] (index, item) in
            self?.locationTxtFld.text = item
            self?.selectedLocationStr = item
            self?.locationTxtFld.isUserInteractionEnabled = true
            self?.locationDropdown.hide()
            let index = self?.locationArr.firstIndex(of: item)
            if let locations: [Int]  = UserDefaults.standard.value(forKey: "allLocations") as? [Int] {
                self?.selectedLocationID = locations[index!]
            } else {
            }
        }
        
    }
    func setupClientDropdown() {
        clientDropdown.anchorView = clientTxtFld
        
        clientDropdown.bottomOffset = CGPoint(x: 0, y: clientTxtFld.bounds.height)
        
        clientDropdown.dataSource = clientArr
        clientDropdown.direction = .bottom
        
        // Action triggered on selection
        clientDropdown.selectionAction = { [weak self] (index, item) in
            self?.clientTxtFld.text = item
            self?.clientTxtFld.isUserInteractionEnabled = true
            self?.clientDropdown.hide()
            let index = self?.clients.firstIndex(where: {$0.name == item})
            self?.selectedClient = self?.clients[index!]
        }
        
        // Action triggered on dropdown cancelation (hide)
        //                locationDropdown.cancelAction = { [unowned self] in
        //                    // You could for example deselect the selected item
        //                    self.inputTxtFld.text = ""
        //                }
        
        // You can manually select a row if needed
        //        dropDown.selectRowAtIndex(3)
    }
    func setupFrequencyDropdown() {
        frequencyDropdown.anchorView = frequencyTxtFld
        
        frequencyDropdown.bottomOffset = CGPoint(x: 0, y: frequencyTxtFld.bounds.height)
        
        frequencyDropdown.dataSource = frequencyArr
        frequencyDropdown.direction = .bottom
        
        // Action triggered on selection
        frequencyDropdown.selectionAction = { [weak self] (index, item) in
            self?.frequencyTxtFld.text = item
            self?.frequencyTxtFld.isUserInteractionEnabled = true
            self?.frequencyDropdown.hide()
            self?.selectedFrequency = item
            if item != "One-off" {
                self?.timesViewHeight.constant = 100
                self?.view.layoutIfNeeded()
                self?.setupTimesDropdown()
            }
        }
        
        // Action triggered on dropdown cancelation (hide)
        //                locationDropdown.cancelAction = { [unowned self] in
        //                    // You could for example deselect the selected item
        //                    self.inputTxtFld.text = ""
        //                }
        
        // You can manually select a row if needed
        //        dropDown.selectRowAtIndex(3)
    }
    
    func setupTimesDropdown() {
        timesDropdown.anchorView = timesTxtFld
        timesDropdown.bottomOffset = CGPoint(x: 0, y: timesTxtFld.bounds.height)
        timesDropdown.dataSource = timesArr
        timesDropdown.direction = .bottom
        
        timesDropdown.selectionAction = { [weak self] (index, item) in
            self?.timesTxtFld.text = item
            self?.timesTxtFld.isUserInteractionEnabled = true
            self?.timesDropdown.hide()
            self?.selectedNoTimes = item
            
        }
    }
    
    //MARK:- Textfield Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == locationTxtFld {
            locationDropdown.show()
            locationTxtFld.isUserInteractionEnabled = false
        } else if textField == clientTxtFld {
            clientDropdown.show()
            clientTxtFld.isUserInteractionEnabled = false
        } else if textField == frequencyTxtFld {
            frequencyDropdown.show()
            frequencyTxtFld.isUserInteractionEnabled = false
        }else if textField == timesTxtFld {
            timesDropdown.show()
            timesTxtFld.isUserInteractionEnabled = false
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("end")
        locationTxtFld.isUserInteractionEnabled = true
        clientTxtFld.isUserInteractionEnabled = true
        frequencyTxtFld.isUserInteractionEnabled = true
        timesTxtFld.isUserInteractionEnabled = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    // MARK: - collectionView Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == serviceCollectionView {
            return services.count
        } else {
            return 3
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == serviceCollectionView {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "svcCell", for: indexPath) as! svcCell
            let service = services[indexPath.row]
            cell.sName.text = service.title
            cell.sImage.kf.setImage(with: URL(string: service.image), placeholder:#imageLiteral(resourceName: "person-24px 1"), options: nil, progressBlock: nil, completionHandler: nil)
            if selectedServices.contains(service.id){
                cell.sOverlay.isHidden = false
            } else {
                cell.sOverlay.isHidden = true
            }
        
        return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addOnCell", for: indexPath) as! addOnCell
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == serviceCollectionView {
        let service = services[indexPath.row]
        if selectedServices.contains(service.id){
            let index = selectedServices.firstIndex(of: service.id)
            selectedServices.remove(at: index!)
            let cell = collectionView.cellForItem(at: indexPath) as! svcCell
            cell.sOverlay.isHidden = true
        } else {
            selectedServices.append(service.id)
            let cell = collectionView.cellForItem(at: indexPath) as! svcCell
            cell.sOverlay.isHidden = false
        }
        }
    }
    
    //MARK:- Server Calls
    func serverCallforClients(){
        showProgressOnView(self.view)
        
        NetworkManager.sharedInstance.executeGetServiceWithToken(WEB_URL.getClients,  completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    self.clients.removeAll()
                    self.clientArr.removeAll()
                    let allClients = response!["data"].arrayValue
                    for item in allClients {
                        let client:Client = Client(image: item["image"].stringValue, name: item["name"].stringValue, email: item["email"].stringValue, mobile: item["mobile"].stringValue)
                        self.clients.append(client)
                        self.clientArr.append(item["name"].stringValue)
                    }
                    DispatchQueue.main.async{
                        self.setupClientDropdown()
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
    
    func serverCallforServices(){
        showProgressOnView(self.view)
        
        NetworkManager.sharedInstance.executeGetServiceWithToken(WEB_URL.my_services,  completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    self.services.removeAll()
                    let allServices = response!["data"].arrayValue
                    for item in allServices {
                        let cat_infoDict = item["category"].dictionaryValue
                        let cat_info: Service_category = Service_category(id: cat_infoDict["id"]!.intValue, name: cat_infoDict["name"]!.stringValue)
                        let service:MyService = MyService(id: item["id"].intValue, image: item["image"].stringValue, title: item["title"].stringValue, description: item["description"].stringValue, price: item["price"].doubleValue, require_add_on: item["require_add_on"].boolValue, time: item["time"].stringValue, category: cat_info)
                        self.services.append(service)
                    }
                    DispatchQueue.main.async{
                        self.serviceCollectionView.reloadData()
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
    
    func serverCallforNewAppointment(){
        showProgressOnView(self.view)
        let params:[String: Any] = ["location_id": self.selectedLocationID, "date":selectedDateStr,"start_time":startTimeTxtFld.text!, "end_time":endTimeTxtFld.text!, "frequency":selectedFrequency, "service_id":selectedServices,"add_on":[], "require_deposit":requiredDeposit]
        NetworkManager.sharedInstance.executeServiceWithToken(WEB_URL.new_appointment, postParameters: params, completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    DispatchQueue.main.async{
                        self.clientTxtFld.text = ""
                        self.startTimeTxtFld.text = ""
                        self.endTimeTxtFld.text = ""
                        self.selectedServices.removeAll()
                        self.serviceCollectionView.reloadData()
                        
                        self.view.makeToast("New Appointment has been scheduled.", duration: 2.0, position: .bottom)
                        let when = DispatchTime.now() + 2
                        DispatchQueue.main.asyncAfter(deadline: when){
                            self.tabBarController?.selectedIndex = 0
                        }
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            } else {
                self.view.makeToast("Some error occured.", duration: 2.0, position: .bottom)
            }
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension NewAppointmentViewController: FSCalendarDelegate {
    func setCalendar() {
        calender.delegate = self
        calender.headerHeight = 0
        calender.scope = .month
        headerLbl.text = self.dateFormatter.string(from: calender.currentPage)
    }
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calenderHeight.constant = bounds.height
        self.view.layoutIfNeeded()
    }
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        self.headerLbl.text = self.dateFormatter.string(from: calendar.currentPage)
        
    }
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print("calendar did select date \(self.dateFormatter.string(from: date))")
        self.selectedDate = date
        self.selectedDateStr = self.dateFormatter2.string(from: date)
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
        }
    }
}

