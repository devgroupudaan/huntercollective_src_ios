//
//  NewChairBookingViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 29/07/21.
//

import UIKit
import FSCalendar
import DropDown
import SwiftyJSON

class bookingTableCell: UITableViewCell {
    @IBOutlet weak var ImgView: DesignableImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var switchBtn: UISwitch!
    
}

class NewChairBookingViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var calender: FSCalendar!
    @IBOutlet weak var bookingTable: UITableView!
    @IBOutlet weak var locationTxtFld: UITextField!
    @IBOutlet weak var frequencyTxtFld: UITextField!
    @IBOutlet weak var timesTxtFld: UITextField!
    
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var calenderHeight: NSLayoutConstraint!
    @IBOutlet weak var timeViewHeight: NSLayoutConstraint!
    
    let locationDropdown = DropDown()
    let frequencyDropdown = DropDown()
    let timesDropdown = DropDown()
    var chairs = [Chair]()
    
    var locationArr = [String]()
    var frequencyArr = [String]()
    var timesArr = [String]()
    var selectedLocationID = 0
    var selectedLocationStr = ""
    var selectedFrequency = ""
    var selectedNoTimes = ""
    var selectedDate = Date()
    var selectedDateStr = ""
    var selectedChairs = [Int]()
    var totalPrice: Double = 0.0
    
    var isBookingDone = false
    var isChairsAvailable = false
    
    var isRepeatBooking: Bool!
    var repeatedBooking: BookData!
    
    private var currentPage: Date?
    private lazy var today: Date = {
        return Date()
    }()
    private lazy var dateFormatter: DateFormatter = {
        let df = DateFormatter()
//        df.locale = Locale(identifier: "ko_KR")
        df.dateFormat = "MMMM yyyy"
        return df
    }()
    private lazy var dateFormatter2: DateFormatter = {
        let df = DateFormatter()
//        df.locale = Locale(identifier: "ko_KR")
        df.dateFormat = "yyyy-MM-dd"
        return df
    }()
    
    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calender, action: #selector(self.calender.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addGestureRecognizer(self.scopeGesture)
        let scope: FSCalendarScope = .month
        self.calender.setScope(scope, animated: false)
        
        bookingTable.delegate = self
        bookingTable.dataSource = self
        locationTxtFld.delegate = self
        frequencyTxtFld.delegate = self
        timesTxtFld.delegate = self
        // Do any additional setup after loading the view.
        
        self.selectedDateStr = self.dateFormatter2.string(from: Date())
        
        self.timeViewHeight.constant = 0
        self.view.layoutIfNeeded()
        
        if isRepeatBooking {
            self.selectedLocationID = repeatedBooking.location.id
            self.selectedLocationStr = repeatedBooking.location.name
            self.locationTxtFld.text = repeatedBooking.location.name
            self.frequencyTxtFld.text = repeatedBooking.frequency
            if self.frequencyTxtFld.text != "One-off" {
                self.timeViewHeight.constant = 100
                self.view.layoutIfNeeded()
                self.setupTimesDropdown()
            }
            self.serverCallforAvailableChairs()
        } else {
            
            if let locations: [String]  = UserDefaults.standard.value(forKey: "allLocationNames") as? [String] {
                locationArr = locations
                if let selectedLocation  = UserDefaults.standard.value(forKey: "selectedLocation") {
                    if !locations.contains(selectedLocation as! String){
                        if locations.count > 0 {
                            if let locationIDs: [Int]  = UserDefaults.standard.value(forKey: "allLocations") as? [Int] {
                                self.selectedLocationID = locationIDs[0]
                                self.selectedLocationStr = locations[0]
                                self.locationTxtFld.text = locations.first
                                UserDefaults.standard.setValue(locations.first, forKey: "selectedLocation")
                                self.serverCallforAvailableChairs()
                            } else {
                            }
                            
                        } else {
                            self.selectedLocationID = 0
                            self.locationTxtFld.text = ""
                            UserDefaults.standard.setValue("", forKey: "selectedLocation")
                        }
                    } else {
                        if let locationIDs: [Int]  = UserDefaults.standard.value(forKey: "allLocations") as? [Int] {
                            let index = locations.firstIndex(of: selectedLocation as! String)
                            self.selectedLocationID = locationIDs[index!]
                            self.locationTxtFld.text = selectedLocation as? String
                            self.serverCallforAvailableChairs()
                        } else {
                        }
                        
                    }
                } else {
                    if let locationIDs: [Int]  = UserDefaults.standard.value(forKey: "allLocations") as? [Int] {
                        self.selectedLocationID = locationIDs[0]
                        self.serverCallforAvailableChairs()
                    } else {
                    }
                    self.locationTxtFld.text = locations.first
                    UserDefaults.standard.setValue(locations.first, forKey: "selectedLocation")
                }
               
            } else {
                self.view.makeToast("You haven't added any location yet.", duration: 3.0, position: .bottom)
                //            locationArr = ["location1","location2","location3","location4","location5"]
            }
        }
        frequencyArr = ["One-off","Week","Month","3 Months","6 Months", "Year"]
        timesArr = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
        setupLocationDropdown()
        setupFrequencyDropdown()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.bookingDone(notification:)), name: NSNotification.Name(rawValue: "bookingDone"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setCalendar()
        self.locationTxtFld.resignFirstResponder()
        self.frequencyTxtFld.resignFirstResponder()
        self.timesTxtFld.resignFirstResponder()
        if isBookingDone {
            isBookingDone = false
            self.dismiss(animated: false, completion: nil)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.locationTxtFld.resignFirstResponder()
        self.frequencyTxtFld.resignFirstResponder()
        self.timesTxtFld.resignFirstResponder()
    }
    
    @objc func bookingDone(notification: Notification) {
        isBookingDone = true
    }
    
    private func scrollCurrentPage(isPrev: Bool) {
        let cal = Calendar.current
        var dateComponents = DateComponents()
        dateComponents.month = isPrev ? -1 : 1
        self.currentPage = cal.date(byAdding: dateComponents, to: self.currentPage ?? self.today)
        self.calender.setCurrentPage(self.currentPage!, animated: true)
        
    }
    
    //MARK: - Set Up stateDropdown
    func setupLocationDropdown() {
        locationDropdown.anchorView = locationTxtFld
        
        locationDropdown.bottomOffset = CGPoint(x: 0, y: locationTxtFld.bounds.height)
        
        locationDropdown.dataSource = locationArr
        locationDropdown.direction = .bottom
        
        // Action triggered on selection
        locationDropdown.selectionAction = { [weak self] (index, item) in
            self?.locationTxtFld.text = item
            self?.selectedLocationStr = item
            self?.locationTxtFld.isUserInteractionEnabled = true
            self?.locationDropdown.hide()
            let index = self?.locationArr.firstIndex(of: item)
            if let locations: [Int]  = UserDefaults.standard.value(forKey: "allLocations") as? [Int] {
                self?.selectedLocationID = locations[index!]
                self?.serverCallforAvailableChairs()
            } else {
            }
        }
        
        // Action triggered on dropdown cancelation (hide)
        //                locationDropdown.cancelAction = { [unowned self] in
        //                    // You could for example deselect the selected item
        //                    self.inputTxtFld.text = ""
        //                }
        
        // You can manually select a row if needed
        //        dropDown.selectRowAtIndex(3)
    }
    func setupFrequencyDropdown() {
        frequencyDropdown.anchorView = frequencyTxtFld
        
        frequencyDropdown.bottomOffset = CGPoint(x: 0, y: frequencyTxtFld.bounds.height)
        
        frequencyDropdown.dataSource = frequencyArr
        frequencyDropdown.direction = .bottom
        
        // Action triggered on selection
        frequencyDropdown.selectionAction = { [weak self] (index, item) in
            self?.frequencyTxtFld.text = item
            self?.frequencyTxtFld.isUserInteractionEnabled = true
            self?.frequencyDropdown.hide()
            self?.selectedFrequency = item
            if item != "One-off" {
                self?.timeViewHeight.constant = 100
                self?.view.layoutIfNeeded()
                self?.setupTimesDropdown()
            }
        }
        
        // Action triggered on dropdown cancelation (hide)
        //                locationDropdown.cancelAction = { [unowned self] in
        //                    // You could for example deselect the selected item
        //                    self.inputTxtFld.text = ""
        //                }
        
        // You can manually select a row if needed
        //        dropDown.selectRowAtIndex(3)
    }
    func setupTimesDropdown() {
        timesDropdown.anchorView = timesTxtFld
        timesDropdown.bottomOffset = CGPoint(x: 0, y: timesTxtFld.bounds.height)
        timesDropdown.dataSource = timesArr
        timesDropdown.direction = .bottom
        
        timesDropdown.selectionAction = { [weak self] (index, item) in
            self?.timesTxtFld.text = item
            self?.timesTxtFld.isUserInteractionEnabled = true
            self?.timesDropdown.hide()
            self?.selectedNoTimes = item
            self?.totalPrice = self!.totalPrice * Double(item)!
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        locationTxtFld.isUserInteractionEnabled = true
        frequencyTxtFld.isUserInteractionEnabled = true
        timesTxtFld.isUserInteractionEnabled = true
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func prevBtnTapped(_ sender: UIButton) {
        scrollCurrentPage(isPrev: true)
    }
    @IBAction func nextBtnTapped(_ sender: UIButton) {
        scrollCurrentPage(isPrev: false)
    }
    @IBAction func chairAdded(_ sender: UISwitch) {
        selectedChairs.removeAll()
        if sender.isOn {
            let sChair = chairs[sender.tag]
            selectedChairs.append(sChair.id)
            totalPrice += Double(sChair.cost)
        } else {
            let sChair = chairs[sender.tag]
            let chair_id = sChair.id
            if let index = selectedChairs.firstIndex(where: {$0 == chair_id}) {
                selectedChairs.remove(at: index)
                totalPrice -= Double(sChair.cost)
            }
        }
        DispatchQueue.main.async{
            self.bookingTable.reloadData()
        }
    }
    @IBAction func continueTapped(_ sender: UIButton) {
        if isChairsAvailable {
            self.performSegue(withIdentifier: "toBookingTime", sender: nil)
        } else {
            self.view.makeToast("No Chair Available", duration: 2.0, position: .bottom)
        }
        
    }
    
    //MARK:- Textfield Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == locationTxtFld {
            locationDropdown.show()
            locationTxtFld.isUserInteractionEnabled = false
        } else if textField == frequencyTxtFld {
            frequencyDropdown.show()
            frequencyTxtFld.isUserInteractionEnabled = false
        } else if textField == timesTxtFld {
            timesDropdown.show()
            timesTxtFld.isUserInteractionEnabled = false
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("end")
        locationTxtFld.isUserInteractionEnabled = true
        frequencyTxtFld.isUserInteractionEnabled = true
        timesTxtFld.isUserInteractionEnabled = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    

    
    // MARK: - tableview Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chairs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bookingTableCell") as! bookingTableCell
        cell.switchBtn.tag = indexPath.row
        let chair = chairs[indexPath.row]
        cell.titleLbl.text = String(format: "%@ - £%d/hr", chair.name,chair.cost)
        cell.ImgView.kf.setImage(with: URL(string: chair.image), placeholder:#imageLiteral(resourceName: "event_seat_grey"), options: nil, progressBlock: nil, completionHandler: nil)
        if self.selectedChairs.contains(chair.id){
            cell.switchBtn.setOn(true, animated: true)
        } else {
            cell.switchBtn.setOn(false, animated: true)
        }
        return cell
    }
    
    func serverCallUpdateDetails(){
        showProgressOnView(self.view)
        let params:[String: Any] = ["location_id": self.selectedLocationID,
                                    "date":selectedDateStr,
                                   "frequency":selectedFrequency,
                                   "chairs":[]]
        NetworkManager.sharedInstance.executeServiceWithToken(WEB_URL.chair_booking, postParameters: params, completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    DispatchQueue.main.async{
                        self.view.makeToast(response!["message"].stringValue, duration: 2.0, position: .bottom)
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            } else {
                self.view.makeToast("Some error occured.", duration: 2.0, position: .bottom)
            }
        })
    }
    
    func serverCallforAvailableChairs(){
        showProgressOnView(self.view)
        let params:[String: Any] = ["location_id": self.selectedLocationID,
                                    "date":selectedDateStr]
        NetworkManager.sharedInstance.executeServiceWithToken(WEB_URL.available_chair, postParameters: params, completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    self.chairs.removeAll()
                    let allClients = response!["data"].arrayValue
                    for item in allClients {
                        let client:Chair = Chair(id: item["id"].intValue, status: item["status"].intValue, name: item["name"].stringValue, image: item["image"].stringValue, cost: item["cost"].doubleValue, location_id: item["location_id"].intValue, created_at: item["created_at"].stringValue, updated_at: item["updated_at"].stringValue, deleted_at: "")
                        self.chairs.append(client)
                    }
                    if self.chairs.count > 0{
                        self.isChairsAvailable = true
                        if self.isRepeatBooking {
                            let allChairs:[BookingEntity] = self.repeatedBooking.booked
                            self.selectedChairs.removeAll()
                            self.totalPrice = 0.0
                            for item in allChairs {
                            self.selectedChairs.append(item.chair_id)
                            self.totalPrice += Double(item.chair_cost)
                            }
                        }
                    } else {
                        self.isChairsAvailable = false
                        self.view.makeToast("No Chair Available", duration: 2.0, position: .bottom)
                    }
                    DispatchQueue.main.async{
                        self.bookingTable.reloadData()
//                        self.view.makeToast(response!["message"].stringValue, duration: 2.0, position: .bottom)
                    }
                } else {
                    self.isChairsAvailable = false
                    self.view.makeToast("No Chair Available", duration: 2.0, position: .bottom)
                }
            } else {
                self.view.makeToast("Some error occured.", duration: 2.0, position: .bottom)
            }
        })
    }
 
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toBookingTime" {
            let  BookingTimeVc: BookingTimeViewController = segue.destination as! BookingTimeViewController
            var Dict:[String: Any] = [:]
            if isRepeatBooking {
                let bookingDateStr = selectedDateStr
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                let bookingDate = formatter.date(from: bookingDateStr)
                            
                let start_dateStr = repeatedBooking.start_time
                let startDateStr = "\(bookingDateStr) \(start_dateStr)"
                
                var end_dateStr = repeatedBooking.end_time
                end_dateStr = end_dateStr.trimmingCharacters(in: .whitespaces)
                let endDateStr = "\(bookingDateStr) \(end_dateStr)"
                
                let dF = DateFormatter()
                dF.dateFormat = "yyyy-MM-dd hh:mm a"
                dF.locale =  .current//Locale(identifier: "en_US_POSIX")
                let startDate = dF.date(from: startDateStr )
                let endDate = dF.date(from: endDateStr )
                Dict = ["location_id":selectedLocationID, "date":selectedDateStr, "frequency":selectedFrequency, "chairs": selectedChairs, "timeslot": "","location": selectedLocationStr, "totalPrice": totalPrice, "event_date": bookingDate!, "event_start_date":startDate!, "event_end_date": endDate! ]
                BookingTimeVc.isRepeatBooking = true
            } else {
             Dict = ["location_id":selectedLocationID, "date":selectedDateStr, "frequency":selectedFrequency, "chairs": selectedChairs, "timeslot": "","location": selectedLocationStr, "totalPrice": totalPrice ]
                BookingTimeVc.isRepeatBooking = false
            }
            
            BookingTimeVc.dict = Dict
            
            
        }
    }
    

}

extension NewChairBookingViewController: FSCalendarDelegate {
    func setCalendar() {
        calender.delegate = self
        calender.headerHeight = 0
        calender.scope = .month
        headerLbl.text = self.dateFormatter.string(from: calender.currentPage)
    }
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calenderHeight.constant = bounds.height + 10
        self.view.layoutIfNeeded()
    }

    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        self.headerLbl.text = self.dateFormatter.string(from: calendar.currentPage)
        
    }
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print("calendar did select date \(self.dateFormatter.string(from: date))")
        self.selectedDate = date
        self.selectedDateStr = self.dateFormatter2.string(from: date)
        self.serverCallforAvailableChairs()
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
        }
    }
}
