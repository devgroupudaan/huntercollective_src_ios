//
//  BookingChairViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 08/06/21.
//

import UIKit
import SwiftyJSON

class chairBookingCell: UITableViewCell {
    @IBOutlet weak var monthLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var chair_count: UILabel!
    @IBOutlet weak var total_duration: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var whiteBtn: DesignableButton!
    @IBOutlet weak var blackBtn: DesignableButton!
}

class BookingChairViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {


    @IBOutlet weak var chairBookingTblView: UITableView!
    
    var upcomingBookings = [BookData]()
    var pastBookings = [BookData]()
    
    var isRepeatBooking = false
    var selectedBooking: BookData!
    var bookingType = "upcoming"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        chairBookingTblView.delegate = self
        chairBookingTblView.dataSource = self
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        serverCallforBookings()
    }
    
    @IBAction func typeChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 1:
            bookingType = "past"
        default:
            bookingType = "upcoming"
        }
        chairBookingTblView.reloadData()
    }
    
    @IBAction func newChairBooking(_ sender: UIButton) {
        self.performSegue(withIdentifier: "toNewChair", sender: nil)
    }
    @IBAction func repeatBooking(_ sender: UIButton) {
        debugPrint("repeat Booking")
        selectedBooking = pastBookings[sender.tag]
        isRepeatBooking = true
        self.performSegue(withIdentifier: "toNewChair", sender: nil)
    }
    @IBAction func blackBtnAction(_ sender: UIButton) {
        if bookingType == "upcoming" {
            debugPrint("cancel")
            
            let deletAlert = UIAlertController(title: "Are you sure?", message: "This will remove the booking completely.", preferredStyle: UIAlertController.Style.alert)

            deletAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                  print("Handle Ok logic here")
                let booking = self.upcomingBookings[sender.tag]
                let bookingID = booking.id
                self.serverCallforDeleteBooking(deleteID: bookingID)
                
            }))

            deletAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
                  print("Handle Cancel Logic here")
                deletAlert.dismiss(animated: true, completion: nil)
            }))

            self.present(deletAlert, animated: true, completion: nil)
        } else {
            debugPrint("PDF Receipt")
        }
    }
    
    // MARK: - tableview Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if bookingType == "upcoming" {
            return upcomingBookings.count
        } else {
            return pastBookings.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "chairBookingCell") as! chairBookingCell
        if bookingType == "upcoming" {
            let booking = upcomingBookings[indexPath.row]
            let bookingDateStr = booking.booking_date
            let dF = DateFormatter()
            dF.dateFormat = "yyyy-MM-dd"
            let bookingDate = dF.date(from: bookingDateStr)
            let Day = bookingDate!.dayOfWeek()!.prefix(3)
            let month = bookingDate!.monthStrfromDate()
            let dateDigit = bookingDate!.dateDigitfromDate()
            cell.dayLbl.text = String(Day)
            cell.monthLbl.text = month
            cell.dateLbl.text = dateDigit
            cell.chair_count.text = "#\(booking.booked.count)"
            cell.location.text = booking.location.name
            
            cell.timeLbl.text = "\(booking.start_time) - \(booking.end_time)"//booking.booking_time
            
            let start_dateStr = booking.start_time
            let startDateStr = "\(bookingDateStr) \(start_dateStr)"
            var end_dateStr = booking.end_time
            end_dateStr = end_dateStr.trimmingCharacters(in: .whitespaces)
            let endDateStr = "\(bookingDateStr) \(end_dateStr)"
            dF.dateFormat = "yyyy-MM-dd hh:mm a"
            dF.locale =  .current//Locale(identifier: "en_US_POSIX")
            let startDate = dF.date(from: startDateStr )
            let endDate = dF.date(from: endDateStr )
            let diff = endDate!.seconds(from: startDate!)
            let hours = diff / 3600
            let minutes = Float(diff - hours * 3600) / Float(3600)
            cell.total_duration.text = "\(String(Float(hours) + minutes)) hours"
            
            cell.price.text = "£\(Float(booking.total_cost))"
//            cell.whiteBtn.isHidden = true
            cell.whiteBtn.isUserInteractionEnabled = false
            cell.whiteBtn.setTitle("", for: .normal)
            cell.whiteBtn.borderColor = .clear
            cell.blackBtn.setTitle("Cancel", for: .normal)
        } else {
            
            let booking = pastBookings[indexPath.row]
            let bookingDateStr = booking.booking_date
            let dF = DateFormatter()
            dF.dateFormat = "yyyy-MM-dd"
            let bookingDate = dF.date(from: bookingDateStr)
            let Day = bookingDate!.dayOfWeek()!.prefix(3)
            let month = bookingDate!.monthStrfromDate()
            let dateDigit = bookingDate!.dateDigitfromDate()
            cell.dayLbl.text = String(Day)
            cell.monthLbl.text = month
            cell.dateLbl.text = dateDigit
            cell.chair_count.text = "#\(booking.booked.count)"
            cell.location.text = booking.location.name
            cell.timeLbl.text = booking.booking_time
            
            cell.price.text = "£\(Float(booking.total_cost))"
            cell.whiteBtn.isUserInteractionEnabled = true
            cell.whiteBtn.setTitle("Repeat Booking", for: .normal)
            cell.whiteBtn.borderColor = .lightGray
            cell.whiteBtn.isHidden = false
            cell.blackBtn.setTitle("PDF Receipt", for: .normal)
        }
        cell.whiteBtn.tag = indexPath.row
        cell.blackBtn.tag = indexPath.row
        return cell
    }
    
    //MARK:- Server Calls
    func serverCallforBookings(){
        showProgressOnView(self.view)
        
        NetworkManager.sharedInstance.executeGetServiceWithToken(WEB_URL.get_booking,  completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    self.upcomingBookings.removeAll()
                    self.pastBookings.removeAll()
                    let allBookings = response!["book_data"].arrayValue
                    
                    for item in allBookings {
                        let bArr = item["booked"].arrayValue
                        var entities = [BookingEntity]()
                        for object in bArr {
                            let dict = object["chair"].dictionaryValue
                            var chair: Chair!
                            if dict.count > 0 {
                                chair = Chair(id: dict["id"]!.intValue, status: dict["status"]!.intValue, name: dict["name"]!.stringValue, image: dict["image"]!.stringValue, cost: dict["cost"]!.doubleValue, location_id: dict["location_id"]!.intValue, created_at: dict["created_at"]!.stringValue, updated_at: dict["updated_at"]!.stringValue, deleted_at: dict["deleted_at"]!.stringValue)
                            } else {
                                chair = Chair(id: 0, status: 0, name: "", image: "", cost: 0, location_id: 0, created_at: "", updated_at: "", deleted_at: "")
                            }
                            let currentEntity: BookingEntity = BookingEntity(id: object["id"].intValue, user_id: object["user_id"].intValue, booking_id: object["booking_id"].intValue, chair_id: object["chair_id"].intValue, chair_cost: object["chair_cost"].doubleValue, created_at: object["created_at"].stringValue, updated_at: object["updated_at"].stringValue, chair: chair)
                            entities.append(currentEntity)
                        }
                        let locDict = item["location"].dictionaryValue
                        var loc: MyLocation!
                        if locDict.count > 0 {
                         loc = MyLocation(id: locDict["id"]!.intValue, name: locDict["name"]!.stringValue, address1: locDict["address1"]!.stringValue, city: locDict["city"]!.stringValue, status: locDict["status"]!.stringValue, country: locDict["country"]!.stringValue)
                        } else {
                            loc = MyLocation(id: 0, name: "", address1: "", city: "", status: "", country: "")
                        }
                        let book_data:BookData = BookData(id: item["id"].intValue, user_id: item["user_id"].intValue, chair_id: item["chair_id"].intValue, location_id: item["location_id"].intValue, booking_date: item["boooking_date"].stringValue, booking_time: item["boooking_time"].stringValue, start_time: item["start_time"].stringValue, end_time: item["end_time"].stringValue, frequency: item["frequency"].stringValue, status: item["status"].boolValue, chair_name: item["chair_name"].stringValue, total_cost: item["total_cost"].doubleValue, created_at: item["created_at"].stringValue, updated_at: item["updated_at"].stringValue, booked: entities, location: loc)
                        let bookingDateStr = item["boooking_date"].stringValue
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd"
                        let bookingDate = formatter.date(from: bookingDateStr)
                        if bookingDate! > Date(){
                            self.upcomingBookings.append(book_data)
                        } else {
                            self.pastBookings.append(book_data)
                        }
                    }
                    DispatchQueue.main.async{
                        self.chairBookingTblView.reloadData()
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
    func serverCallforDeleteBooking(deleteID: Int){
        showProgressOnView(self.view)
        let params:[String: Any] = ["delete": deleteID]
        NetworkManager.sharedInstance.executeServiceWithToken(WEB_URL.delete_booking, postParameters: params, completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    DispatchQueue.main.async{
                        self.serverCallforBookings()
                        self.view.makeToast(response!["message"].stringValue, duration: 2.0, position: .bottom)
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            } else {
                self.view.makeToast("Some error occured.", duration: 2.0, position: .bottom)
            }
        })
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toNewChair"{
            if isRepeatBooking {
                let newChairBookingVC: NewChairBookingViewController = segue.destination as! NewChairBookingViewController
                newChairBookingVC.isRepeatBooking = true
                newChairBookingVC.repeatedBooking = self.selectedBooking
            } else{
                let newChairBookingVC: NewChairBookingViewController = segue.destination as! NewChairBookingViewController
                newChairBookingVC.isRepeatBooking = false
            }
        }
    }
    

}
