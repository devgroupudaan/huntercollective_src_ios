//
//  BookingTimeViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 17/08/21.
//

import UIKit
import SwiftyJSON
import CalendarKit
import EventKit
import EventKitUI

class BookingTimeViewController: UIViewController {

    @IBOutlet weak var confirmBookingPopUp: UIView!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var frequencylbl: UILabel!
    @IBOutlet weak var hourLbl: UILabel!
    @IBOutlet weak var totalpriceLbl: UILabel!
    @IBOutlet weak var confirmBookingBtn: UIButton!
    @IBOutlet weak var bookingSuccessPopUp: UIView!
    @IBOutlet weak var bookingNumber: UILabel!
    
    private let eventStore = EKEventStore()
    
    var selectedLocationID = 0
    var selectedLocationStr = ""
    var selectedFrequency = ""
    var selectedDate = Date()
    var selectedDateStr = ""
    var selectedChairs = [Int]()
    var totalPrice:Double = 0.0
    var eStartTime = ""
    var eEndTime = ""
    var timeSlot = ""
    
    var dict: [String: Any]! = [:]
    var isTimeSet = false
    var isRepeatBooking: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        selectedLocationID = dict["location_id"] as! Int
        selectedLocationStr = dict["location"] as! String
        selectedFrequency = dict["frequency"] as! String
        selectedDateStr = dict["date"] as! String
        selectedChairs = dict["chairs"] as! [Int]
        totalPrice = dict["totalPrice"] as! Double
        
        if isRepeatBooking{
            var repeatedTimeDict = [String: Any]()
            repeatedTimeDict = ["event_date": dict["event_date"] as! Date, "event_start_date":dict["event_start_date"] as! Date, "event_end_date": dict["event_end_date"] as! Date ]
            NotificationCenter.default.post(name: Notification.Name("repeatedTime"), object: repeatedTimeDict)
        }
        
        location.text = selectedLocationStr
        dateLbl.text = selectedDateStr
        frequencylbl.text = selectedFrequency
        totalpriceLbl.text = String(totalPrice)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.messagereceived(notification:)), name: NSNotification.Name(rawValue: "eventAdded"), object: nil)
    }
    @objc func messagereceived(notification: Notification) {
        isTimeSet = true
        let message = notification.object
        let arr = message as! [Date]
        let eventStartDate: Date = arr.first!
        let eventEndDate = arr.last!
        
        let diff = eventEndDate.seconds(from: eventStartDate)
        let hours = diff / 3600
        let minutes = Float(diff - hours * 3600) / Float(3600)
        hourLbl.text = String(Float(hours) + minutes)
        
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.init(identifier: "UTC")
//        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let eventStartDateStr = formatter.string(from: eventStartDate)
        let eventEndDateStr = formatter.string(from: eventEndDate)
        
        eStartTime = Utilities.shared.getLocalTimeFromUTC(strDate: eventStartDateStr)
        eEndTime = Utilities.shared.getLocalTimeFromUTC(strDate: eventEndDateStr)
        timeSlot = String(format: "%@ - %@", eStartTime, eEndTime)

        timeLbl.text = timeSlot
//        let timeArr = timeSlot.split(separator: "-")
//        var startDateStr: String = String(timeArr.first!)
//        startDateStr = startDateStr.trimmingCharacters(in: .whitespaces)
//        var endDateStr = String(timeArr.last!)
//        endDateStr = endDateStr.trimmingCharacters(in: .whitespaces)
//        let dF = DateFormatter()
//        dF.dateFormat = "hh:mm"
//        let startDate = dF.date(from: startDateStr as! String)
//        let endDate = dF.date(from: endDateStr as! String)
        print(timeSlot)
    }
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func confirmTapped(_ sender: UIButton) {
        if isTimeSet {
            confirmBookingPopUp.isHidden = false
        } else {
            self.view.makeToast("Please select a time slot.", duration: 2.0, position: .bottom)
        }
    }
   
    @IBAction func agreeBtnTapped(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            sender.setImage(#imageLiteral(resourceName: "Group 276"), for: .normal)
            confirmBookingBtn.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            confirmBookingBtn.isUserInteractionEnabled = false
        } else {
            sender.isSelected = true
            sender.setImage(#imageLiteral(resourceName: "Group 287"), for: .normal)
            confirmBookingBtn.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            confirmBookingBtn.isUserInteractionEnabled = true
        }
    }
    @IBAction func confirmBooking(_ sender: UIButton) {
        serverCallforChairBooking()
    }
    
    @IBAction func closeBookingSuccessPopUp(_ sender: UIButton) {
        bookingSuccessPopUp.isHidden = true
        NotificationCenter.default.post(name: Notification.Name("bookingDone"), object: nil)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    //MARK:-
    func serverCallforChairBooking(){
        showProgressOnView(self.view)
        let params:[String: Any] = ["location_id": self.selectedLocationID,
                                    "date":selectedDateStr,
                                   "frequency":selectedFrequency,
                                   "chairs":selectedChairs,"start_time":eStartTime, "end_time":eEndTime]
        NetworkManager.sharedInstance.executeServiceWithToken(WEB_URL.chair_booking, postParameters: params, completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    let dict = response!["data"].dictionaryValue
                    let id = dict["id"]?.intValue
                    DispatchQueue.main.async{
                        self.bookingNumber.text = "#\(id ?? 0)"
                        self.bookingSuccessPopUp.isHidden = false
//                        self.view.makeToast(response!["message"].stringValue, duration: 2.0, position: .bottom)
//                        let when = DispatchTime.now() + 2
//                        DispatchQueue.main.asyncAfter(deadline: when){
//                            self.dismiss(animated: true, completion: nil)
//                        }
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            } else {
                self.view.makeToast("Some error occured.", duration: 2.0, position: .bottom)
            }
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
