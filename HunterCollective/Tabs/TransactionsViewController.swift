//
//  TransactionsViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 08/06/21.
//

import UIKit
struct Transaction {
    var image: String
    var client_name: String
    var details: String
    var date: String
    var price: String
}

class tansactionCell: UITableViewCell {
    @IBOutlet weak var imgView: DesignableImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var detailsLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var pdfIcon: UIImageView!
}

class TransactionsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var transactionTbl: UITableView!
    @IBOutlet weak var clients_underline: UIView!
    @IBOutlet weak var hc_underline: UIView!
    
    var searchActive : Bool = false
    var filteredData = [Transaction]()
    
    var clientTransactions = [Transaction]()
    var hunterCollectiveTransactions = [Transaction]()
    var selectedClient: Transaction!
    var currentTransactionType = "client"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        transactionTbl.delegate = self
        transactionTbl.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func menuBtnAction(_ sender: UIButton) {
        HamburgerMenu().triggerSideMenu()
    }
    @IBAction func clientsTapped(_ sender: UIButton) {
        
    }
    @IBAction func hunterCollectiveTapped(_ sender: UIButton) {
    }
    
    // MARK: - tableview Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch currentTransactionType {
        case "client":
            return 3
        case "hunterCollective":
            return 1
        default:
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tansactionCell") as! tansactionCell
            
        
        return cell
    }
    //MARK: --- Search code ---
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        transactionTbl.reloadData()
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        switch currentTransactionType {
        case "client":
            filteredData = clientTransactions.filter{($0.client_name.lowercased().contains(searchText.lowercased()))}
        case "hunterCollective":
            filteredData = hunterCollectiveTransactions.filter{($0.client_name.lowercased().contains(searchText.lowercased()))}
        default:
            filteredData = clientTransactions.filter{($0.client_name.lowercased().contains(searchText.lowercased()))}
        }
        

        if(searchText.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.transactionTbl.reloadData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
