//
//  EditNoteViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 13/08/21.
//

import UIKit
import DropDown
import SwiftyJSON

class imageCell: UICollectionViewCell {
    @IBOutlet weak var imgView: DesignableImageView!
    
}

class EditNoteViewController: UIViewController, UIImagePickerControllerDelegate,UICollectionViewDelegate, UICollectionViewDataSource, UINavigationControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var clientTxtFld: UITextField!
    @IBOutlet weak var timeTxtFld: UITextField!
    @IBOutlet weak var descriptionTxtVw: UITextView!
    @IBOutlet weak var imgCollection: UICollectionView!
    @IBOutlet weak var imgCollectionviewHeight: NSLayoutConstraint!
    
    var addedImages = [UIImage]()
    var allImages = [UIImage]()
    let imagePicker = UIImagePickerController()
    
    let clientDropdown = DropDown()
    let frequencyDropdown = DropDown()
    
    var clients = [Client]()
    var selectedClient: Client!
    var clientArr = [String]()
    var frequencyArr = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgCollection.delegate = self
        imgCollection.dataSource = self
        
        clientTxtFld.delegate = self
        timeTxtFld.delegate = self
        allImages = [#imageLiteral(resourceName: "Group 473")]
        // Do any additional setup after loading the view.
        
        clientArr = ["client1","client2","client3","client4","client5"]
        frequencyArr = ["option1","option2","option3","option4","option5"]
        serverCallforClients()
        setupFrequencyDropdown()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        imgCollectionviewHeight.constant = imgCollection.contentSize.height
        imgCollection.layoutIfNeeded()
    }
    
    func showActionSheet() {
        let actionSheet = UIAlertController.init(title: "", message: "Please select one of the following", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction.init(title: "Camera", style: .default, handler: { (action) in
            self.showCamera()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Gallery", style: .default, handler: { (action) in
            self.showPhotoLibrary()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func showCamera() {
        self.imagePicker.sourceType = .camera
        self.imagePicker.allowsEditing = true
        self.imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    func showPhotoLibrary() {
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.allowsEditing = true
        self.imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        clientTxtFld.isUserInteractionEnabled = true
        timeTxtFld.isUserInteractionEnabled = true
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveNote(_ sender: DesignableButton) {
    }
    
    //MARK: - Set Up stateDropdown
    func setupClientDropdown() {
        clientDropdown.anchorView = clientTxtFld
        
        clientDropdown.bottomOffset = CGPoint(x: 0, y: clientTxtFld.bounds.height)
        
        clientDropdown.dataSource = clientArr
        clientDropdown.direction = .bottom
        
        // Action triggered on selection
        clientDropdown.selectionAction = { [weak self] (index, item) in
            self?.clientTxtFld.text = item
            self?.clientTxtFld.isUserInteractionEnabled = true
            self?.clientDropdown.hide()
        }
        
        // Action triggered on dropdown cancelation (hide)
        //                locationDropdown.cancelAction = { [unowned self] in
        //                    // You could for example deselect the selected item
        //                    self.inputTxtFld.text = ""
        //                }
        
        // You can manually select a row if needed
        //        dropDown.selectRowAtIndex(3)
    }
    func setupFrequencyDropdown() {
        frequencyDropdown.anchorView = timeTxtFld
        
        frequencyDropdown.bottomOffset = CGPoint(x: 0, y: timeTxtFld.bounds.height)
        
        frequencyDropdown.dataSource = frequencyArr
        frequencyDropdown.direction = .bottom
        
        // Action triggered on selection
        frequencyDropdown.selectionAction = { [weak self] (index, item) in
            self?.timeTxtFld.text = item
            self?.timeTxtFld.isUserInteractionEnabled = true
            self?.frequencyDropdown.hide()
        }
        
        // Action triggered on dropdown cancelation (hide)
        //                locationDropdown.cancelAction = { [unowned self] in
        //                    // You could for example deselect the selected item
        //                    self.inputTxtFld.text = ""
        //                }
        
        // You can manually select a row if needed
        //        dropDown.selectRowAtIndex(3)
    }
    
    
    //MARK:- Textfield Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == clientTxtFld {
            clientDropdown.show()
            clientTxtFld.isUserInteractionEnabled = false
        } else if textField == timeTxtFld {
            frequencyDropdown.show()
            timeTxtFld.isUserInteractionEnabled = false
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("end")
        clientTxtFld.isUserInteractionEnabled = true
        timeTxtFld.isUserInteractionEnabled = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    //MARK:- UIImagePickerControlloller Delegates
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.addedImages.append(image)
            self.allImages = addedImages + [#imageLiteral(resourceName: "Group 473")]
            self.imgCollection.reloadData()
            let when = DispatchTime.now() + 0.5
            DispatchQueue.main.asyncAfter(deadline: when){
                self.imgCollectionviewHeight.constant = self.imgCollection.contentSize.height
                self.imgCollection.layoutIfNeeded()
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - collectionView Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return allImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! imageCell
        let img =  allImages[indexPath.row]
            cell.imgView.image = allImages[indexPath.row]
            
            return cell
        
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//
//    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 10;
        }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            if indexPath.row == allImages.count - 1 {
                self.showActionSheet()
            }
    }
    
    //MARK:- Server Calls
    func serverCallforClients(){
        showProgressOnView(self.view)
        
        NetworkManager.sharedInstance.executeGetServiceWithToken(WEB_URL.getClients,  completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    self.clients.removeAll()
                    self.clientArr.removeAll()
                    let allClients = response!["data"].arrayValue
                    for item in allClients {
                        let client:Client = Client(image: item["image"].stringValue, name: item["name"].stringValue, email: item["email"].stringValue, mobile: item["mobile"].stringValue)
                        self.clients.append(client)
                        self.clientArr.append(item["name"].stringValue)
                    }
                    DispatchQueue.main.async{
                        self.setupClientDropdown()
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
    func serverCallforCreateNote(){
        showProgressOnView(self.view)
        let params:[String: Any] = ["client_id":173,
                                    "appointment_id":70,
                                    "description":"This is another message"]
        NetworkManager.sharedInstance.executeServiceWithToken(WEB_URL.create_notes, postParameters: params, completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    DispatchQueue.main.async{
                        self.view.makeToast(response!["message"].stringValue, duration: 2.0, position: .bottom)
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            } else {
                self.view.makeToast("Some error occured.", duration: 2.0, position: .bottom)
            }
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
