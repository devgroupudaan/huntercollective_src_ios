//
//  NotesViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 08/06/21.
//

import UIKit
import SwiftyJSON

class noteCell: UICollectionViewCell {
    @IBOutlet weak var clientName: UILabel!
    @IBOutlet weak var noteLbl: UILabel!
    @IBOutlet weak var shareBtn: DesignableButton!
    @IBOutlet weak var editBtn: DesignableButton!
    
}

class NotesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var notesCollection: UICollectionView!
    @IBOutlet weak var dateTimeLbl: UILabel!
    @IBOutlet weak var serviceLbl: UILabel!
    
    var notes = [Note]()
    override func viewDidLoad() {
        super.viewDidLoad()
        notesCollection.delegate = self
        notesCollection.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.serverCallforNotes()
    }
    
    @IBAction func menuBtnAction(_ sender: UIButton) {
        HamburgerMenu().triggerSideMenu()
    }
    @IBAction func newNote(_ sender: UIButton) {
        self.performSegue(withIdentifier: "newNote", sender: nil)
    }
    @IBAction func shareNote(_ sender: UIButton) {
        
        let sharedObjects: [AnyObject] = ["https://huntercollective.global" as AnyObject]
        let activityViewController = UIActivityViewController(activityItems: sharedObjects, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func editNote(_ sender: UIButton) {
    }
    
    // MARK: - collectionView Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "noteCell", for: indexPath) as! noteCell
        let note = notes[indexPath.row]
        cell.clientName.text = note.users.name
        cell.noteLbl.text = note.description
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let W = notesCollection.frame.width
        return CGSize(width: (W - 10)/2 , height: (W - 10) * 3/5);
    
        }
    
    //MARK:- Server Calls
    func serverCallforNotes(){
        showProgressOnView(self.view)
//        let urlString = String(format: "\(WEB_URL.get_notes)?query=%@", )
        NetworkManager.sharedInstance.executeGetServiceWithToken(WEB_URL.get_notes,  completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    self.notes.removeAll()
                    let allNotes = response!["data"].arrayValue
                    for item in allNotes {
                        let userDict = item["users"].dictionaryValue
                        let user: User = User(id: userDict["id"]!.intValue, name: userDict["name"]!.stringValue, email:  userDict["email"]!.stringValue, status: userDict["status"]!.boolValue, image: userDict["image"]!.stringValue, role: userDict["role"]!.intValue, mobile: userDict["mobile"]!.stringValue, date_o_birth: userDict["date_o_birth"]!.stringValue, address: userDict["address"]!.stringValue, introduction: userDict["introduction"]!.stringValue, accept_client: userDict["accept_client"]!.boolValue, created_at: userDict["created_at"]!.stringValue, updated_at: userDict["updated_at"]!.stringValue)
                        let note: Note = Note(id: item["id"].intValue, user_id: item["user_id"].intValue, client_id: item["client_id"].intValue, appointment_id: item["appointment_id"].intValue, description: item["description"].stringValue, image: item["image"].stringValue, created_at: item["created_at"].stringValue, updated_at: item["updated_at"].stringValue, users: user)
                        self.notes.append(note)
                    }
                    DispatchQueue.main.async{
                        self.notesCollection.reloadData()
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
