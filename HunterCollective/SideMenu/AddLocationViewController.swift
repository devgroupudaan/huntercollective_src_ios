//
//  AddLocationViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 08/06/21.
//

import UIKit
import Toast_Swift
import SwiftyJSON

class locCell: UITableViewCell {
    @IBOutlet weak var loc_image: DesignableImageView!
    @IBOutlet weak var loc_name: UILabel!
    @IBOutlet weak var selectBtn: DesignableButton!
    
}
class locationCell: UICollectionViewCell {
    @IBOutlet weak var ImgView: DesignableImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var chairCount: UILabel!
    @IBOutlet weak var alphaView: DesignableView!
    @IBOutlet weak var selectionLbl: UILabel!
}

class AddLocationViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var locationCollectionView: UICollectionView!
    @IBOutlet weak var locationTblView: UITableView!
    @IBOutlet weak var popUpView: UIView!
    
//    var searchActive : Bool = false
//    var filteredData = [Location]()
    
    var locations = [Location]()
    var selectedLocations = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationCollectionView.delegate = self
        locationCollectionView.dataSource = self
        locationTblView.delegate = self
        locationTblView.dataSource = self
        
        searchBar.delegate = self
        searchBar.backgroundColor = .clear
        searchBar.tintColor = .clear
        searchBar.backgroundImage = UIImage()
        
//        locations = [Location(Img: #imageLiteral(resourceName: "Farringdon"), location_name: "Farringdon", chair_count: 19), Location(Img: #imageLiteral(resourceName: "Mayfair"), location_name: "Covent Garden", chair_count: 17), Location(Img: #imageLiteral(resourceName: "Putney"), location_name: "Mayfair", chair_count: 20),Location(Img: #imageLiteral(resourceName: "Highbury"), location_name: "Shoreditch", chair_count: 12)]
        selectedLocations.removeAll()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let locations: [Int]  = UserDefaults.standard.value(forKey: "allLocations") as? [Int] {
            selectedLocations = locations
        }
        serverCallforLocations(query: "")
    }
    
    // MARK: - Button Actions
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func selectLocation(_ sender: UIButton) {
        var loc: Location = locations[sender.tag]
        if selectedLocations.contains(loc.id) {
            let index = selectedLocations.firstIndex(of: loc.id)
            selectedLocations.remove(at: index!)
            let cell = locationTblView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! locCell //collectionView.cellForItem(at: indexPath) as! locationCell
            cell.selectBtn.isSelected = false
            cell.selectBtn.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        } else {
            if loc.chairs_count == 0 {
                var style = ToastStyle()
                style.messageColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                style.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                self.view.makeToast("No chair available", duration: 1.5, position: .bottom, style: style)
            } else {
                selectedLocations.append(loc.id)
                let cell = locationTblView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! locCell
                cell.selectBtn.isSelected = true
                cell.selectBtn.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
        }
    }
    @IBAction func addLocations(_ sender: UIButton) {
//        UserDefaults.standard.setValue(selectedLocations, forKey: "allLocations")
        self.serverCalltoAddLocations()
        
    }
    @IBAction func popUpDone(_ sender: UIButton) {
        popUpView.isHidden = true
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - tableview Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "locCell") as! locCell
        var loc: Location = locations[indexPath.row]
        let image_url = loc.image
        if image_url.contains("https://huntercollective-staging.udaantechnologies.com") {
            cell.loc_image.kf.setImage(with: URL(string: image_url), placeholder:#imageLiteral(resourceName: "person-24px 1"), options: nil, progressBlock: nil, completionHandler: nil)
        } else {
        cell.loc_image.kf.setImage(with: URL(string: (WEB_URL.MainUrl + image_url)), placeholder:#imageLiteral(resourceName: "person-24px 1"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        cell.loc_name.text = loc.name
        if selectedLocations.contains(loc.id){
            cell.selectBtn.isSelected = true
            cell.selectBtn.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        } else {
            cell.selectBtn.isSelected = false
            cell.selectBtn.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        cell.selectBtn.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    // MARK: - collectionView Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        if searchActive {
//            return filteredData.count
//        } else {
            return locations.count
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "locationCell", for: indexPath) as! locationCell
        var loc: Location!
//        if searchActive {
//            loc = filteredData[indexPath.row]
//        } else {
            loc = locations[indexPath.row]
//        }
        cell.ImgView.kf.setImage(with: URL(string: loc.image), placeholder:#imageLiteral(resourceName: "person-24px 1"), options: nil, progressBlock: nil, completionHandler: nil)
        cell.titleLbl.text = loc.name
        if selectedLocations.contains(loc.id){
            cell.alphaView.isHidden = false
            cell.selectionLbl.isHidden = false
            cell.selectionLbl.text = String(format: "Premise No. %d", (selectedLocations.firstIndex(of: loc.id)! + 1))
        } else {
            cell.alphaView.isHidden = true
            cell.selectionLbl.isHidden = true
        }
        cell.chairCount.text = String(loc.chairs_count) + " Chairs"
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let W = locationCollectionView.frame.width
            return CGSize(width: (W - 10)/2 , height: (W - 10)/2);
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 10;
        }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var loc: Location!
//        if searchActive {
//            loc = filteredData[indexPath.row]
//        } else {
            loc = locations[indexPath.row]
//        }
        if selectedLocations.contains(loc.id) {
            let index = selectedLocations.firstIndex(of: loc.id)
            selectedLocations.remove(at: index!)
            let cell = collectionView.cellForItem(at: indexPath) as! locationCell
            cell.alphaView.isHidden = true
            cell.selectionLbl.isHidden = true
            
        } else {
            if loc.chairs_count == 0 {
                var style = ToastStyle()
                style.messageColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                style.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                self.view.makeToast("No chair available", duration: 1.5, position: .bottom, style: style)
            } else {
                selectedLocations.append(loc.id)
                let cell = collectionView.cellForItem(at: indexPath) as! locationCell
                cell.alphaView.isHidden = false
                cell.selectionLbl.isHidden = false
                cell.selectionLbl.text = String(format: "Premise No. %d", selectedLocations.count)
            }
        }
    }
    
    //MARK: --- Search code ---
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        searchActive = false;
//        locationCollectionView.reloadData()
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
//        filteredData = locations.filter{($0.name.lowercased().contains(searchText.lowercased()))}
//
//        if(searchText.count == 0){
//            searchActive = false;
//        } else {
//            searchActive = true;
//        }
//        self.locationCollectionView.reloadData()
        serverCallforLocations(query: searchText)
    }
    
    //MARK:- Server Calls
    func serverCallforLocations(query:String){
        showProgressOnView(self.view)
        let urlString = String(format: "\(WEB_URL.getLocations)?query=%@", query)
        
        NetworkManager.sharedInstance.executeGetServiceWithToken(urlString,  completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    self.locations.removeAll()
                    let allLocations = response!["data"].arrayValue
                    for item in allLocations {
                        let countryNameDict = item["countryName"].dictionaryValue
                        let c_info: countryName = countryName(id: countryNameDict["id"]!.intValue, iso: countryNameDict["iso"]!.stringValue, name: countryNameDict["name"]!.stringValue, nicename: countryNameDict["nicename"]!.stringValue, iso3: countryNameDict["iso3"]!.stringValue, numcode: countryNameDict["numcode"]!.intValue, phonecode: countryNameDict["phonecode"]!.intValue)
                        let location:Location = Location(id: item["id"].intValue, name: item["name"].stringValue, address1: item["address1"].stringValue, city: item["city"].stringValue, status: item["status"].stringValue, chairs_count: item["chairs_count"].intValue, countryName: c_info, image: item["image"].stringValue)
                        if item["status"].stringValue == "active" {
                            self.locations.append(location)
                        }
                    }
                    DispatchQueue.main.async{
                        self.locationCollectionView.reloadData()
                        self.locationTblView.reloadData()
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })
    }
    
    func serverCalltoAddLocations(){
        
        showProgressOnView(self.view)
        let params:[String: Any] = ["locations": self.selectedLocations]
        
        NetworkManager.sharedInstance.executeServiceWithToken(WEB_URL.addLocations, postParameters: params, completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    DispatchQueue.main.async{
                            self.popUpView.isHidden = false
                        let notificationLocationChanged = Notification.Name("locationAdded")
                        NotificationCenter.default.post(name: notificationLocationChanged, object: nil)
                        self.dismiss(animated: true, completion: nil)
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            } else {
                self.view.makeToast("Some error occured.", duration: 2.0, position: .bottom)
            }
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
