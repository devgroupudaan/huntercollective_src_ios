//
//  BookingLinkViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 09/06/21.
//

import UIKit
import LinkPresentation

class BookingLinkViewController: UIViewController, UIActivityItemSource {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func shareLink(_ sender: UIButton) {
        let sharedObjects: [AnyObject] = ["https://huntercollective.global" as AnyObject]
//        let activityViewController = UIActivityViewController(activityItems: sharedObjects, applicationActivities: nil)
        let activityViewController = UIActivityViewController(activityItems: [self], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
//        activityViewController.excludedActivityTypes = [.addToReadingList,
//                                                        .airDrop,
//                                                        .assignToContact,
//                                                        .copyToPasteboard,
//                                                        .mail,
//                                                        .message,
//                                                        .openInIBooks,
//                                                        .print,
//                                                        .saveToCameraRoll,
//                                                        .postToWeibo,
//                                                        .copyToPasteboard,
//                                                        .saveToCameraRoll,
//                                                        .postToFlickr,
//                                                        .postToVimeo,
//                                                        .postToTencentWeibo,
//        ]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return ""
    }

    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        return nil
    }

    @available(iOS 13.0, *)
    func activityViewControllerLinkMetadata(_ activityViewController: UIActivityViewController) -> LPLinkMetadata? {
        let image = UIImage(named: "AppIcon")!
        let imageProvider = NSItemProvider(object: image)
        let metadata = LPLinkMetadata()
        metadata.imageProvider = imageProvider
        metadata.title = "Hunter Collective Booking Link"
        metadata.url =  URL(string: "https://huntercollective.global") 
        return metadata
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
