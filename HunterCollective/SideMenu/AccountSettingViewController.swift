//
//  AccountSettingViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 04/06/21.
//

import UIKit
import SwiftyJSON




class settingCell: UITableViewCell {
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
}

class AccountSettingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var profile_pic: DesignableImageView!
    @IBOutlet weak var user_name: UILabel!
    @IBOutlet weak var user_email: UILabel!
    
    @IBOutlet weak var settingTable: UITableView!
    
    var UserDetails: UserDetail!
    override func viewDidLoad() {
        super.viewDidLoad()
        settingTable.delegate = self
        settingTable.dataSource = self
        
        if let fullName  = UserDefaults.standard.value(forKey: "fullName"){
            user_name.text = fullName as? String
        }
        if let email  = UserDefaults.standard.value(forKey: "email"){
            user_email.text = (email as! String)
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.serverCallUserDetails()
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func deleteAccountTapped(_ sender: UIButton) {
    }
    
    // MARK: - tableview Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "settingCell") as! settingCell
            switch indexPath.row {
            case 1:
                cell.iconView.image = #imageLiteral(resourceName: "credit-card")
                cell.titleLbl.text = "Payment Methods"
            case 2:
                cell.iconView.image = #imageLiteral(resourceName: "notifications")
                cell.titleLbl.text = "Notifications"
            case 3:
                cell.iconView.image = #imageLiteral(resourceName: "people-24px")
                cell.titleLbl.text = "Invite Friends"
//            case 4:
//                cell.iconView.image = #imageLiteral(resourceName: "settings-24px")
//                cell.titleLbl.text = "Settings"
//            case 5:
//                cell.iconView.image = #imageLiteral(resourceName: "help-24px 1")
//                cell.titleLbl.text = "Help"
//            case 6:
//                cell.iconView.image = #imageLiteral(resourceName: "info-24px")
//                cell.titleLbl.text = "About Us"
            default:
                cell.iconView.image = #imageLiteral(resourceName: "person-24px 1")
                cell.titleLbl.text = "Profile"
            }
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            self.performSegue(withIdentifier: "toEditAccount", sender: nil)
        case 1:
            self.performSegue(withIdentifier: "toPaymentMethods", sender: nil)
        default:
            print("nothing")
        }
    }
    
    //MARK:- Server Calls
    func serverCallUserDetails(){
        showProgressOnView(self.view)
        
        NetworkManager.sharedInstance.executeGetServiceWithToken(WEB_URL.account_details,  completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    
                    let user_detail = response!["data"][0].dictionaryValue
                    let tags = user_detail["tags"]!.arrayValue
                    var addedTgs: [Tag] = []
                    for tag in tags{
                        let tg = Tag(id: tag["id"].intValue, name: tag["name"].stringValue)
                        addedTgs.append(tg)
                    }
                    self.UserDetails = UserDetail(name: user_detail["name"]!.stringValue, email: user_detail["email"]!.stringValue, image: user_detail["image"]!.stringValue, mobile: user_detail["mobile"]!.stringValue, date_o_birth: user_detail["date_o_birth"]!.stringValue, address: user_detail["address"]!.stringValue, introduction: user_detail["introduction"]!.stringValue, accept_client: user_detail["accept_client"]!.boolValue, tags: addedTgs)
                    
                    DispatchQueue.main.async{
                        self.user_name.text = user_detail["name"]?.stringValue
                        self.user_email.text = user_detail["email"]!.stringValue
                        self.profile_pic.kf.setImage(with: URL(string: user_detail["image"]!.stringValue), placeholder:#imageLiteral(resourceName: "person-24px 1"), options: nil, progressBlock: nil, completionHandler: nil)
                    }
                    
                    if let encodedCurrenUser = try? JSONEncoder().encode(self.UserDetails) {
                        let defaults = UserDefaults.standard
                        defaults.set(encodedCurrenUser, forKey: "currentUser")
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
