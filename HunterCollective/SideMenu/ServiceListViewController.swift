//
//  ServiceListViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 11/06/21.
//

import UIKit
import SwiftyJSON



class serviceListCell: UITableViewCell{
    @IBOutlet weak var bgView: DesignableView!
    @IBOutlet weak var imgView: DesignableImageView!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var service_description: UILabel!
    @IBOutlet weak var charges: UILabel!
    
}

class ServiceListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var serviceTbl: UITableView!
    
    var services = [MyService]()
    var selectedServiceID: Int!
    var selectedIndexpath: IndexPath!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        serviceTbl.delegate = self
        serviceTbl.dataSource = self

//        services = [Service(image: "1", title: "Mens Cut", description: "Excluding Wash", price:65.00, require_add_on: false, time: "1hr"),Service(image: "2", title: "Ladies Cut", description: "Including Wash + Blow Dry", price: 90.00, require_add_on: false, time: "1hr"),Service(image: "3", title: "Blow Dry - Short Hair", description: "Including Wash", price: 40.00, require_add_on: false, time: "30min"),Service(image: "4", title: "Blow Dry - Long Hair", description: "Including Wash", price: 60.00, require_add_on: false, time: "1hr"),Service(image: "5", title: "Full Head Colour - Roots to End", description: "Excluding Wash + Blow Dry", price: 85.00, require_add_on: false, time: "1hr")]
        
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.serverCallforServices()
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func AddService(_ sender: UIButton) {
        self.performSegue(withIdentifier: "toCategory", sender: nil)
    }
    

    
    // MARK: - tableview Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return services.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "serviceListCell") as! serviceListCell
        let service = services[indexPath.row]
//        cell.imgView.image = UIImage(named: service.image)
        cell.imgView.kf.setImage(with: URL(string: service.image), placeholder:#imageLiteral(resourceName: "person-24px 1"), options: nil, progressBlock: nil, completionHandler: nil)
        cell.serviceName.text = service.title
        cell.service_description.text = service.description
        cell.charges.text = "£\(service.price) (\(Utilities.shared.hourMinutesSecondsToSeconds(minSec: service.time)))"//service.time
        
        if indexPath == selectedIndexpath {
            cell.bgView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        } else {
            cell.bgView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
//        let backgroundView = UIView()
//            backgroundView.backgroundColor = #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1)
//            cell.selectedBackgroundView = backgroundView
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let cell = tableView.cellForRow(at: indexPath) as! serviceListCell
//        cell.bgView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        let service = services[indexPath.row]
        selectedServiceID = service.id
        self.performSegue(withIdentifier: "toEditService", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        let cell = tableView.cellForRow(at: indexPath) as! serviceListCell
        cell.bgView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        return indexPath
    }
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! serviceListCell
        selectedIndexpath = indexPath
        cell.bgView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    }
    
    func tableView(_ tableView: UITableView,
                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let deleteAction = UIContextualAction(style: .normal, title:  "", handler: {
            (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            print("delete")
            
            let deletAlert = UIAlertController(title: "Are you sure?", message: "This service will be deleted from your account.", preferredStyle: UIAlertController.Style.alert)

            deletAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                  print("Handle Ok logic here")
                let service = self.services[indexPath.row]
                let service_id = service.id
                self.servercalltoDeleteService(serviceID: service_id)
                
            }))

            deletAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
                  print("Handle Cancel Logic here")
                deletAlert.dismiss(animated: true, completion: nil)
            }))

            self.present(deletAlert, animated: true, completion: nil)
            success(true)
            
        })
        deleteAction.backgroundColor = UIColor.init(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.0)//.red
        if #available(iOS 13.0, *) {
            deleteAction.image = UIImage.init(named: "icons8-waste")!.withRenderingMode(.alwaysOriginal).withTintColor(#colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1))
        } else {
            // Fallback on earlier versions
            deleteAction.image = UIImage.init(named: "icons8-waste")!.withRenderingMode(.alwaysOriginal)
            deleteAction.backgroundColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
        }
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    //MARK:- Server Calls
    func serverCallforServices(){
        showProgressOnView(self.view)
        
        NetworkManager.sharedInstance.executeGetServiceWithToken(WEB_URL.my_services,  completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    self.services.removeAll()
                    let allServices = response!["data"].arrayValue
                    for item in allServices {
                        let cat_infoDict = item["category"].dictionaryValue
                        let cat_info: Service_category = Service_category(id: cat_infoDict["id"]!.intValue, name: cat_infoDict["name"]!.stringValue)
                        let service:MyService = MyService(id: item["id"].intValue, image: item["image"].stringValue, title: item["title"].stringValue, description: item["description"].stringValue, price: item["price"].doubleValue, require_add_on: item["require_add_on"].boolValue, time: item["time"].stringValue, category: cat_info)
                        self.services.append(service)
                    }
                    DispatchQueue.main.async{
                        self.serviceTbl.reloadData()
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
    
    func servercalltoDeleteService(serviceID: Int){
        showProgressOnView(self.view)
        let url = WEB_URL.MainUrl + String(format: "api/delete/%d/service", serviceID)
        NetworkManager.sharedInstance.deleteAPIwithToken(parameter: [:], url: url, {(json) in
                if json["status"].stringValue  == "success"{
                    hideProgressOnView(self.view)
                    DispatchQueue.main.async {
                        self.view.makeToast(json["message"].stringValue, duration: 2.0, position: .bottom)
                        let when = DispatchTime.now() + 2
                        DispatchQueue.main.asyncAfter(deadline: when){
                            self.serverCallforServices()
                        }
                    }
                } else {
//                    let error =
                    hideProgressOnView(self.view)
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
        })
        { (error) in
            debugPrint(error)
            DispatchQueue.main.async{
        hideProgressOnView(self.view)
                self.view.makeToast("Service Deletion failed", duration: 3.0, position: .bottom)
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toEditService" {
            let editServiceVc: EditServiceViewController = segue.destination as! EditServiceViewController
            editServiceVc.service_id = self.selectedServiceID
        }
    }
    

}
