//
//  SideMenuViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 03/06/21.
//

import UIKit

class menuCell: UITableViewCell {
    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    
}

class SideMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var user_img: DesignableImageView!
    @IBOutlet weak var user_name: UILabel!
    @IBOutlet weak var menuTable: UITableView!
    
    var secondSectionItems = [String]()
    var currentUser: UserDetail!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuTable.delegate = self
        menuTable.dataSource = self
        
        if let fullName  = UserDefaults.standard.value(forKey: "fullName") {
            user_name.text = fullName as? String
        }
        NotificationCenter.default.addObserver(self, selector: #selector(LocationAdded), name: Notification.Name("locationsUpdated"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let locations: [String]  = UserDefaults.standard.value(forKey: "allLocationNames") as? [String] {
            secondSectionItems.removeAll()
            secondSectionItems.append("Add Location")
            secondSectionItems.append(contentsOf: locations)
        } else {
            secondSectionItems = ["Add Location"]
        }
        menuTable.reloadData()
        
        if let current_userData = UserDefaults.standard.object(forKey: "currentUser") as? Data {
            if let current_user = try? JSONDecoder().decode(UserDetail.self, from: current_userData){
                self.currentUser = current_user
            }
        }
        else {
            self.currentUser = UserDetail(name: "", email: "", image: "", mobile: "", date_o_birth: "", address: "", introduction: "", accept_client: false, tags: [Tag(id: 0, name: "")])
        }
        
        self.user_name.text = currentUser.name
        if let image = currentUser.image as? String {
//            let imgURL = WEB_URL.MainUrl + image
            self.user_img.kf.setImage(with: URL(string: image), placeholder:#imageLiteral(resourceName: "person-24px 1"), options: nil, progressBlock: nil, completionHandler: nil)
        }
    }

    @objc func LocationAdded(){
        if let locations: [String]  = UserDefaults.standard.value(forKey: "allLocationNames") as? [String] {
            secondSectionItems.removeAll()
            secondSectionItems.append("Add Location")
            secondSectionItems.append(contentsOf: locations)
        } else {
            secondSectionItems = ["Add Location"]
        }
        menuTable.reloadData()
    }
    
    @IBAction func accountTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "toAccountSetting", sender: nil)
    }
    @IBAction func LogoutTapped(_ sender: UIButton) {
//        UserDefaults.standard.set(false, forKey: "isLoggedIn")
        let defaults = UserDefaults.standard
            let dictionary = defaults.dictionaryRepresentation()
            dictionary.keys.forEach { key in
                defaults.removeObject(forKey: key)
            }
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "loginVC") as! SignInViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = vc
        HamburgerMenu().closeSideMenu()
    }
    
    // MARK: - tableview Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1:
            return secondSectionItems.count
        default:
            return 4
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell") as! menuCell
        switch indexPath.section {
        case 1:
//            switch indexPath.row {
//            case 1:
//                cell.iconImg.image = #imageLiteral(resourceName: "my_location")
//                cell.titleLbl.text = "Farringdon"
//            case 2:
//                cell.iconImg.image = #imageLiteral(resourceName: "my_location")
//                cell.titleLbl.text = "Shoreditch"
//            default:
//                cell.iconImg.image = #imageLiteral(resourceName: "domain")
//                cell.titleLbl.text = "Add Location"
//            }
        
            if indexPath.row == 0 {
                cell.iconImg.image = #imageLiteral(resourceName: "domain")
            } else {
                cell.iconImg.image = #imageLiteral(resourceName: "my_location")
            }
            cell.titleLbl.text = secondSectionItems[indexPath.row]
            if let selectedLocation  = UserDefaults.standard.value(forKey: "selectedLocation") {
                if selectedLocation as! String == secondSectionItems[indexPath.row] {
                    cell.titleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.348244863)
                    cell.iconImg.image = #imageLiteral(resourceName: "my_location").withRenderingMode(.alwaysTemplate)
                    cell.iconImg.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.348244863)
                } else {
                    cell.titleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    cell.iconImg.image = #imageLiteral(resourceName: "my_location").withRenderingMode(.alwaysTemplate)
                    cell.iconImg.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
            } else {
                if indexPath.row == 1 {
                    cell.titleLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.348244863)
                    cell.iconImg.image = #imageLiteral(resourceName: "my_location").withRenderingMode(.alwaysTemplate)
                    cell.iconImg.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.348244863)
                }
            }
        
        default:
            switch indexPath.row {
            case 1:
                cell.iconImg.image = #imageLiteral(resourceName: "message")
                cell.titleLbl.text = "Messages"
            case 2:
                cell.iconImg.image = #imageLiteral(resourceName: "inventory")
                cell.titleLbl.text = "Service List"
            case 3:
                cell.iconImg.image = #imageLiteral(resourceName: "share")
                cell.titleLbl.text = "Booking Link"
            default:
                cell.iconImg.image = #imageLiteral(resourceName: "clients")
                cell.titleLbl.text = "Clients"
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            let label = UILabel()
            label.text = "      GENERAL"
            label.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            label.font = UIFont.boldSystemFont(ofSize: 14)
            return label
        case 1:
            let label = UILabel()
            label.text = "      LOCATION"
            label.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            label.font = UIFont.boldSystemFont(ofSize: 14)
            return label
        default:
            let label = UILabel()
            label.text = "      GENERAL"
            label.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            label.font = UIFont.boldSystemFont(ofSize: 14)
            return label
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 24
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch (indexPath.section,indexPath.row) {
        case (0,0):
            self.performSegue(withIdentifier: "toClients", sender: nil)
        case (0,1):
            self.performSegue(withIdentifier: "toMessage", sender: nil)
        case (0,2):
            self.performSegue(withIdentifier: "toServices", sender: nil)
        case (0,3):
            self.performSegue(withIdentifier: "toBookingLink", sender: nil)
        case (1,0):
            self.performSegue(withIdentifier: "toAddLocation", sender: nil)
//        case (1,1):
//            let location = secondSectionItems[indexPath.row]
//            UserDefaults.standard.setValue(location, forKey: "selectedLocation")
        default:
            print("nothing")
        }
        if indexPath.section == 1 && indexPath.row > 0{
            let location = secondSectionItems[indexPath.row]
            UserDefaults.standard.setValue(location, forKey: "selectedLocation")
            let notificationLocationChanged = Notification.Name("locationChanged")
            NotificationCenter.default.post(name: notificationLocationChanged, object: nil)
            
            menuTable.reloadData()
        }
        HamburgerMenu().closeSideMenu()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
