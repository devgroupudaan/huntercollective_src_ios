//
//  MessageViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 10/06/21.
//

import UIKit
import SwiftyJSON


class messageCell: UITableViewCell {
    @IBOutlet weak var sender_img: DesignableImageView!
    @IBOutlet weak var sender_name: UILabel!
    @IBOutlet weak var messageText: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    
}

class MessageViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var messageTable: UITableView!
    
    var searchActive : Bool = false
    var filteredData = [MessageListObject]()
    
    var clientMessages = [MessageListObject]()
    var broadcastMessages = [MessageListObject]()
    var communityMessages = [MessageListObject]()
    var selectedClient: MessageListObject!
    var currentMessageType = "client"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        messageTable.delegate = self
        messageTable.dataSource = self
        
        searchBar.delegate = self
        searchBar.backgroundColor = .clear
        searchBar.tintColor = .clear
        searchBar.backgroundImage = UIImage()
        // Do any additional setup after loading the view.
        serverCallforClientMessages(query: "")
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func typeChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 1:
            currentMessageType = "broadcast"
            serverCallforBroadcastMessages(query: "")
        case 2:
            currentMessageType = "community"
            serverCallforCommunityMessages(query: "")
        default:
            currentMessageType = "client"
            serverCallforClientMessages(query: "")
        }
        messageTable.reloadData()
       
    }
    
    
    // MARK: - tableview Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        switch currentMessageType {
//        case "client":
            return clientMessages.count
//        case "broadcast":
//            return 1
//        case "community":
//            return 2
//        default:
//            return 3
//        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "messageCell") as! messageCell
        switch currentMessageType {
        case "client":
            let message = clientMessages[indexPath.row]
            cell.sender_name.text = message.name
            cell.messageText.text = message.lastMessage.message
            cell.sender_img.kf.setImage(with: URL(string: message.image), placeholder:#imageLiteral(resourceName: "person-24px 1"), options: nil, progressBlock: nil, completionHandler: nil)
            cell.dateLbl.text = Utilities.shared.dateOrTimeTodayStr(strDate: message.lastMessage.created_at)
        case "broadcast":
            let message = broadcastMessages[indexPath.row]
            cell.sender_name.text = message.name
            cell.messageText.text = message.lastMessage.message
            cell.sender_img.kf.setImage(with: URL(string: message.image), placeholder:#imageLiteral(resourceName: "person-24px 1"), options: nil, progressBlock: nil, completionHandler: nil)
            cell.dateLbl.text = Utilities.shared.dateOrTimeTodayStr(strDate: message.lastMessage.created_at)
        case "community":
            let message = communityMessages[indexPath.row]
            cell.sender_name.text = message.name
            cell.messageText.text = message.lastMessage.message
            cell.sender_img.kf.setImage(with: URL(string: message.image), placeholder:#imageLiteral(resourceName: "person-24px 1"), options: nil, progressBlock: nil, completionHandler: nil)
            cell.dateLbl.text = Utilities.shared.dateOrTimeTodayStr(strDate: message.lastMessage.created_at)
        default:
            let message = clientMessages[indexPath.row]
            cell.sender_name.text = message.name
    //        cell.messageText.text = message.message
            cell.sender_img.kf.setImage(with: URL(string: message.image), placeholder:#imageLiteral(resourceName: "person-24px 1"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        
        
        return cell
    }
    //MARK: --- Search code ---
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        messageTable.reloadData()
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        switch currentMessageType {
        case "client":
//            filteredData = clientMessages.filter{($0.name.lowercased().contains(searchText.lowercased()))}
            serverCallforClientMessages(query: searchText)
        case "broadcast":
            serverCallforBroadcastMessages(query: searchText)
//            filteredData = broadcastMessages.filter{($0.name.lowercased().contains(searchText.lowercased()))}
        case "community":
            serverCallforCommunityMessages(query: searchText)
//            filteredData = communityMessages.filter{($0.name.lowercased().contains(searchText.lowercased()))}
        default:
//            filteredData = clientMessages.filter{($0.name.lowercased().contains(searchText.lowercased()))}
            serverCallforClientMessages(query: searchText)
        }
        

        if(searchText.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.messageTable.reloadData()
    }
    //MARK:- Server Calls
    func serverCallforClientMessages(query: String){
        showProgressOnView(self.view)
        let urlString = String(format: "\(WEB_URL.clients_chat_lists)?query=%@", query)
        NetworkManager.sharedInstance.executeGetServiceWithToken(urlString,  completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    self.clientMessages.removeAll()
                    let allClientMsgs = response!["data"].arrayValue
                    for item in allClientMsgs {
                        let lastMsgDict = item["lastMessage"].arrayValue[0].dictionaryValue
                        let lastMsg: LastMessage = LastMessage(id: lastMsgDict["id"]!.intValue, message: lastMsgDict["message"]!.stringValue, send_by: lastMsgDict["send_by"]!.intValue, send_to: lastMsgDict["send_to"]!.intValue, read_message: lastMsgDict["read_message"]!.boolValue, created_at: lastMsgDict["created_at"]!.stringValue, updated_at: lastMsgDict["updated_at"]!.stringValue)
                        let msg:MessageListObject = MessageListObject(id: item["id"].intValue, email: item["email"].stringValue, name: item["name"].stringValue, image: item["image"].stringValue, lastMessage: lastMsg)
                        self.clientMessages.append(msg)
                    }
                    DispatchQueue.main.async{
                        self.messageTable.reloadData()
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
    func serverCallforBroadcastMessages(query: String){
        showProgressOnView(self.view)
        let urlString = String(format: "\(WEB_URL.broadcast_all_message)?query=%@", query)
        NetworkManager.sharedInstance.executeGetServiceWithToken(urlString,  completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    self.broadcastMessages.removeAll()
                    let allClientMsgs = response!["data"].arrayValue
                    for item in allClientMsgs {
                        let lastMsgDict = item["lastMessage"].arrayValue[0].dictionaryValue
                        let lastMsg: LastMessage = LastMessage(id: lastMsgDict["id"]!.intValue, message: lastMsgDict["message"]!.stringValue, send_by: lastMsgDict["send_by"]!.intValue, send_to: lastMsgDict["send_to"]!.intValue, read_message: lastMsgDict["read_message"]!.boolValue, created_at: lastMsgDict["created_at"]!.stringValue, updated_at: lastMsgDict["updated_at"]!.stringValue)
                        let msg:MessageListObject = MessageListObject(id: item["id"].intValue, email: item["email"].stringValue, name: item["name"].stringValue, image: item["image"].stringValue, lastMessage: lastMsg)
                        self.broadcastMessages.append(msg)
                    }
                    DispatchQueue.main.async{
                        self.messageTable.reloadData()
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
    func serverCallforCommunityMessages(query: String){
        showProgressOnView(self.view)
        let urlString = String(format: "\(WEB_URL.community_messages)?query=%@", query)
        NetworkManager.sharedInstance.executeGetServiceWithToken(urlString,  completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    self.communityMessages.removeAll()
                    let allClientMsgs = response!["data"].arrayValue
                    for item in allClientMsgs {
                        let lastMsgDict = item["lastMessage"].arrayValue[0].dictionaryValue
                        let lastMsg: LastMessage = LastMessage(id: lastMsgDict["id"]!.intValue, message: lastMsgDict["message"]!.stringValue, send_by: lastMsgDict["send_by"]!.intValue, send_to: lastMsgDict["send_to"]!.intValue, read_message: lastMsgDict["read_message"]!.boolValue, created_at: lastMsgDict["created_at"]!.stringValue, updated_at: lastMsgDict["updated_at"]!.stringValue)
                        let msg:MessageListObject = MessageListObject(id: item["id"].intValue, email: item["email"].stringValue, name: item["name"].stringValue, image: item["image"].stringValue, lastMessage: lastMsg)
                        self.communityMessages.append(msg)
                    }
                    DispatchQueue.main.async{
                        self.messageTable.reloadData()
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
