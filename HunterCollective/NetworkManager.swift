//
//  NetworkManager.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 10/06/21.
//

import Foundation
import Alamofire
import SwiftyJSON

class NetworkManager: NSObject {
    static let sharedInstance = NetworkManager()
    fileprivate override init() {} //This prevents others from using the default '()' initializer for this class.
    func executeService(_ url:String, postParameters:[String: Any],completionHandler:@escaping (_ success:Bool, _ response:JSON?)->()) {
        print("URL " + (url as String))
        print("params - \(postParameters as NSDictionary?)")
        let urlString:String = url as String
       
        var headers: HTTPHeaders
        headers = ["Content-Type":"application/json"]
       
        if postParameters == nil {
            
            AF.request(urlString, method: .post , headers:headers).responseJSON { (response) in
                    switch response.result {
                                  case .success(let value):
                                    if let value = response.value {
                                        let json = JSON(value)
                                        debugPrint(json)
                                        completionHandler(true,json)
                                    }
//                                      if let json = value as? [String: Any] {
//                                        print("response - \(json as NSDictionary?)")
//                                        completionHandler(true,json as NSDictionary?)
//                                      }
                                  case .failure(let error): break
                                      // error handling
                                    print(error)
//                                    AppSharedData.sharedInstance.showErrorAlert(message: error.localizedDescription)
                                    completionHandler(false,nil)
                                  }
            }
        } else {
            
            AF.request(urlString, method: .post ,parameters: postParameters, encoding: JSONEncoding.default).responseJSON { (response) in
                    switch response.result {
                                  case .success:
                                    if let value = response.value {
                                        let json = JSON(value)
                                        debugPrint(json)
                                        completionHandler(true,json)
                                    }
//                                      if let json = value as? [String: Any] {
//                                        print("response - \(json as NSDictionary?)")
//                                        completionHandler(true,json as NSDictionary?)
//                                      }
                                  case .failure(let error): 
                                      // error handling
                                    print(error)
//                                    AppSharedData.sharedInstance.showErrorAlert(message: error.localizedDescription)
                                    completionHandler(false,nil)
                                  }
                }
        }
    }
    func executeServiceWithToken(_ url:String, postParameters:[String: Any],completionHandler:@escaping (_ success:Bool, _ response:JSON?)->()) {
        
        debugPrint(url)
        debugPrint(postParameters)
        let tokenStr = UserDefaults.standard.value(forKey: "token")
//        let headers: HTTPHeaders = [
//            "Content-Type": "application/json",
//            "accept": "application/json",
//           "Authorization" : String(format: "Bearer %@", tokenStr as! String)
//        ]
        let headers: HTTPHeaders = [
           "authorization" : String(format: "Bearer %@", tokenStr as! String),
            "Accept":"application/json"
        ]
        print("Header ", headers)
        
        if postParameters == nil {
            AF.request(url, method: .post , headers:headers).responseJSON { (response) in
                    switch response.result {
                                  case .success:
                                    if let value = response.value {
                                        let json = JSON(value)
                                        debugPrint(json)
                                        completionHandler(true,json)
                                    }
                                  case .failure(let error):
                                      // error handling
                                    print(error)
//                                    AppSharedData.sharedInstance.showErrorAlert(message: error.localizedDescription)
                                    completionHandler(false,nil)
                                  }
            }
        } else {
        AF.request(url, method: .post ,parameters: postParameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                              case .success:
                                if let value = response.value {
                                    let json = JSON(value)
                                    debugPrint(json)
                                    completionHandler(true,json)
                                }
                              case .failure(let error):
                                  // error handling
                                print(error)
                                completionHandler(false,nil)
                              }
            }
        }
    }
    
    func executeServiceWithMultipart(_ url:String,fileType:String,arrDataToSend:NSArray, postParameters:NSDictionary?,completionHandler:@escaping (_ success:Bool, _ response:NSDictionary?)->()) {
        print("URL " + (url as String))
        print("params - \(postParameters as NSDictionary?)")
        let urlString:String = url as String
        
        
        AF.upload(multipartFormData: { (multipartFormData) in
            for (index,dataToSend) in arrDataToSend.enumerated() {
           
                let data:Data = dataToSend as! Data
                
                if fileType == "Image"{
                    print(index)
                    multipartFormData.append(data, withName: "filename", fileName: "imagefile\(index).png", mimeType: "image/png")
                
                }else if fileType == "Video"{
                    
                    multipartFormData.append(data, withName: "filename", fileName: "videofile\(index).mp4", mimeType: "video/mp4")
               
                }else if fileType == "Sound"{
                    
                    multipartFormData.append(data, withName: "filename", fileName: "audiofile\(index).mp3", mimeType: "audio/mpeg")
//                    multipartFormData.append(data, withName: "file_pack\(index)", fileName: "audiofile\(index).mp3", mimeType: "audio/mpeg")

                }else if fileType == "Gif"{
                    
                    multipartFormData.append(data, withName: "filename", fileName: "giffile\(index).gif", mimeType: "image/gif")
               
                }
              
                

            }
                }, to: urlString)
            .uploadProgress{progress in
                debugPrint("uploading \(progress)")
        }
        .response{ resp in
            switch resp.result {
            case .success(_):
                if let value = resp.value {
                    do{
                        let json = try JSONSerialization.jsonObject(with: value!, options: []) as? [String : Any]
                        print("JSON:",json!)
                        completionHandler(true,json as Any? as? NSDictionary)
                    }catch{
                        print("erroMsg")
                        
                    }
                }
            case .failure(let error):
                print("multipart upload encodingError: \(error)")
//                AppSharedData.sharedInstance.showErrorAlert(message: error.localizedDescription)
                completionHandler(false,nil)
            }
        }
    }
    
    func executeServiceWithImage(_ url:String,postParameters:NSDictionary?,completionHandler:@escaping (_ success:Bool, _ response:NSDictionary?)->()) {
        print("URL " + (url as String))
        print("params - \(postParameters as NSDictionary?)")
        let urlString:String = url as String
        
        
        
        AF.upload(multipartFormData: { (multipart) in
            for (key, value) in postParameters! {
                //  multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
            }
                }, to: urlString)
            .uploadProgress{progress in
                debugPrint("uploading \(progress)")
        }
        .response{ resp in
            switch resp.result {
            case .success(_):
                if let value = resp.value {
                    do{
                        let json = try JSONSerialization.jsonObject(with: value!, options: []) as? [String : Any]
                        print("JSON:",json!)
                        completionHandler(true,json as Any? as? NSDictionary)
                    }catch{
                        print("erroMsg")
                        
                    }
                }
            case .failure(let error):
                print("multipart upload encodingError: \(error)")
                completionHandler(false,nil)
            }
        }
    }

    func defaultGalleryServerRequest(url:String,videoData:Data, completionHandler:@escaping (_ success:Bool, _ response:NSDictionary?)->()) {
        
        let timeStamp = NSDate().timeIntervalSince1970
        
        AF.upload(multipartFormData: { (multipart) in
            multipart.append(videoData, withName: "filename", fileName: "\(timeStamp).mp4", mimeType: "video/mp4")
                }, to: "\(url)", method: .post, headers: nil)
            .uploadProgress{progress in
                debugPrint("uploading \(progress)")
        }
        .response{ resp in
            switch resp.result {
            case .success(_):
                if let value = resp.value {
                    do{
                        let json = try JSONSerialization.jsonObject(with: value!, options: []) as? [String : Any]
                        print("JSON:",json!)
                        completionHandler(true,json as Any? as? NSDictionary)
                    }catch{
                        print("erroMsg")
                        
                    }
                }
            case .failure(let error):
                print("multipart upload encodingError: \(error)")
                completionHandler(false,nil)
            }
        }
    }
    
    func executeGetService(_ url:String, parameters:String?,completionHandler:@escaping (_ success:Bool, _ response:NSDictionary?)->()) {
        
        print("URL " + (url as String))
        print("params - \(parameters as String?)")
        let urlString:String = (url as String) + "?" + parameters!
        
        var headers: HTTPHeaders
        headers = ["Content-Type":"application/json"]
        
        AF.request(urlString, method: .get, headers:headers).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                if let json = value as? [String: Any] {
                    print("response - \(json as NSDictionary?)")
                    completionHandler(true,json as NSDictionary?)
                }
            case .failure(let error): break
                // error handling
                print(error)
//                AppSharedData.sharedInstance.showErrorAlert(message: error.localizedDescription)
                completionHandler(false,nil)
            }
        }
    }
    func executeGetServiceWithToken(_ url:String,completionHandler:@escaping (_ success:Bool, _ response:JSON?)->()) {
        
        print("URL " + (url as String))
//        print("params - \(parameters as String?)")
        let urlString:String = (url as String)
        
        let tokenStr = UserDefaults.standard.value(forKey: "token")
        let headers: HTTPHeaders = [
           "authorization" : String(format: "Bearer %@", tokenStr as! String),
            "Accept":"application/json"
        ]
        print("Header ", headers)
        
        AF.request(urlString, method: .get, headers:headers).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                if let json = value as? [String: Any] {
                    print("response - \(json as NSDictionary?)")
//                    completionHandler(true,json as NSDictionary?)
                    let resp = JSON(json)
                    let msg = resp["message"].stringValue
                    if msg == "Unauthenticated." {
                        
                        let alert = UIAlertController(title: "Alert!", message: "Your session is expired. Please login again", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: {_ in
                            UIApplication.shared.windows.last?.rootViewController?.present(alert, animated: true, completion: nil)

                            let defaults = UserDefaults.standard
                                let dictionary = defaults.dictionaryRepresentation()
                                dictionary.keys.forEach { key in
                                    defaults.removeObject(forKey: key)
                                }
                            let storyboard = UIStoryboard(name:"Main", bundle: nil)
                            let vc  = storyboard.instantiateViewController(withIdentifier: "loginVC") as! SignInViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = vc
                        }))
                        UIApplication.shared.windows.last?.rootViewController?.present(alert, animated: true, completion: nil)

//                        let defaults = UserDefaults.standard
//                            let dictionary = defaults.dictionaryRepresentation()
//                            dictionary.keys.forEach { key in
//                                defaults.removeObject(forKey: key)
//                            }
//                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
//                        let vc  = storyboard.instantiateViewController(withIdentifier: "loginVC") as! SignInViewController
//                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                        appDelegate.window?.rootViewController = vc
                    }
                    completionHandler(true,JSON(json))
                }
            case .failure(let error): break
                // error handling
                print(error)
//                AppSharedData.sharedInstance.showErrorAlert(message: error.localizedDescription)
                completionHandler(false,nil)
            }
        }
    }
    
    func executeServiceFor(url:String, methodType:HTTPMethod, postParameters:[String:Any]?,completionHandler:@escaping (_ success:Bool, _ response:NSDictionary?)->()) {
        
        print("URL " + (url as String))
        print("params - \(postParameters as NSDictionary?)")
        
        let urlString:String = url as String
        
        var headers: HTTPHeaders
//        headers = ["Content-Type":"application/json"]
        headers    = [
                    "Content-Type": "application/x-www-form-urlencoded",
                    "Authorization" : "Token"
                ]
       
        AF.request(urlString, method: methodType, parameters: postParameters, encoding: JSONEncoding.default)
            .responseJSON { (response) in
                switch response.result {
                              case .success(let value):
                                  if let json = value as? [String: Any] {
                                    print("response - \(json as NSDictionary?)")
                                    completionHandler(true,json as NSDictionary?)
                                  }
                              case .failure(let error):
                                  // error handling
                                print(error)
//                                AppSharedData.sharedInstance.showErrorAlert(message: error.localizedDescription)
                                completionHandler(false,nil)
                                break
                              }
            }
    }
   
    
    func uploadImageWith(url:String,userID:String,imageData:Data,completionHandler:@escaping (_ success:Bool)->()){
        
        // generate boundary string using a unique per-app string
        let boundary = UUID().uuidString
        
        let fieldName = "userid"
        let fieldValue = "\(userID)"
        
        
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // Set the URLRequest to POST and to the specified URL
        var urlRequest = URLRequest(url: URL(string:"https://api.talkblox.info/updatephoto")!)
        urlRequest.httpMethod = "POST"
        
        // Set Content-Type Header to multipart/form-data, this is equivalent to submitting form data with file upload in a web browser
        // And the boundary is also set here
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        var data = Data()
        
        // Add the reqtype field and its value to the raw http request data
        
        
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"\(fieldName)\"\r\n\r\n".data(using: .utf8)!)
        data.append("\(fieldValue)".data(using: .utf8)!)
        
        
        
        let imgData = imageData
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"filename\"; filename=\"ddd\"\r\n".data(using: .utf8)!)
        data.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
        data.append(imgData)
        
        
        // End the raw http request data, note that there is 2 extra dash ("-") at the end, this is to indicate the end of the data
        // According to the HTTP 1.1 specification https://tools.ietf.org/html/rfc7230
        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
        
        // Send a POST request to the URL, with the data we created earlier
        session.uploadTask(with: urlRequest, from: data, completionHandler: { responseData, response, error in
            
            if(error != nil){
                print("\(error!.localizedDescription)")
                completionHandler(false)
            }
            
            guard let responseData = responseData else {
                print("no response data")
                completionHandler(false)
                return
            }
            
            if let responseString = String(data: responseData, encoding: .utf8) {
                print("uploaded to: \(responseString)")
                completionHandler(true)
            }
        }).resume()
    }
    
    
    
    
//    func executeServiceWithToken(_ url:String, postParameters:NSDictionary?,completionHandler:@escaping (_ success:Bool, _ response:NSDictionary?)->())
    func putAPIWithToken(_ url:String, postParameters:[String:Any],completionHandler:@escaping (_ success:Bool, _ response:JSON?)->()){
        
        debugPrint(url)
        debugPrint(postParameters)
        let tokenStr = UserDefaults.standard.value(forKey: "token")
        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
           "Authorization" : String(format: "Bearer %@", tokenStr as! String)
        ]
        
        AF.request(url, method: .put, parameters: postParameters, encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.value {
                    let json = JSON(value)
                    debugPrint(json)
                    completionHandler(true,json)
                }
            case .failure(let error):
                print(error)
                completionHandler(false,nil)

            }}
    }
    
    func patchAPI( parameter:[String:Any], url:String, _ successBlock:@escaping ( _ response: JSON )->Void , errorBlock: @escaping (_ error: NSError) -> Void ){
        
        debugPrint(parameter)
        
        AF.request(url, method: .patch, parameters: parameter, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.value {
                    let json = JSON(value)
                    successBlock(json)
                }
            case .failure(let error):
                errorBlock(error as NSError)
                
            }}
    }
    
    func deleteAPI( parameter:[String:Any], url:String, _ successBlock:@escaping ( _ response: JSON )->Void , errorBlock: @escaping (_ error: NSError) -> Void ){
        
        debugPrint(parameter)
        
        AF.request(url, method: .delete, parameters: parameter, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.value {
                    let json = JSON(value)
                    successBlock(json)
                }
            case .failure(let error):
                errorBlock(error as NSError)
            }}
    }
    
    func deleteAPIwithToken( parameter:[String:Any], url:String, _ successBlock:@escaping ( _ response: JSON )->Void , errorBlock: @escaping (_ error: NSError) -> Void ){
        
        debugPrint(parameter)
        let tokenStr = UserDefaults.standard.value(forKey: "token")
        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
           "Authorization" : String(format: "Bearer %@", tokenStr as! String)
        ]
        
        AF.request(url, method: .delete, parameters: parameter, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.value {
                    let json = JSON(value)
                    successBlock(json)
                }
            case .failure(let error):
                errorBlock(error as NSError)
            }}
    }
    
//    func postImageAPI(_ url:String, postParameters:[String:Any],completionHandler:@escaping (_ success:Bool, _ response:JSON?)->()){
    func postImageAPI( image:UIImage, imageName:String, parameter:[String:Any], url:String,completionHandler:@escaping (_ success:Bool, _ response:JSON?)->()){
        
        debugPrint(url)
        debugPrint(parameter)
        let tokenStr = UserDefaults.standard.value(forKey: "token")
        let headers: HTTPHeaders = [
            "Authorization" : String(format: "Bearer %@", tokenStr as! String),
            "Accept":"application/json"
        ]
        print("Header ", headers)
        
        let imageData = image.jpegData(compressionQuality: 0.80)
        debugPrint(image, imageData!)
        
        AF.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(imageData!, withName: imageName, fileName: "swift_file.png", mimeType: "image/png")
            for (key, value) in parameter {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: url, method: .post, headers: headers)
        .uploadProgress{progress in
            debugPrint("uploading \(progress)")
        }
        .response{ resp in
            debugPrint(resp)
            switch resp.result {
            case .success(_):
                if let Value = resp.value {
                    let json = JSON(Value)
                    completionHandler(true,json)
                }
            case .failure(let error):
                completionHandler(true,nil)
            }
        }
    }
}
