//
//  PaymentMethodsViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 13/08/21.
//

import UIKit
class cardCell: UITableViewCell {
    
}

class PaymentMethodsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var cardTableView: UITableView!
    @IBOutlet weak var addCardView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        cardTableView.delegate = self
        cardTableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func openAddNewCardView(_ sender: UIButton) {
        addCardView.isHidden = false
    }
    @IBAction func deleteCard(_ sender: UIButton) {
    }
    @IBAction func closeAddCardView(_ sender: UIButton) {
        addCardView.isHidden = true
    }
    @IBAction func addNewCard(_ sender: UIButton) {
        addCardView.isHidden = true
    }
    
    
    // MARK: - tableview Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cardCell") as! cardCell
           
        return cell
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
