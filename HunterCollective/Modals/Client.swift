//
//  Client.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 24/06/21.
//

struct Client {
    var image: String
    var name: String
    var email: String
    var mobile: String
}
