//
//  Message.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 24/06/21.
//

import Foundation

struct Message {
    var id: Int
//    var image: String
    var sender: String
    var message: String
    var created_at: String
}
