//
//  countryName.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 30/06/21.
//

struct countryName {
    var id: Int
    var iso: String
    var name: String
    var nicename: String
    var iso3: String
    var numcode: Int
    var phonecode: Int
}
