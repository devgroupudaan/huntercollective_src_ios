//
//  LastMessage.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 03/07/21.
//

import Foundation

struct LastMessage {
    var id: Int
    var message: String
    var send_by: Int
    var send_to: Int
    var read_message: Bool
    var created_at: String
    var updated_at: String
}


