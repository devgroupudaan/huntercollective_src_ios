//
//  Note.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 06/09/21.
//

struct Note {
    var id: Int
    var user_id: Int
    var client_id: Int
    var appointment_id: Int
    var description: String
    var image: String
    var created_at: String
    var updated_at: String
    var users: User
}
