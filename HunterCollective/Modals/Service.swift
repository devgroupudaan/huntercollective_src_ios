//
//  Service.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 24/06/21.
//

struct Service {
    var id: Int
    var title: String
    var description: String
    var price: Double
    var require_add_on: Bool
    var time: String
}
