//
//  Service.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 24/06/21.
//

struct MyService {
    var id: Int
    var image: String
    var title: String
    var description: String
    var price: Double
    var require_add_on: Bool
    var time: String
    var category: Service_category
}
