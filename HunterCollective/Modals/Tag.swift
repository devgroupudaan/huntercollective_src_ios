//
//  Tag.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 24/06/21.
//

struct Tag: Encodable, Decodable{
    var id: Int
    var name: String
}
