//
//  UserDetail.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 24/06/21.
//

import Foundation

struct UserDetail: Encodable, Decodable{
    var name : String
    var email : String
    var image : String
    var mobile : String
    var date_o_birth : String
    var address : String
    var introduction : String
    var accept_client : Bool
    var tags : [Tag]
}
