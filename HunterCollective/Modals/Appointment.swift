//
//  Appointment.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 03/09/21.
//

struct Appointment {
    var id: Int
    var user_id: Int
    var chair_count: Int
    var location_id: Int
    var status: Int
    var boooking_date: String
    var boooking_time: String
    var users: User
    var start_time: String
    var end_time: String
    var frequency: String
    var booked_service: String
    var chair_name: String
    var total_cost: Double
    var created_at: String
    var updated_at: String
    var location: MyLocation!
//    var booked_services: []
}

