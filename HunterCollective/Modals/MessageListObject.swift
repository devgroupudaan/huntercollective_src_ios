//
//  MessageListObject.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 03/07/21.
//

import Foundation

struct MessageListObject {
    var id: Int
    var email: String
    var name: String
    var image: String
    var lastMessage: LastMessage
}
