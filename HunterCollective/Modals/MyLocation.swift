//
//  MyLocation.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 30/06/21.
//

struct MyLocation {
    var id: Int
    var name: String
    var address1: String
    var city: String
    var status: String
    var country: String
}
