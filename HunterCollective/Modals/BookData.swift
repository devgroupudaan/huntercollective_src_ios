//
//  BookData.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 18/08/21.
//

struct BookData {
    var id: Int
    var user_id: Int
    var chair_id: Int
    var location_id: Int
    var booking_date: String
    var booking_time: String
    var start_time: String
    var end_time: String
    var frequency: String
    var status: Bool
    var chair_name: String
    var total_cost: Double
    var created_at: String
    var updated_at: String
    var booked: [BookingEntity]
    var location: MyLocation
}
