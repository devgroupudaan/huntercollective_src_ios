//
//  BookingEntity.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 18/08/21.
//

struct BookingEntity {
    var id: Int
    var user_id: Int
    var booking_id: Int
    var chair_id: Int
    var chair_cost: Double
    var created_at: String
    var updated_at: String
    var chair: Chair
}
