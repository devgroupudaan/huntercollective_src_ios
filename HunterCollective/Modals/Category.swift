//
//  Category.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 24/06/21.
//

struct Category {
    var id: Int
    var image: String
    var name: String
    var description: String
    var status: Bool
}
