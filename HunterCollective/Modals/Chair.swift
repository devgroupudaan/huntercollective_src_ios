//
//  Chair.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 17/08/21.
//

struct Chair {
    var id: Int
    var status: Int
    var name: String
    var image: String
    var cost: Double
    var location_id: Int
    var created_at: String
    var updated_at: String
    var deleted_at: String
}

