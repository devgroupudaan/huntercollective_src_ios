//
//  Location.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 24/06/21.
//

import UIKit

struct Location {
    var id: Int
    var name: String
    var address1: String
    var city: String
    var status: String
    var chairs_count: Int
    var countryName: countryName
    var image: String
}
