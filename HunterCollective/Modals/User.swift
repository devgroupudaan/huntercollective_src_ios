//
//  User.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 06/09/21.
//

struct User {
    var id: Int
    var name: String
    var email: String
//    var email_verified_at: String
    var status: Bool
    var image: String
    var role: Int
    var mobile: String
    var date_o_birth: String
    var address: String
    var introduction: String
    var accept_client: Bool
//    var broadcast_message: null
    var created_at: String
    var updated_at: String
//    var deleted_at: null
}
