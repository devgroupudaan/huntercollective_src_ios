//
//  EditAccountViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 04/06/21.
//

import UIKit
import Kingfisher
import Toast_Swift
import SwiftyJSON

class addTagCell: UICollectionViewCell{
    @IBOutlet weak var bgView: DesignableView!
    @IBOutlet weak var tagName: UILabel!
    @IBOutlet weak var removeBtn: UIButton!
    
}
class addNewTagCell: UICollectionViewCell{
    @IBOutlet weak var checkMark: UIImageView!
    @IBOutlet weak var tag_name: UILabel!
    
}

class EditAccountViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var profile_pic: DesignableImageView!
    @IBOutlet weak var dob: DesignableTextfield!
    @IBOutlet weak var address: DesignableTextfield!
    @IBOutlet weak var phone: DesignableTextfield!
    @IBOutlet weak var email: DesignableTextfield!
    @IBOutlet weak var tagCollectionView: UICollectionView!
    @IBOutlet weak var txtCountLbl: UILabel!
    @IBOutlet weak var introTextView: DesignableTextView!
    @IBOutlet weak var addTagPopup: UIView!
    @IBOutlet weak var popUpTagsCollection: UICollectionView!
    
    @IBOutlet weak var newClientYesBtn: DesignableButton!
    @IBOutlet weak var newClientNoBtn: DesignableButton!
    @IBOutlet weak var popUpSubView: DesignableView!
    @IBOutlet weak var tagCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var popUpCollectionViewHeight: NSLayoutConstraint!
    
    var AcceptNewClient = false
    var addedTags = [String]()
    var preAddedTags:[String] =  []
    var allTags = [Tag]()
    var selectedTags = [String]()
    var selectedTagIDs = [Int]()
    
    var currentUser: UserDetail!
    
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tagCollectionView.delegate = self
        tagCollectionView.dataSource = self
        popUpTagsCollection.delegate = self
        popUpTagsCollection.dataSource = self
        introTextView.delegate = self
        
        addedTags = ["Add Tag"]
//        allTags = ["Hair Stylist","Colour Expert","Curl Expert","Hair Extentionist","Trichologist","Colour Specialist","Colour Degree","Barber","Mens Grooming","Manicurist","Eye Lash Tech","Esthetician"]

        
        if let emailStr  = UserDefaults.standard.value(forKey: "email"){
            email.text = emailStr as? String
        }
//        tagCollectionView.
        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        var H = tagCollectionView.contentSize.height
        if H == 0.0 {
            H = 26
        }
        tagCollectionViewHeight.constant = H//tagCollectionView.contentSize.height
        tagCollectionView.layoutIfNeeded()
        
        if let current_userData = UserDefaults.standard.object(forKey: "currentUser") as? Data {
            if let current_user = try? JSONDecoder().decode(UserDetail.self, from: current_userData){
                self.currentUser = current_user
            }
        }
        else {
            self.currentUser = UserDetail(name: "", email: "", image: "", mobile: "", date_o_birth: "", address: "", introduction: "", accept_client: false, tags: [Tag(id: 0, name: "")])
        }
        
        self.address.text = currentUser.address
        self.dob.text = currentUser.date_o_birth
        self.phone.text = currentUser.mobile
        self.email.text = currentUser.email
        self.introTextView.text = currentUser.introduction
        if let image = currentUser.image as? String {
//            let imgURL = WEB_URL.MainUrl + image
            self.profile_pic.kf.setImage(with: URL(string: image), placeholder:#imageLiteral(resourceName: "person-24px 1"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        selectedTags.removeAll()
        selectedTagIDs.removeAll()
        for tag in currentUser.tags{
            let name = tag.name
            preAddedTags.append(name)
            selectedTags.append(name)
            selectedTagIDs.append(tag.id)
        }
        addedTags = preAddedTags + ["Add Tag"]
        self.tagCollectionView.reloadData()
        serverCalltoGetTags()
        
        if currentUser.accept_client{
            AcceptNewClient = true
            newClientYesBtn.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            newClientYesBtn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            newClientYesBtn.layer.borderWidth = 0.0
            
            newClientNoBtn.backgroundColor = .clear
            newClientNoBtn.titleLabel?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            newClientNoBtn.layer.cornerRadius = 15
            newClientNoBtn.layer.borderWidth = 1.0
            newClientNoBtn.layer.borderColor = UIColor.darkGray.cgColor
        } else {
            AcceptNewClient = false
            newClientYesBtn.backgroundColor = .clear
            newClientYesBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            newClientYesBtn.layer.cornerRadius = 15
            newClientYesBtn.layer.borderWidth = 1.0
            newClientYesBtn.layer.borderColor = UIColor.darkGray.cgColor
            
            newClientNoBtn.backgroundColor = #colorLiteral(red: 0.1353600621, green: 0.1353642344, blue: 0.1353619695, alpha: 1)
            newClientNoBtn.titleLabel?.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            newClientNoBtn.layer.borderWidth = 0.0
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tagCollectionViewHeight.constant = tagCollectionView.contentSize.height
        tagCollectionView.layoutIfNeeded()
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func newClientYes(_ sender: UIButton) {
        AcceptNewClient = true
        newClientYesBtn.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        newClientYesBtn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        newClientYesBtn.layer.borderWidth = 0.0
        
        newClientNoBtn.backgroundColor = .clear
        newClientNoBtn.titleLabel?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        newClientNoBtn.layer.cornerRadius = 15
        newClientNoBtn.layer.borderWidth = 1.0
        newClientNoBtn.layer.borderColor = UIColor.darkGray.cgColor
    }
    @IBAction func newClientNo(_ sender: UIButton) {
        AcceptNewClient = false
        newClientYesBtn.backgroundColor = .clear
        newClientYesBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        newClientYesBtn.layer.cornerRadius = 15
        newClientYesBtn.layer.borderWidth = 1.0
        newClientYesBtn.layer.borderColor = UIColor.darkGray.cgColor
        
        newClientNoBtn.backgroundColor = #colorLiteral(red: 0.1353600621, green: 0.1353642344, blue: 0.1353619695, alpha: 1)
        newClientNoBtn.titleLabel?.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        newClientNoBtn.layer.borderWidth = 0.0
    }
    @IBAction func saveTapped(_ sender: UIButton) {
        self.serverCallUpdateDetails()
        
    }
    @IBAction func addTag(_ sender: UIButton) {
//        addedTags = preAddedTags + selectedTags + ["Add Tag"]
        addedTags =  selectedTags + ["Add Tag"]
        addTagPopup.isHidden = true
        tagCollectionView.reloadData()
        let when = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: when){
            self.tagCollectionViewHeight.constant = self.tagCollectionView.contentSize.height
            self.tagCollectionView.layoutIfNeeded()
        }
    }
    @IBAction func cancelAddTag(_ sender: UIButton) {
        addTagPopup.isHidden = true
    }
    @IBAction func changeImage(_ sender: UIButton) {
        self.showActionSheet()
    }
    @IBAction func removeTag(_ sender: UIButton) {
        let currentTag = selectedTags[sender.tag]
        let currentTagID = selectedTagIDs[sender.tag]
        selectedTags = selectedTags.filter { $0 != currentTag }
        addedTags = addedTags.filter{ $0 != currentTag }
        selectedTagIDs = selectedTagIDs.filter{ $0 != currentTagID }
        tagCollectionView.reloadData()
        let when = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: when){
            self.tagCollectionViewHeight.constant = self.tagCollectionView.contentSize.height
            self.tagCollectionView.layoutIfNeeded()
        }
    }
    
    func showActionSheet() {
        let actionSheet = UIAlertController.init(title: "", message: "Please select one of the following", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction.init(title: "Camera", style: .default, handler: { (action) in
            self.showCamera()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Gallery", style: .default, handler: { (action) in
            self.showPhotoLibrary()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func showCamera() {
        self.imagePicker.sourceType = .camera
        self.imagePicker.allowsEditing = true
        self.imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    func showPhotoLibrary() {
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.allowsEditing = true
        self.imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK:- UIImagePickerControlloller Delegates
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.profile_pic.image = image
//            let imageData = image.pngData()
//            self.serverCallForUpdateProfilePhoto(imageData!)
            self.serverCallForProfilePicUpdate(image: image)
        }
        self.dismiss(animated: true, completion: nil)
    }
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//// Local variable inserted by Swift 4.2 migrator.
////let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
////
////        if let image = infoconvertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage) as? UIImage {
////            self.imguser.image = image
////            let imageData = image.pngData()
////            self.serverCallForUpdateProfilePhoto(imageData!)
////
////        }
//
//        self.dismiss(animated: true, completion: nil)
//    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - TextView Delegates
    func textViewDidChange(_ textView: UITextView) {
        let length = textView.text.count
        txtCountLbl.text = "\(length)" + "/100"
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text.count == 0 {
            if textView.text.count != 0 {
                return true
            }
        } else if textView.text.count > 99 {
            return false
        }
        return true
    }
    
    // MARK: - collectionView Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == tagCollectionView{
            return addedTags.count
        } else {
            return allTags.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == tagCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addTagCell", for: indexPath) as! addTagCell
            cell.tagName.text = addedTags[indexPath.row]
            cell.removeBtn.tag = indexPath.row
            if indexPath.row == addedTags.count - 1 {
                cell.bgView.backgroundColor = .clear
                cell.bgView.borderWidth = 1.0
                cell.bgView.borderColor = .black
                cell.tagName.textColor = .black
                cell.removeBtn.isHidden = true
                
            } else {
                cell.bgView.backgroundColor = .black
                cell.bgView.borderWidth = 0.0
                cell.tagName.textColor = .white
                cell.removeBtn.isHidden = false
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addNewTagCell", for: indexPath) as! addNewTagCell
            cell.tag_name.text = allTags[indexPath.row].name
            let currentTag = allTags[indexPath.row]
            if selectedTagIDs.contains(currentTag.id) {
                cell.checkMark.image = #imageLiteral(resourceName: "Group 287")
            } else {
                cell.checkMark.image = #imageLiteral(resourceName: "Group 276")
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let screenSize = UIScreen.main.bounds
//        let screenWidth = screenSize.width
        if collectionView == tagCollectionView {
            let item = addedTags[indexPath.row]
                    let itemSize = item.size(withAttributes: [
                        NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14)
                    ])
//                    return itemSize
            if indexPath.row == addedTags.count - 1 {
                return CGSize(width: itemSize.width + 20, height: 26);
            } else {
                return CGSize(width: itemSize.width + 45, height: 26);
            }
        } else {
            let W = popUpSubView.frame.width
            return CGSize(width: (W - 50)/2 , height: 34);
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 10;
        }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == tagCollectionView{
            if indexPath.row == addedTags.count - 1 {
//                var a = 14.123456789
//                a.rounded(.awayFromZero)
                var roundVal:Float = Float(allTags.count/2)
                roundVal = roundVal.rounded(.awayFromZero)
                let H = screenHeight * 0.6
                let estimatedH = CGFloat(roundVal * 39)
                if estimatedH > H {
                    popUpCollectionViewHeight.constant = H
                } else {
                    popUpCollectionViewHeight.constant = estimatedH
                }
//                popUpCollectionViewHeight.constant = CGFloat(roundVal * 39)
                addTagPopup.isHidden = false
            }
        } else {
            let currentTag = allTags[indexPath.row]
            
            if selectedTagIDs.contains(currentTag.id) {
                selectedTags = selectedTags.filter { $0 != currentTag.name }
                selectedTagIDs = selectedTagIDs.filter{ $0 != currentTag.id }
                let cell = popUpTagsCollection.cellForItem(at: indexPath) as! addNewTagCell
                cell.checkMark.image = #imageLiteral(resourceName: "Group 276")
            } else {
                selectedTags.append(currentTag.name)
                selectedTagIDs.append(currentTag.id)
                let cell = popUpTagsCollection.cellForItem(at: indexPath) as! addNewTagCell
                cell.checkMark.image = #imageLiteral(resourceName: "Group 287")
            }
        }
    }
    
    
    // MARK:- API Calls
    func serverCalltoGetTags(){
        showProgressOnView(self.view)
        
        NetworkManager.sharedInstance.executeGetServiceWithToken(WEB_URL.tags,  completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    self.allTags.removeAll()
                    let tags = response!["data"].arrayValue
                    for tag in tags{
                        let tg = Tag(id: tag["id"].intValue, name: tag["name"].stringValue)
                        self.allTags.append(tg)
                    }
                    
                    DispatchQueue.main.async{
                        self.popUpTagsCollection.reloadData()
                    }
                    
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
    func serverCallUpdateDetails(){
        
        var addedTgs: [Int] = []
        for tag in currentUser.tags{
            addedTgs.append(tag.id)
        }
        showProgressOnView(self.view)
        let params:[String: Any] = ["date_o_birth": self.dob.text!,
                                    "name":self.currentUser.name,
                                   "mobile":self.phone.text!,
                                   "address":self.address.text!,
                                   "introduction":self.introTextView.text!,
                                   "tags":selectedTagIDs,
                                   "accept_client":AcceptNewClient]
//        NetworkManager.sharedInstance.putAPIWithToken(WEB_URL.account_update, postParameters: params, completionHandler: { (success:Bool, response:JSON?) in
        
        NetworkManager.sharedInstance.executeServiceWithToken(WEB_URL.account_update, postParameters: params, completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    DispatchQueue.main.async{
                        self.dob.resignFirstResponder()
                        self.address.resignFirstResponder()
                        self.introTextView.resignFirstResponder()
                        self.email.resignFirstResponder()
                        self.phone.resignFirstResponder()
                        self.view.makeToast(response!["message"].stringValue, duration: 2.0, position: .bottom)
                    }
                } else {
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            } else {
                self.view.makeToast("Some error occured.", duration: 2.0, position: .bottom)
            }
        })
    }

    func serverCallForProfilePicUpdate(image:UIImage){
        showProgressOnView(self.view)
        NetworkManager.sharedInstance.postImageAPI(image: image, imageName: "profile_img", parameter: [:], url: WEB_URL.update_profile, completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    DispatchQueue.main.async {
                        self.view.makeToast(response!["message"].stringValue, duration: 2.0, position: .bottom)
                    }
                } else {
//                    let error =
                    self.view.makeToast("error", duration: 2.0, position: .bottom)
                }
            }
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


class TagsLayout: UICollectionViewFlowLayout {
    
    required override init() {super.init(); common()}
    required init?(coder aDecoder: NSCoder) {super.init(coder: aDecoder); common()}
    
    private func common() {
        estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        minimumLineSpacing = 10
        minimumInteritemSpacing = 10
    }
    
    override func layoutAttributesForElements(
                    in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        guard let att = super.layoutAttributesForElements(in:rect) else {return []}
        var x: CGFloat = sectionInset.left
        var y: CGFloat = -1.0
        
        for a in att {
            if a.representedElementCategory != .cell { continue }
            
            if a.frame.origin.y >= y { x = sectionInset.left }
            a.frame.origin.x = x
            x += a.frame.width + minimumInteritemSpacing
            y = a.frame.maxY
        }
        return att
    }
}
