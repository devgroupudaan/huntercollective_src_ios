//
//  ViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 03/06/21.
//

import UIKit
import GoogleSignIn
import AuthenticationServices
import Toast_Swift
import SwiftyJSON

class SignInViewController: UIViewController,GIDSignInDelegate {
    @IBOutlet weak var emailTxtFld: DesignableTextfield!
    @IBOutlet weak var passwordTxtFld: DesignableTextfield!
    
    var window: UIWindow?
    var appleSignInDelegates: SignInWithAppleDelegates! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    @available(iOS 13.0, *)
    private func performSignIn(using requests: [ASAuthorizationRequest]) {
      appleSignInDelegates = SignInWithAppleDelegates(window: window) { success in
        if success {
          // update UI
        } else {
          // show the user an error
        }
      }

      let controller = ASAuthorizationController(authorizationRequests: requests)
      controller.delegate = appleSignInDelegates
      controller.presentationContextProvider = appleSignInDelegates

      controller.performRequests()
    }
    //MARK: - Button Actions
    @IBAction func toggleEye(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            passwordTxtFld.isSecureTextEntry = true
        } else {
            sender.isSelected = true
            passwordTxtFld.isSecureTextEntry = false
        }
    }
    @IBAction func signInTapped(_ sender: UIButton) {
        if !emailTxtFld.text!.isValidEmail {
            self.view.makeToast("Email is not valid.", duration: 3.0, position: .bottom)
        } else if passwordTxtFld.text?.isEmpty == true {
            self.view.makeToast("Please enter a valid password.", duration: 3.0, position: .bottom)
        } else{
//            self.performSegue(withIdentifier: "toHome", sender: nil)
            serverCallForLogin()
        }
    }
    @IBAction func appleLoginTapped(_ sender: UIButton) {
//        let appleIDProvider = ASAuthorizationAppleIDProvider()
//        let request = appleIDProvider.createRequest()
//        request.requestedScopes = [.fullName, .email]
//
//        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
//        authorizationController.delegate = self
//        authorizationController.presentationContextProvider = self
//        authorizationController.performRequests()
        
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]

            performSignIn(using: [request])
        } else {
            // Fallback on earlier versions
        }
       
    }
    @IBAction func googleSigninTapped(_ sender: UIButton) {
        GIDSignIn.sharedInstance()?.presentingViewController = self
         GIDSignIn.sharedInstance().signIn()
        GIDSignIn.sharedInstance().delegate = self
    }
    
    //MARK: - Google Signin Delegates
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
      if let error = error {
        if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
          debugPrint("The user has not signed in before or they have since signed out.")
        } else {
          debugPrint("\(error.localizedDescription)")
        }
        return
      }
      // Perform any operations on signed in user here.
        _ = user.userID                  // For client-side use only!
        let code = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile.name
        let first_name  = user?.profile.givenName
        let last_name = (user?.profile.familyName)!
        let email = user.profile.email
        
        self.serverCallForSocialLogin(code: code!, email: email!)
//        self.apiforSocialLogin(name: first_name!, last_name: last_name, email: email!, type: "g")
      // ...
//        UserDefaults.standard.set(true, forKey: "isLoggedIn")
//        UserDefaults.standard.set(fullName, forKey: "fullName")
//        UserDefaults.standard.set(email, forKey: "email")
        
//        self.view.makeToast(String(format: "Welcome %@", fullName!), duration: 2.0, position: .bottom)
////        let when = DispatchTime.now() + 2
////        DispatchQueue.main.asyncAfter(deadline: when){
//        let notificationLoggedIn = Notification.Name("loggedIn")
//        NotificationCenter.default.post(name: notificationLoggedIn, object: nil)
//            self.performSegue(withIdentifier: "toHome", sender: nil)
//        }
        
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
      // Perform any operations when the user disconnects from app here.
      // ...
    }
    
    
    //MARK:- Server Calls
    func serverCallForLogin(){
        showProgressOnView(self.view)
        let params:[String: Any] = ["email":self.emailTxtFld.text ?? "",
                                   "password":self.passwordTxtFld.text ?? ""]
        
        
        NetworkManager.sharedInstance.executeService(WEB_URL.login, postParameters: params, completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    let token = response!["token"].stringValue
                    UserDefaults.standard.set(token, forKey: "token")
                    UserDefaults.standard.set(true, forKey: "isLoggedIn")
                    UserDefaults.standard.set(self.emailTxtFld.text, forKey: "email")
                    self.performSegue(withIdentifier: "toHome", sender: nil)
                } else {
                    let errors = response!["error"].dictionaryValue
                    let error = errors.values.first?.stringValue
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
    func serverCallForSocialLogin(code:String, email:String){
        showProgressOnView(self.view)
        let params:[String: Any] = ["code":code]
        
        
        NetworkManager.sharedInstance.executeService(WEB_URL.google_auth, postParameters: params, completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    let token = response!["token"].stringValue
                    UserDefaults.standard.set(token, forKey: "token")
                    UserDefaults.standard.set(true, forKey: "isLoggedIn")
                    UserDefaults.standard.set(email, forKey: "email")
                    self.performSegue(withIdentifier: "toHome", sender: nil)
                } else {
                    let errors = response!["error"].dictionaryValue
                    let error = errors.values.first?.stringValue
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
    
    
}

@available(iOS 13.0, *)
extension SignInViewController : ASAuthorizationControllerDelegate {
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        if let e = error as? ASAuthorizationError {
            switch e.code {
            case .canceled:
                let alert = UIAlertController(title: "Error", message: "User did cancel authorization.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            case .failed:
                let alert = UIAlertController(title: "Error", message: "Authorization failed.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case .invalidResponse:
                let alert = UIAlertController(title: "Error", message: "Authorization failed.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case .notHandled:
                let alert = UIAlertController(title: "Error", message: "Authorization failed.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case .unknown:
                    let alert = UIAlertController(title: "Error", message: "Unknown error with password auth, trying to request for appleID auth..", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
            default:
                let alert = UIAlertController(title: "Error", message: "Unsupported error code.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }

    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            
            // Create an account in your system.
            // For the purpose of this demo app, store the these details in the keychain.
            let currentUserIdentifier = appleIDCredential.user
            let currentUserFirstName = appleIDCredential.fullName?.givenName ?? ""
            let currentUserLastName = appleIDCredential.fullName?.familyName ?? ""
            let currentUserEmail = appleIDCredential.email ?? ""
            
//            let oldUser = UserDefaults.standard.object(forKey: "User") ?? ""
//            UserDefaults.standard.set(currentUserIdentifier, forKey: "User")
//
//            if oldUser as! String == currentUserIdentifier {
//                print("same")
//            }
            
//            KeychainItem.currentUserIdentifier = appleIDCredential.user
//            KeychainItem.currentUserFirstName = appleIDCredential.fullName?.givenName
//            KeychainItem.currentUserLastName = appleIDCredential.fullName?.familyName
//            KeychainItem.currentUserEmail = appleIDCredential.email
//
//            self.apiforSocialLogin(name: currentUserFirstName, last_name: currentUserLastName, email: currentUserEmail, type: "a")
            
            print("User Id - \(appleIDCredential.user)")
            print("User Name - \(appleIDCredential.fullName?.description ?? "N/A")")
            print("User Email - \(appleIDCredential.email ?? "N/A")")
            print("Real User Status - \(appleIDCredential.realUserStatus.rawValue)")
            
            if let identityTokenData = appleIDCredential.identityToken,
                let identityTokenString = String(data: identityTokenData, encoding: .utf8) {
                print("Identity Token \(identityTokenString)")
            }
            
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password
//            loginWithCredentials(email: username, password: password)
//            apiforSignIn(email: username, password: password)
        }
    }
    
    
}

extension SignInViewController : ASAuthorizationControllerPresentationContextProviding {
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}
