//
//  EmailVerificationViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 29/06/21.
//

import UIKit
import SwiftyJSON

class EmailVerificationViewController: UIViewController {

    @IBOutlet weak var emailText: UILabel!
    var email: String!
    var password: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        emailText.text = "We have sent an email to \(email ?? "you.")"
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func statusCheck(_ sender: UIButton) {
        serverCallForLogin()
    }
    @IBAction func resendMail(_ sender: UIButton) {
        serverCallForResendMail()
    }
    
    //MARK:- Server Calls
    func serverCallForLogin(){
        showProgressOnView(self.view)
        let params:[String: Any] = ["email":self.email!,
                                    "password":self.password!]
        
        
        NetworkManager.sharedInstance.executeService(WEB_URL.login, postParameters: params, completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    let token = response!["token"].stringValue
                    UserDefaults.standard.set(token, forKey: "token")
                    UserDefaults.standard.set(true, forKey: "isLoggedIn")
                    UserDefaults.standard.set(self.email, forKey: "email")
                    self.performSegue(withIdentifier: "toHome3", sender: nil)
                } else {
                    let errors = response!["error"].dictionaryValue
                    let error = errors.values.first?.stringValue
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
    
    func serverCallForResendMail(){
        showProgressOnView(self.view)
        let params:[String: Any] = ["email":self.email!]
        
        
        NetworkManager.sharedInstance.executeService(WEB_URL.resend_verification, postParameters: params, completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    self.view.makeToast(response!["message"].stringValue, duration: 2.0, position: .bottom)
                } else {
                    let errors = response!["error"].dictionaryValue
                    let error = errors.values.first?.stringValue
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
