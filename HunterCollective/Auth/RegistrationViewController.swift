//
//  RegistrationViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 03/06/21.
//

import UIKit
import GoogleSignIn
import AuthenticationServices
import Toast_Swift
import SwiftyJSON

class RegistrationViewController: UIViewController,GIDSignInDelegate {

    @IBOutlet weak var emailTxtFld: DesignableTextfield!
    @IBOutlet weak var passwordTxtFld: DesignableTextfield!
    @IBOutlet weak var cnfrmPwdTxtFld: DesignableTextfield!
    
    @IBOutlet weak var bottomLbl: UILabel!
    var window: UIWindow?
    var appleSignInDelegates: SignInWithAppleDelegates! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let text = "By tapping Sign Up or Sign In, you agree to our Terms and Conditions. Learn how we use your data in our Privacy Policy and Cookies Policy."
        bottomLbl.text = text
        self.bottomLbl.textColor =  UIColor.black
        let boldlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: "Terms and Conditions.")
        let range2 = (text as NSString).range(of: "Privacy Policy")
        let range3 = (text as NSString).range(of: "Cookies Policy")
        boldlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: "Roboto-Bold", size: 12.0)!, range: range1)
        boldlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: "Roboto-Bold", size: 12.0)!, range: range2)
        boldlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: "Roboto-Bold", size: 12.0)!, range: range3)
        bottomLbl.attributedText = boldlineAttriString
        bottomLbl.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tappedOnLabel(_:)))
        tapGesture.numberOfTouchesRequired = 1
        bottomLbl.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
        guard let text = bottomLbl.text else { return }
        let termsConditionRange = (text as NSString).range(of: "Terms and Conditions")
        let privacypolicyRange = (text as NSString).range(of: "Privacy Policy")
        let cookiePolicyRange = (text as NSString).range(of: "Cookies Policy")
        if gesture.didTapAttributedTextInLabel(label: self.bottomLbl, inRange: termsConditionRange) {
            print("Terms and Conditions tapped")
        } else if gesture.didTapAttributedTextInLabel(label: self.bottomLbl, inRange: privacypolicyRange) {
            print("Privacy Policy tapped")
        } else if gesture.didTapAttributedTextInLabel(label: self.bottomLbl, inRange: cookiePolicyRange){
            print("Cookies Policy tapped")
        }
    }
    @available(iOS 13.0, *)
    private func performSignIn(using requests: [ASAuthorizationRequest]) {
      appleSignInDelegates = SignInWithAppleDelegates(window: window) { success in
        if success {
          // update UI
        } else {
          // show the user an error
        }
      }

      let controller = ASAuthorizationController(authorizationRequests: requests)
      controller.delegate = appleSignInDelegates
      controller.presentationContextProvider = appleSignInDelegates

      controller.performRequests()
    }
    
    //MARK: - Button Actions
    @IBAction func toggleEye(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            passwordTxtFld.isSecureTextEntry = true
            cnfrmPwdTxtFld.isSecureTextEntry = true
        } else {
            sender.isSelected = true
            passwordTxtFld.isSecureTextEntry = false
            cnfrmPwdTxtFld.isSecureTextEntry = false
        }
    }
    
    @IBAction func createAccount(_ sender: UIButton) {
        if !emailTxtFld.text!.isValidEmail {
            self.view.makeToast("Email is not valid.", duration: 3.0, position: .bottom)
        } else if passwordTxtFld.text?.isEmpty == true {
            self.view.makeToast("Please enter a valid password.", duration: 3.0, position: .bottom)
        } else if passwordTxtFld.text != cnfrmPwdTxtFld.text{
            self.view.makeToast("Password do not match.", duration: 3.0, position: .bottom)
        } else{
//            self.performSegue(withIdentifier: "toHome2", sender: nil)
            serverCallForRegistration()
        }
    }
    @IBAction func signInTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func appleSignUp(_ sender: UIButton) {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]

            performSignIn(using: [request])
        } else {
            // Fallback on earlier versions
        }
        
    }
    @IBAction func googleSignUp(_ sender: UIButton) {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
        GIDSignIn.sharedInstance().delegate = self
    }
    
    //MARK: - Google Signin Delegates
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
      if let error = error {
        if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
          debugPrint("The user has not signed in before or they have since signed out.")
        } else {
          debugPrint("\(error.localizedDescription)")
        }
        return
      }
      // Perform any operations on signed in user here.
        _ = user.userID                  // For client-side use only!
        let code = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile.name
        let first_name  = user?.profile.givenName
        let last_name = (user?.profile.familyName)!
        let email = user.profile.email
        
        self.serverCallForSocialLogin(code: code!, email: email!)
//        self.apiforSocialLogin(name: first_name!, last_name: last_name, email: email!, type: "g")
      // ...
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
      // Perform any operations when the user disconnects from app here.
      // ...
    }
    
    
    //MARK:- Server Calls
    func serverCallForRegistration(){
        showProgressOnView(self.view)
        let params:[String: Any] = ["email":self.emailTxtFld.text ?? "",
                                   "password":self.passwordTxtFld.text ?? ""]
        
        
        NetworkManager.sharedInstance.executeService(WEB_URL.register, postParameters: params, completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    
                    let message = response!["message"].stringValue
                    self.view.makeToast(message, duration: 2.0, position: .bottom)
                    
                    let when = DispatchTime.now() + 2
                    DispatchQueue.main.asyncAfter(deadline: when){
//                        self.dismiss(animated: true, completion: nil)
                        self.performSegue(withIdentifier: "toVerify", sender: nil)
                    }
                } else {
                    let errors = response!["error"].dictionaryValue
                    let error = errors.values.first?.stringValue //.allValues[0] as! String
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
    func serverCallForSocialLogin(code:String, email:String){
        showProgressOnView(self.view)
        let params:[String: Any] = ["code":code]
        
        
        NetworkManager.sharedInstance.executeService(WEB_URL.google_auth, postParameters: params, completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    let token = response!["token"].stringValue
                    UserDefaults.standard.set(token, forKey: "token")
                    UserDefaults.standard.set(true, forKey: "isLoggedIn")
                    UserDefaults.standard.set(email, forKey: "email")
                    self.performSegue(withIdentifier: "toHome2", sender: nil)
                } else {
                    let errors = response!["error"].dictionaryValue
                    let error = errors.values.first?.stringValue
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toVerify" {
            let emailVerifVc: EmailVerificationViewController = segue.destination as! EmailVerificationViewController
            emailVerifVc.email = self.emailTxtFld.text
            emailVerifVc.password = self.passwordTxtFld.text
        }
    }

}


extension UITapGestureRecognizer {
   
   func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
       guard let attributedText = label.attributedText else { return false }

       let mutableStr = NSMutableAttributedString.init(attributedString: attributedText)
       mutableStr.addAttributes([NSAttributedString.Key.font : label.font!], range: NSRange.init(location: 0, length: attributedText.length))
       
       // If the label have text alignment. Delete this code if label have a default (left) aligment. Possible to add the attribute in previous adding.
       let paragraphStyle = NSMutableParagraphStyle()
       paragraphStyle.alignment = .center
       mutableStr.addAttributes([NSAttributedString.Key.paragraphStyle : paragraphStyle], range: NSRange(location: 0, length: attributedText.length))

       // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
       let layoutManager = NSLayoutManager()
       let textContainer = NSTextContainer(size: CGSize.zero)
       let textStorage = NSTextStorage(attributedString: mutableStr)
       
       // Configure layoutManager and textStorage
       layoutManager.addTextContainer(textContainer)
       textStorage.addLayoutManager(layoutManager)
       
       // Configure textContainer
       textContainer.lineFragmentPadding = 0.0
       textContainer.lineBreakMode = label.lineBreakMode
       textContainer.maximumNumberOfLines = label.numberOfLines
       let labelSize = label.bounds.size
       textContainer.size = labelSize
       
       // Find the tapped character location and compare it to the specified range
       let locationOfTouchInLabel = self.location(in: label)
       let textBoundingBox = layoutManager.usedRect(for: textContainer)
       let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                         y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
       let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x,
                                                    y: locationOfTouchInLabel.y - textContainerOffset.y);
       let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
       return NSLocationInRange(indexOfCharacter, targetRange)
   }
   
}
