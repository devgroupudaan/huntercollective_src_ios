//
//  ForgotPasswordViewController.swift
//  HunterCollective
//
//  Created by Macbook Pro (L43) on 03/06/21.
//

import UIKit
import SwiftyJSON

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var emailTxtFld: DesignableTextfield!
    @IBOutlet weak var popUpView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK: - Button Actions
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func popUpDone(_ sender: UIButton) {
        popUpView.isHidden = true
    }
    
    @IBAction func sendLink(_ sender: UIButton) {
        serverCallForgotPassword()
//        popUpView.isHidden = false
    }
    
    //MARK:- Server Calls
    func serverCallForgotPassword(){
        showProgressOnView(self.view)
        let params:[String: Any] = ["email":self.emailTxtFld.text ?? ""]
        
        
        NetworkManager.sharedInstance.executeService(WEB_URL.resetPassword, postParameters: params, completionHandler: { (success:Bool, response:JSON?) in
            hideProgressOnView(self.view)
            
            if success == true {
                if response!["status"].stringValue  == "success"{
                    DispatchQueue.main.async{
                        self.popUpView.isHidden = false
                        self.emailTxtFld.text = ""
                    }
                } else {
                    let errors = response!["error"].dictionaryValue
                    let error = errors.values.first?.stringValue
                    self.view.makeToast(error, duration: 2.0, position: .bottom)
                }
            }
        })
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
